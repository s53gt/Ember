import 'package:ember_ui/features/device_control/application/wifi_service.dart';
import 'package:ember_ui/features/device_control/domain/wifi_network.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'wifi_scan_page_controller.g.dart';

@riverpod
class WifiScanPageController extends _$WifiScanPageController {
  void refresh() => ref.invalidateSelf();

  @override
  Future<List<WifiNetwork>> build() async {
    final wifiService = ref.watch(wifiServiceProvider);
    return wifiService.scan();
  }
}
