#include "commands/provisioning.h"

#include "argtable3/argtable3.h"
#include "esp_console.h"
#include "esp_log.h"
#include "esp_system.h"
#include "provisioning.h"
#include "repl_private.h"

static const char* TAG = "prov";

static int repl_prov_cmd(int argc, char** argv);

static struct {
  struct arg_lit* reset;
  struct arg_lit* stop;
  struct arg_end* end;
} prov_args;

static esp_console_cmd_t prov_cmd = {
    .command = "prov",
    .help = "(Re)provision the device",
    .hint = NULL,
    .func = &repl_prov_cmd,
    .argtable = &prov_args,
};

static void repl_prov_init_arg_tables(void) {
  prov_args.reset = arg_lit0("r", "reset", "Reset and reprovision");
  prov_args.stop = arg_lit0("x", "stop", "Stop ongoing provisioning");
  prov_args.end = arg_end(2);
}

esp_err_t repl_prov_register() {
  repl_prov_init_arg_tables();
  return esp_console_cmd_register(&prov_cmd);
}

static int repl_prov_cmd(int argc, char** argv) {
  REPL_ARGS_CHECK(prov_args, argc, argv);
  if (prov_args.stop->count > 0) {
    prov_stop();
    ESP_LOGI(TAG, "Provisioning stopped");
    return 0;
  }
  if (prov_args.reset->count > 0) {
    prov_reset();
    esp_restart();
    return 0;
  }
  return prov_start();
}
