import 'package:drift/drift.dart';
import 'package:ember_ui/constants/ble_constants.dart';

@TableIndex(name: "position_report_from_call", columns: {#fromCall})
@TableIndex(name: "position_report_to_call", columns: {#toCall})
@TableIndex(name: "position_report_timestamp", columns: {#timestamp})
@TableIndex(name: "position_report_created_at", columns: {#createdAt})
class PositionReport extends Table {
  IntColumn get id => integer().autoIncrement()();
  TextColumn get fromCall =>
      text().withLength(min: 1, max: bleHamStaCallsignMaxLen)();
  TextColumn get toCall =>
      text().withLength(min: 1, max: bleHamStaCallsignMaxLen)();
  TextColumn get path => text().withDefault(const Constant(""))();
  RealColumn get latitude => real()();
  RealColumn get longitude => real()();
  IntColumn get ambiguity => integer()();
  TextColumn get tableChr => text().withLength(min: 1, max: 1)();
  TextColumn get symbolChr => text().withLength(min: 1, max: 1)();
  TextColumn get comment => text().withDefault(const Constant(""))();
  IntColumn get altitude => integer().nullable()();
  DateTimeColumn get timestamp => dateTime().nullable()();
  DateTimeColumn get createdAt => dateTime().withDefault(currentDateAndTime)();
}
