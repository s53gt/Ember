#pragma once
#include <time.h>

#include "esp_err.h"
#include "freertos/FreeRTOS.h"

typedef struct {
  int degrees;
  double minutes;
  char cardinal;
} gnss_position_t;

typedef struct {
  uint8_t fix;
  int n_satellites;
  struct tm time;
  gnss_position_t latitude;
  gnss_position_t longitude;
  double altitude;
  char altitude_unit;
} gnss_fix_t;

esp_err_t gnss_init(void);
esp_err_t gnss_start(void);
void gnss_stop(void);
esp_err_t gnss_get_fix(gnss_fix_t* out, TickType_t block_time);
