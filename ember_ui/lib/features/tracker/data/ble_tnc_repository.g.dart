// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ble_tnc_repository.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$bleTncRepositoryHash() => r'fb65f8ce3b980f6bd31124cdc1ef0c0e6e1cadf8';

/// See also [bleTncRepository].
@ProviderFor(bleTncRepository)
final bleTncRepositoryProvider = AutoDisposeProvider<BleTncRepository>.internal(
  bleTncRepository,
  name: r'bleTncRepositoryProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$bleTncRepositoryHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef BleTncRepositoryRef = AutoDisposeProviderRef<BleTncRepository>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
