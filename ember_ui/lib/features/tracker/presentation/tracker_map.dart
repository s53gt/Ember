import 'dart:async';

import 'package:ember_ui/features/permission/application/location_permission.dart';
import 'package:ember_ui/features/permission/domain/permission_result.dart';
import 'package:ember_ui/features/tracker/presentation/cluster_marker_layer.dart';
import 'package:ember_ui/utils/environment.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_map_cancellable_tile_provider/flutter_map_cancellable_tile_provider.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:latlong2/latlong.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter_map_location_marker/flutter_map_location_marker.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class TrackerMap extends ConsumerStatefulWidget {
  const TrackerMap({super.key});

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _TrackerMapState();
}

class _TrackerMapState extends ConsumerState<TrackerMap> {
  late AlignOnUpdate _alignPositionOnUpdate;
  late final StreamController<double?> _alignPositionStreamController;
  final MapController _controller = MapController();
  PermissionResult _lastLocPermRes = PermissionResult.pending;

  @override
  void initState() {
    super.initState();
    _alignPositionOnUpdate = AlignOnUpdate.always;
    _alignPositionStreamController = StreamController<double?>();
  }

  @override
  void dispose() {
    _alignPositionStreamController.close();
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final localizations = AppLocalizations.of(context)!;
    final locPermRes = ref.watch(locationPermissionProvider);
    if (locPermRes != _lastLocPermRes) {
      _lastLocPermRes = locPermRes;
      if (locPermRes == PermissionResult.granted) {
        WidgetsBinding.instance.addPostFrameCallback((_) {
          _controller.move(_controller.camera.center, 18);
        });
      }
    }
    return FlutterMap(
        mapController: _controller,
        options: MapOptions(
          backgroundColor: Theme.of(context).colorScheme.surface,
          minZoom: 3,
          maxZoom: 20,
          initialCenter: const LatLng(0, 0),
          initialZoom: 3,
          onPositionChanged: (MapPosition position, bool hasGesture) {
            if (hasGesture && _alignPositionOnUpdate != AlignOnUpdate.never) {
              setState(() => _alignPositionOnUpdate = AlignOnUpdate.never);
            }
          },
        ),
        children: [
          TileLayer(
            // urlTemplate: "https://tile.openstreetmap.org/{z}/{x}/{y}.png",
            urlTemplate:
                "https://{s}.basemaps.cartocdn.com/dark_all/{z}/{x}/{y}{r}.png",
            subdomains: const ["a", "b", "c", "d"],
            retinaMode: RetinaMode.isHighDensity(context),
            userAgentPackageName: Environment.instance.packageInfo.packageName,
            tileProvider: CancellableNetworkTileProvider(),
          ),
          if (locPermRes == PermissionResult.granted)
            CurrentLocationLayer(
              alignPositionStream: _alignPositionStreamController.stream,
              alignPositionOnUpdate: _alignPositionOnUpdate,
            ),
          const ClusterMarkerLayer(),
          RichAttributionWidget(
              popupInitialDisplayDuration: const Duration(seconds: 5),
              animationConfig: const ScaleRAWA(),
              showFlutterMapAttribution: false,
              alignment: AttributionAlignment.bottomLeft,
              openButton: (_, onPressed) => IconButton(
                    onPressed: onPressed,
                    icon: const Icon(Icons.info_outline),
                  ),
              attributions: [
                TextSourceAttribution(
                  'OpenStreetMap contributors',
                  onTap: () async => launchUrl(
                    Uri.parse('https://openstreetmap.org/copyright'),
                  ),
                ),
                TextSourceAttribution(
                  'CARTO',
                  onTap: () async => launchUrl(
                    Uri.parse('https://carto.com/attributions'),
                  ),
                ),
              ]),
          Align(
            alignment: Alignment.bottomRight,
            child: Padding(
              padding: const EdgeInsets.all(20),
              child: FloatingActionButton(
                shape: RoundedRectangleBorder(
                  side: BorderSide(
                    color: Theme.of(context).colorScheme.primary,
                  ),
                  borderRadius: BorderRadius.circular(10.0),
                ),
                tooltip: localizations.followMe,
                onPressed: locPermRes != PermissionResult.granted
                    ? null
                    : () {
                        setState(() =>
                            _alignPositionOnUpdate = AlignOnUpdate.always);
                        _alignPositionStreamController
                            .add(18); // Center at zoom level 18
                      },
                child: Icon(
                  Icons.my_location,
                  color: _alignPositionOnUpdate == AlignOnUpdate.always
                      ? Theme.of(context).colorScheme.primary
                      : null,
                ),
              ),
            ),
          ),
        ]);
  }
}
