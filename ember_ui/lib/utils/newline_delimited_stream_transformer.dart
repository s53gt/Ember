import 'dart:async';
import 'dart:convert';

class NewlineDelimitedStreamTransformer
    implements StreamTransformer<String, String> {
  late final StreamController<String> _streamController;
  late Stream<String> _stream;
  StreamSubscription<String>? _streamSubscription;
  String _buffer = "";

  NewlineDelimitedStreamTransformer() {
    _streamController = StreamController(
      onListen: () => _listen(),
      onPause: () => _streamSubscription?.pause(),
      onResume: () => _streamSubscription?.resume(),
      onCancel: () => _streamSubscription?.cancel(),
    );
  }

  void _listen() {
    _streamSubscription = _stream.listen(
      (chunk) {
        _buffer += chunk;
        var line = "";
        for (var c in utf8.encode(_buffer)) {
          if (c == utf8.encode("\r")[0]) continue;
          if (c == utf8.encode("\n")[0]) {
            if (line.trim().isNotEmpty) _streamController.add(line);
            line = "";
            continue;
          }
          line += utf8.decode(([c]));
        }
        _buffer = line;
      },
      onDone: () => _streamController.close(),
      onError: (e) => _streamController.addError(e),
    );
  }

  @override
  Stream<String> bind(Stream<String> stream) {
    _stream = stream;
    return _streamController.stream;
  }

  @override
  StreamTransformer<RS, RT> cast<RS, RT>() {
    return StreamTransformer.castFrom(this);
  }
}
