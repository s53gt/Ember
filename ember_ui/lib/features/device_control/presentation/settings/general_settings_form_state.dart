import 'package:flutter/material.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'general_settings_form_state.freezed.dart';

@freezed
class GeneralSettingsFormState with _$GeneralSettingsFormState {
  const factory GeneralSettingsFormState({
    required TextEditingController callsignController,
    required TextEditingController passcodeController,
  }) = _GeneralSettingsFormState;
}
