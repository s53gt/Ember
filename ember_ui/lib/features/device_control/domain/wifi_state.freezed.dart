// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'wifi_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$WifiState {
  String get ssid => throw _privateConstructorUsedError;
  String get ipAddress => throw _privateConstructorUsedError;
  String get gwAddress => throw _privateConstructorUsedError;
  String get netmask => throw _privateConstructorUsedError;
  String get macAddress => throw _privateConstructorUsedError;
  int get rssi => throw _privateConstructorUsedError;
  WifiStatus get status => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $WifiStateCopyWith<WifiState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $WifiStateCopyWith<$Res> {
  factory $WifiStateCopyWith(WifiState value, $Res Function(WifiState) then) =
      _$WifiStateCopyWithImpl<$Res, WifiState>;
  @useResult
  $Res call(
      {String ssid,
      String ipAddress,
      String gwAddress,
      String netmask,
      String macAddress,
      int rssi,
      WifiStatus status});
}

/// @nodoc
class _$WifiStateCopyWithImpl<$Res, $Val extends WifiState>
    implements $WifiStateCopyWith<$Res> {
  _$WifiStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? ssid = null,
    Object? ipAddress = null,
    Object? gwAddress = null,
    Object? netmask = null,
    Object? macAddress = null,
    Object? rssi = null,
    Object? status = null,
  }) {
    return _then(_value.copyWith(
      ssid: null == ssid
          ? _value.ssid
          : ssid // ignore: cast_nullable_to_non_nullable
              as String,
      ipAddress: null == ipAddress
          ? _value.ipAddress
          : ipAddress // ignore: cast_nullable_to_non_nullable
              as String,
      gwAddress: null == gwAddress
          ? _value.gwAddress
          : gwAddress // ignore: cast_nullable_to_non_nullable
              as String,
      netmask: null == netmask
          ? _value.netmask
          : netmask // ignore: cast_nullable_to_non_nullable
              as String,
      macAddress: null == macAddress
          ? _value.macAddress
          : macAddress // ignore: cast_nullable_to_non_nullable
              as String,
      rssi: null == rssi
          ? _value.rssi
          : rssi // ignore: cast_nullable_to_non_nullable
              as int,
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as WifiStatus,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$WifiStateImplCopyWith<$Res>
    implements $WifiStateCopyWith<$Res> {
  factory _$$WifiStateImplCopyWith(
          _$WifiStateImpl value, $Res Function(_$WifiStateImpl) then) =
      __$$WifiStateImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {String ssid,
      String ipAddress,
      String gwAddress,
      String netmask,
      String macAddress,
      int rssi,
      WifiStatus status});
}

/// @nodoc
class __$$WifiStateImplCopyWithImpl<$Res>
    extends _$WifiStateCopyWithImpl<$Res, _$WifiStateImpl>
    implements _$$WifiStateImplCopyWith<$Res> {
  __$$WifiStateImplCopyWithImpl(
      _$WifiStateImpl _value, $Res Function(_$WifiStateImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? ssid = null,
    Object? ipAddress = null,
    Object? gwAddress = null,
    Object? netmask = null,
    Object? macAddress = null,
    Object? rssi = null,
    Object? status = null,
  }) {
    return _then(_$WifiStateImpl(
      ssid: null == ssid
          ? _value.ssid
          : ssid // ignore: cast_nullable_to_non_nullable
              as String,
      ipAddress: null == ipAddress
          ? _value.ipAddress
          : ipAddress // ignore: cast_nullable_to_non_nullable
              as String,
      gwAddress: null == gwAddress
          ? _value.gwAddress
          : gwAddress // ignore: cast_nullable_to_non_nullable
              as String,
      netmask: null == netmask
          ? _value.netmask
          : netmask // ignore: cast_nullable_to_non_nullable
              as String,
      macAddress: null == macAddress
          ? _value.macAddress
          : macAddress // ignore: cast_nullable_to_non_nullable
              as String,
      rssi: null == rssi
          ? _value.rssi
          : rssi // ignore: cast_nullable_to_non_nullable
              as int,
      status: null == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as WifiStatus,
    ));
  }
}

/// @nodoc

class _$WifiStateImpl implements _WifiState {
  const _$WifiStateImpl(
      {required this.ssid,
      required this.ipAddress,
      required this.gwAddress,
      required this.netmask,
      required this.macAddress,
      required this.rssi,
      required this.status});

  @override
  final String ssid;
  @override
  final String ipAddress;
  @override
  final String gwAddress;
  @override
  final String netmask;
  @override
  final String macAddress;
  @override
  final int rssi;
  @override
  final WifiStatus status;

  @override
  String toString() {
    return 'WifiState(ssid: $ssid, ipAddress: $ipAddress, gwAddress: $gwAddress, netmask: $netmask, macAddress: $macAddress, rssi: $rssi, status: $status)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$WifiStateImpl &&
            (identical(other.ssid, ssid) || other.ssid == ssid) &&
            (identical(other.ipAddress, ipAddress) ||
                other.ipAddress == ipAddress) &&
            (identical(other.gwAddress, gwAddress) ||
                other.gwAddress == gwAddress) &&
            (identical(other.netmask, netmask) || other.netmask == netmask) &&
            (identical(other.macAddress, macAddress) ||
                other.macAddress == macAddress) &&
            (identical(other.rssi, rssi) || other.rssi == rssi) &&
            (identical(other.status, status) || other.status == status));
  }

  @override
  int get hashCode => Object.hash(runtimeType, ssid, ipAddress, gwAddress,
      netmask, macAddress, rssi, status);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$WifiStateImplCopyWith<_$WifiStateImpl> get copyWith =>
      __$$WifiStateImplCopyWithImpl<_$WifiStateImpl>(this, _$identity);
}

abstract class _WifiState implements WifiState {
  const factory _WifiState(
      {required final String ssid,
      required final String ipAddress,
      required final String gwAddress,
      required final String netmask,
      required final String macAddress,
      required final int rssi,
      required final WifiStatus status}) = _$WifiStateImpl;

  @override
  String get ssid;
  @override
  String get ipAddress;
  @override
  String get gwAddress;
  @override
  String get netmask;
  @override
  String get macAddress;
  @override
  int get rssi;
  @override
  WifiStatus get status;
  @override
  @JsonKey(ignore: true)
  _$$WifiStateImplCopyWith<_$WifiStateImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
