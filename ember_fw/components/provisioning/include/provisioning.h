#pragma once

#include <stdbool.h>

#include "esp_err.h"

esp_err_t prov_init(void);
void prov_deinit(void);
esp_err_t prov_start(void);
void prov_stop(void);
void prov_reset(void);
