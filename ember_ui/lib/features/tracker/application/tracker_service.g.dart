// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'tracker_service.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$trackerServiceHash() => r'aabd8dc6f5819120ae55472250f11a2da813e657';

/// See also [trackerService].
@ProviderFor(trackerService)
final trackerServiceProvider = AutoDisposeProvider<TrackerService>.internal(
  trackerService,
  name: r'trackerServiceProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$trackerServiceHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef TrackerServiceRef = AutoDisposeProviderRef<TrackerService>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
