import 'package:ember_ui/features/device_control/application/device_service.dart';
import 'package:ember_ui/features/device_control/domain/device.dart';
import 'package:ember_ui/features/tracker/application/aprs_service.dart';
import 'package:ember_ui/features/tracker/application/tnc_service.dart';
import 'package:ember_ui/features/tracker/domain/aprs_position.dart';
import 'package:ember_ui/features/tracker/domain/tnc2_header.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:test/test.dart';

import 'aprs_service_test.mocks.dart';

// Test cases taken from APRS spec http://www.aprs.org/doc/APRS101.PDF
@GenerateMocks([DeviceService, TncService])
void main() {
  group('Position report', () {
    late AprsService service;
    late MockDeviceService mockDeviceService;
    late MockTncService mockTncService;
    const dummyPosition = AprsPosition(
      header: Tnc2Header(
        from: "NOCALL",
        to: "APRS",
        path: "WIDE1-1*,WIDE2-1",
      ),
      latitude: 49 + (3 / 60) + (50 / 100 / 60),
      longitude: -(72 + (1 / 60) + (75 / 100 / 60)),
      ambiguity: 0,
      tableChr: "/",
      symbolChr: "-",
      comment: "Test 001234",
    );
    setUp(() {
      mockDeviceService = MockDeviceService();
      mockTncService = MockTncService();
      service = AprsService(mockDeviceService, mockTncService);
      const device = Device(
        id: "10:20:30:40:50:60",
        name: "ember_XXXXX-XX",
        connected: true,
        linkType: LinkType.ble,
      );
      when(mockDeviceService.connectedDevice).thenReturn(device);
      when(mockDeviceService.connectedDeviceStream)
          .thenAnswer((_) => Stream.fromIterable([device]));
    });
    tearDown(() {
      service.dispose();
    });
    group('common cases', () {
      test('no timestamp, no messaging, with comment', () {
        when(mockTncService.packets()).thenAnswer(
          (_) => Stream.fromIterable(
              ["NOCALL>APRS,WIDE1-1*,WIDE2-1:!4903.50N/07201.75W-Test 001234"]),
        );
        expect(service.positionReports, emits(dummyPosition));
        service.start();
      });
      test('no timestamp, no messaging, altitude = 1234 ft', () {
        when(mockTncService.packets()).thenAnswer(
          (_) => Stream.fromIterable([
            "NOCALL>APRS,WIDE1-1*,WIDE2-1:!4903.50N/07201.75W-Test /A=001234"
          ]),
        );
        expect(
          service.positionReports,
          emits(dummyPosition.copyWith(
              comment: "Test /A=001234", altitude: 1234)),
        );
        service.start();
      });
      test('zulu timestamp, no messaging, comment', () {
        when(mockTncService.packets()).thenAnswer(
          (_) => Stream.fromIterable([
            "NOCALL>APRS,WIDE1-1*,WIDE2-1:/092345z4903.50N/07201.75W>Test1234"
          ]),
        );
        expect(
          service.positionReports,
          emits(dummyPosition.copyWith(
            timestamp: DateTime.timestamp().copyWith(
              day: 9,
              hour: 23,
              minute: 45,
              second: 0,
              millisecond: 0,
              microsecond: 0,
            ),
            comment: "Test1234",
            symbolChr: ">",
          )),
        );
        service.start();
      });
      test('local timestamp, messaging, comment', () {
        when(mockTncService.packets()).thenAnswer(
          (_) => Stream.fromIterable([
            "NOCALL>APRS,WIDE1-1*,WIDE2-1:@092345/4903.50N/07201.75W>Test1234"
          ]),
        );
        expect(
          service.positionReports,
          emits(dummyPosition.copyWith(
            timestamp: DateTime.now().copyWith(
              day: 9,
              hour: 23,
              minute: 45,
              second: 0,
              millisecond: 0,
              microsecond: 0,
            ),
            comment: "Test1234",
            symbolChr: ">",
          )),
        );
        service.start();
      });
    });
    group('edge cases', () {
      test('position ambiguity 1', () {
        when(mockTncService.packets()).thenAnswer(
          (_) => Stream.fromIterable(
              ["NOCALL>APRS,WIDE1-1*,WIDE2-1:!4903.5 N/07201.75W-Test 001234"]),
        );
        expect(
          service.positionReports,
          emits(dummyPosition.copyWith(
              longitude: -(72 + (1.7 / 60)), ambiguity: 1)),
        );
        service.start();
      });
      test('position ambiguity 2', () {
        when(mockTncService.packets()).thenAnswer(
          (_) => Stream.fromIterable(
              ["NOCALL>APRS,WIDE1-1*,WIDE2-1:!4903.  N/07201.75W-Test 001234"]),
        );
        expect(
          service.positionReports,
          emits(dummyPosition.copyWith(
              latitude: 49 + 3 / 60,
              longitude: -(72 + (1 / 60)),
              ambiguity: 2)),
        );
        service.start();
      });
      test('position ambiguity 3', () {
        when(mockTncService.packets()).thenAnswer(
          (_) => Stream.fromIterable(
              ["NOCALL>APRS,WIDE1-1*,WIDE2-1:!490 .  N/07201.75W-Test 001234"]),
        );
        expect(
          service.positionReports,
          emits(dummyPosition.copyWith(
              latitude: 49, longitude: -72, ambiguity: 3)),
        );
        service.start();
      });
      test('position ambiguity 4', () {
        when(mockTncService.packets()).thenAnswer(
          (_) => Stream.fromIterable(
              ["NOCALL>APRS,WIDE1-1*,WIDE2-1:!49  .  N/07201.75W-Test 001234"]),
        );
        expect(
          service.positionReports,
          emits(dummyPosition.copyWith(
              latitude: 49, longitude: -72, ambiguity: 4)),
        );
        service.start();
      });
      test('X1J node header string', () {
        when(mockTncService.packets()).thenAnswer(
          (_) => Stream.fromIterable([
            "NOCALL>APRS,WIDE1-1*,WIDE2-1:TheNet X-1J4  (BFLD)!4903.50N/07201.75Wn"
          ]),
        );
        expect(
          service.positionReports,
          emits(dummyPosition.copyWith(symbolChr: "n", comment: "")),
        );
        service.start();
      });
      test('default null position - symbol should be fixed to \\.', () {
        when(mockTncService.packets()).thenAnswer(
          (_) => Stream.fromIterable(
              ["NOCALL>APRS,WIDE1-1*,WIDE2-1:!0000.00N/00000.00W>"]),
        );
        expect(
          service.positionReports,
          emits(dummyPosition.copyWith(
              latitude: 0,
              longitude: 0,
              tableChr: "\\",
              symbolChr: ".",
              comment: "")),
        );
        service.start();
      });
    });
  });
}
