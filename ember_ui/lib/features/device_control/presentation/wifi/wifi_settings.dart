import 'package:ember_ui/features/device_control/application/device_service.dart';
import 'package:ember_ui/features/device_control/presentation/wifi/wifi_settings_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:go_router/go_router.dart';

class WifiSettings extends ConsumerStatefulWidget {
  const WifiSettings({super.key});

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _WifiSettings();
}

class _WifiSettings extends ConsumerState<WifiSettings> {
  @override
  Widget build(BuildContext context) {
    final localizations = AppLocalizations.of(context)!;
    final deviceService = ref.watch(deviceServiceProvider);
    final wifiSettingsState = ref.watch(wifiSettingsControllerProvider);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        const SizedBox(height: 20),
        Text(
          localizations.wifi,
          style: const TextStyle(fontSize: 18),
        ),
        const SizedBox(height: 20),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              localizations.ssid,
              style: TextStyle(
                color: Theme.of(context).colorScheme.onPrimaryContainer,
              ),
            ),
            const SizedBox(height: 10),
            Text(wifiSettingsState.valueOrNull?.ssid ?? ""),
          ],
        ),
        const SizedBox(height: 10),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              localizations.rssi,
              style: TextStyle(
                color: Theme.of(context).colorScheme.onPrimaryContainer,
              ),
            ),
            const SizedBox(height: 10),
            Text("${wifiSettingsState.valueOrNull?.rssi ?? ""}"),
          ],
        ),
        const SizedBox(height: 10),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              localizations.ipAddress,
              style: TextStyle(
                color: Theme.of(context).colorScheme.onPrimaryContainer,
              ),
            ),
            const SizedBox(height: 10),
            Text(wifiSettingsState.valueOrNull?.ipAddress ?? ""),
          ],
        ),
        const SizedBox(height: 10),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              localizations.netmask,
              style: TextStyle(
                color: Theme.of(context).colorScheme.onPrimaryContainer,
              ),
            ),
            const SizedBox(height: 10),
            Text(wifiSettingsState.valueOrNull?.netmask ?? ""),
          ],
        ),
        const SizedBox(height: 10),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              localizations.gwAddress,
              style: TextStyle(
                color: Theme.of(context).colorScheme.onPrimaryContainer,
              ),
            ),
            const SizedBox(height: 10),
            Text(wifiSettingsState.valueOrNull?.gwAddress ?? ""),
          ],
        ),
        const SizedBox(height: 10),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              localizations.macAddress,
              style: TextStyle(
                color: Theme.of(context).colorScheme.onPrimaryContainer,
              ),
            ),
            const SizedBox(height: 10),
            Text(wifiSettingsState.valueOrNull?.macAddress ?? ""),
          ],
        ),
        const SizedBox(height: 10),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              localizations.status,
              style: TextStyle(
                color: Theme.of(context).colorScheme.onPrimaryContainer,
              ),
            ),
            const SizedBox(height: 10),
            if (wifiSettingsState.valueOrNull != null)
              Text(wifiSettingsState.value!.status.name
                  .toUpperCase()), // TODO l10n
            if (wifiSettingsState.valueOrNull == null)
              SizedBox(
                width: IconTheme.of(context).size ?? 24,
                height: IconTheme.of(context).size ?? 24,
                child: const CircularProgressIndicator(),
              )
          ],
        ),
        const SizedBox(height: 20),
        FilledButton(
          onPressed: deviceService.connectedDevice == null
              ? null
              : () => context.pushNamed("wifi-scan"),
          child: Text(localizations.chooseAccessPoint),
        ),
      ],
    );
  }
}
