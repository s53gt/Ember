const { v4: uuidv4 } = require("uuid");

const uuid = uuidv4();
console.log("UUID:", uuid);

const bytes = uuid.replace(/-/g, '').matchAll(/[0-9a-fA-F]{2}/g);
let bleUuidStr = "BLE_UUID128_INIT(";
for (const byte of [...bytes].reverse()) {
  bleUuidStr += `0x${byte[0]}, `;
}
bleUuidStr = bleUuidStr.substring(0, bleUuidStr.length - 2);
bleUuidStr += ")";
console.log(bleUuidStr);
