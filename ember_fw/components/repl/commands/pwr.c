#include "commands/pwr.h"

#include "argtable3/argtable3.h"
#include "esp_console.h"
#include "esp_err.h"
#include "esp_log.h"
#include "esp_system.h"
#include "repl_private.h"

static const char* TAG = "pwr";

static int repl_pwr_cmd(int argc, char** argv);

static struct {
  struct arg_lit* reboot;
  struct arg_lit* force;
  struct arg_end* end;
} pwr_args;

static esp_console_cmd_t pwr_cmd = {
    .command = "pwr",
    .help = "Manage device power state",
    .hint = NULL,
    .func = &repl_pwr_cmd,
    .argtable = &pwr_args,
};

static void repl_pwr_init_arg_tables(void) {
  pwr_args.reboot = arg_lit0("r", "reboot", "Reboot the system");
  pwr_args.force = arg_lit0("f", "force", "Perform a forceful reboot; skips gracefully shutting down all components");
  pwr_args.end = arg_end(2);
}

esp_err_t repl_pwr_register(void) {
  repl_pwr_init_arg_tables();
  return esp_console_cmd_register(&pwr_cmd);
}

static int repl_pwr_cmd(int argc, char** argv) {
  REPL_ARGS_CHECK(pwr_args, argc, argv);
  if (pwr_args.reboot->count > 0) {
    if (pwr_args.force->count == 0) {
      // TODO Gracefully deinit/shutdown all components
    }
    esp_restart();
    return 0;
  }
  ESP_LOGE(TAG, "Invalid arguments");
  return 1;
}
