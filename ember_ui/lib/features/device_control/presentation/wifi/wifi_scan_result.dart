import 'package:ember_ui/features/device_control/application/wifi_service.dart';
import 'package:ember_ui/features/device_control/domain/wifi_network.dart';
import 'package:ember_ui/features/device_control/presentation/wifi/wifi_connect_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class WifiScanResult extends ConsumerStatefulWidget {
  final WifiNetwork net;

  const WifiScanResult({super.key, required this.net});

  @override
  ConsumerState<WifiScanResult> createState() => _WifiScanResultState();
}

class _WifiScanResultState extends ConsumerState<WifiScanResult> {
  bool _isConnecting = false;

  Icon _buildRssiIcon() {
    if (widget.net.rssi > -50) {
      return const Icon(Icons.signal_wifi_4_bar);
    } else if (widget.net.rssi >= -60) {
      return const Icon(Icons.network_wifi_3_bar);
    } else if (widget.net.rssi >= -70) {
      return const Icon(Icons.network_wifi_2_bar);
    } else {
      return const Icon(Icons.network_wifi_1_bar);
    }
  }

  Widget? _getSubtitle() {
    final localizations = AppLocalizations.of(context)!;
    if (_isConnecting) return Text(localizations.connecting);
    return null;
  }

  void _showErrorSnack() {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          backgroundColor: Colors.red.withAlpha(128),
          showCloseIcon: true,
          closeIconColor: Colors.white.withAlpha(200),
          content: Text(
            AppLocalizations.of(context)!.connectionFailed,
            style: TextStyle(
              color: Colors.white.withAlpha(200),
            ),
          ),
        ),
      );
    });
  }

  Future<void> _onTap() async {
    setState(() => _isConnecting = true);
    String? password;
    try {
      if (widget.net.authMode != WifiAuthMode.wifiAuthOpen) {
        password = await showWifiConnectDialog(context, widget.net);
        if (password == null) return;
      }
      final wifiService = ref.read(wifiServiceProvider);
      await wifiService.connect(widget.net, password);
      if (mounted) Navigator.of(context).pop();
    } catch (e) {
      _showErrorSnack();
    } finally {
      if (mounted) {
        setState(() => _isConnecting = false);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: _onTap,
      title: Row(children: [
        Text(widget.net.ssid),
        if (widget.net.authMode != WifiAuthMode.wifiAuthOpen)
          const Padding(
            padding: EdgeInsets.only(left: 5),
            child: Icon(
              Icons.lock_outline,
              size: 16,
            ),
          )
      ]),
      subtitle: _getSubtitle(),
      trailing: _buildRssiIcon(),
    );
  }
}
