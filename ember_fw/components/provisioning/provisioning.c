#include "provisioning.h"

#include "esp_check.h"
#include "esp_log.h"
#include "host/ble_hs.h"
#include "host/util/util.h"
#include "nimble/nimble_port.h"
#include "nimble/nimble_port_freertos.h"
#include "services/gap/ble_svc_gap.h"

// #include <stdio.h>
// #include <string.h>
//
// #include "esp_check.h"
#include "esp_err.h"
// #include "esp_event.h"
// #include "esp_event_base.h"
// #include "esp_log.h"
// #include "esp_wifi.h"
// #include "esp_wifi_types.h"
// #include "wifi_provisioning/manager.h"
// #include "wifi_provisioning/scheme_ble.h"

static const char* TAG = "prov";
// static const char *POP = "lora123";

// static bool did_init = false;
// static esp_event_handler_instance_t prov_handler = NULL;
static uint8_t own_addr_type;

static void prov_advertise(void);
// static void prov_on_event(void *arg, esp_event_base_t event_base, int32_t event_id, void *event_data);

static void prov_on_reset(int reason) { ESP_LOGE(TAG, "Resetting BLE state, reason=%d", reason); }

static int dynamic_service_gap_event(struct ble_gap_event* event, void* arg) {
  struct ble_gap_conn_desc desc;
  int rc;

  switch (event->type) {
    case BLE_GAP_EVENT_CONNECT:
      /* A new connection was established or a connection attempt failed. */
      MODLOG_DFLT(INFO, "connection %s; status=%d ", event->connect.status == 0 ? "established" : "failed",
                  event->connect.status);
      if (event->connect.status == 0) {
        rc = ble_gap_conn_find(event->connect.conn_handle, &desc);
        assert(rc == 0);
        // dynamic_service_print_conn_desc(&desc);
      }
      MODLOG_DFLT(INFO, "\n");
      int ret = ble_svc_gap_device_name_set("something-else-123");
      ESP_RETURN_ON_FALSE(ret == 0, ESP_FAIL, TAG, "Failed to set BLE device name, ret=%d", ret);

      if (event->connect.status != 0) {
        /* Connection failed; resume advertising. */
        prov_advertise();
      }

      return 0;

    case BLE_GAP_EVENT_DISCONNECT:
      MODLOG_DFLT(INFO, "disconnect; reason=%d ", event->disconnect.reason);
      // dynamic_service_print_conn_desc(&event->disconnect.conn);
      MODLOG_DFLT(INFO, "\n");

      /* Connection terminated; resume advertising. */
      prov_advertise();
      return 0;

    case BLE_GAP_EVENT_CONN_UPDATE:
      /* The central has updated the connection parameters. */
      MODLOG_DFLT(INFO, "connection updated; status=%d ", event->conn_update.status);
      rc = ble_gap_conn_find(event->conn_update.conn_handle, &desc);
      assert(rc == 0);
      // dynamic_service_print_conn_desc(&desc);
      MODLOG_DFLT(INFO, "\n");
      return 0;

    case BLE_GAP_EVENT_ADV_COMPLETE:
      MODLOG_DFLT(INFO, "advertise complete; reason=%d", event->adv_complete.reason);
      prov_advertise();
      return 0;

    case BLE_GAP_EVENT_NOTIFY_TX:
      MODLOG_DFLT(INFO,
                  "notify_tx event; conn_handle=%d attr_handle=%d "
                  "status=%d is_indication=%d",
                  event->notify_tx.conn_handle, event->notify_tx.attr_handle, event->notify_tx.status,
                  event->notify_tx.indication);
      return 0;

    case BLE_GAP_EVENT_SUBSCRIBE:
      MODLOG_DFLT(INFO,
                  "subscribe event; conn_handle=%d attr_handle=%d "
                  "reason=%d prevn=%d curn=%d previ=%d curi=%d\n",
                  event->subscribe.conn_handle, event->subscribe.attr_handle, event->subscribe.reason,
                  event->subscribe.prev_notify, event->subscribe.cur_notify, event->subscribe.prev_indicate,
                  event->subscribe.cur_indicate);
      return 0;

    case BLE_GAP_EVENT_MTU:
      MODLOG_DFLT(INFO, "mtu update event; conn_handle=%d cid=%d mtu=%d\n", event->mtu.conn_handle,
                  event->mtu.channel_id, event->mtu.value);
      return 0;
  }

  return 0;
}

static void prov_advertise(void) {
  struct ble_gap_adv_params adv_params;
  struct ble_hs_adv_fields fields;
  const char* name;
  int rc;
  memset(&fields, 0, sizeof fields);
  fields.flags = BLE_HS_ADV_F_DISC_GEN | BLE_HS_ADV_F_BREDR_UNSUP;
  fields.tx_pwr_lvl_is_present = 1;
  fields.tx_pwr_lvl = BLE_HS_ADV_TX_PWR_LVL_AUTO;
  name = ble_svc_gap_device_name();
  fields.name = (uint8_t*)name;
  fields.name_len = strlen(name);
  fields.name_is_complete = 1;
  fields.uuids16 = (ble_uuid16_t[]){};
  fields.num_uuids16 = 1;
  fields.uuids16_is_complete = 1;
  rc = ble_gap_adv_set_fields(&fields);
  if (rc != 0) {
    MODLOG_DFLT(ERROR, "error setting advertisement data; rc=%d\n", rc);
    return;
  }
  memset(&adv_params, 0, sizeof adv_params);
  adv_params.conn_mode = BLE_GAP_CONN_MODE_UND;
  adv_params.disc_mode = BLE_GAP_DISC_MODE_GEN;
  rc = ble_gap_adv_start(own_addr_type, NULL, BLE_HS_FOREVER, &adv_params, dynamic_service_gap_event, NULL);
  if (rc != 0) {
    MODLOG_DFLT(ERROR, "error enabling advertisement; rc=%d\n", rc);
    return;
  }
}

static void prov_on_sync(void) {
  ESP_LOGD(TAG, "BLE sync");
  int rc;
  rc = ble_hs_util_ensure_addr(0);
  assert(rc == 0);
  rc = ble_hs_id_infer_auto(0, &own_addr_type);
  if (rc != 0) {
    MODLOG_DFLT(ERROR, "error determining address type; rc=%d\n", rc);
    return;
  }
  uint8_t addr_val[6] = {0};
  rc = ble_hs_id_copy_addr(own_addr_type, addr_val, NULL);
  ESP_LOGI(TAG, "Device Address: %02X:%02X:%02X:%02X:%02X:%02X", addr_val[5], addr_val[4], addr_val[3], addr_val[2],
           addr_val[1], addr_val[0]);
  prov_advertise();
}

static void prov_host_task(void* arg) {
  ESP_LOGI(TAG, "Provisioning BLE host task starting...");
  nimble_port_run();
  nimble_port_freertos_deinit();
}

esp_err_t prov_init(void) {
  ESP_RETURN_ON_ERROR(nimble_port_init(), TAG, "Failed to init NimBLE stack");
  ble_hs_cfg.reset_cb = prov_on_reset;
  ble_hs_cfg.sync_cb = prov_on_sync;
  int ret = ble_svc_gap_device_name_set("test-esp-ble");
  ESP_RETURN_ON_FALSE(ret == 0, ESP_FAIL, TAG, "Failed to set BLE device name, ret=%d", ret);
  return ESP_OK;
  // if (did_init) {
  //   ESP_LOGW(TAG, "Provisioning already initialized");
  //   return ESP_OK;
  // }
  // did_init = true;
  // esp_err_t err;
  // wifi_prov_mgr_config_t cfg = {
  //     .scheme = wifi_prov_scheme_ble,
  //     .scheme_event_handler = WIFI_PROV_SCHEME_BLE_EVENT_HANDLER_FREE_BT,
  // };
  // ESP_RETURN_ON_ERROR(wifi_prov_mgr_init(cfg), TAG, "Failed to init WiFi provisioning mananger");
  // err = esp_event_handler_instance_register(WIFI_PROV_EVENT, ESP_EVENT_ANY_ID, &prov_on_event, NULL, &prov_handler);
  // ESP_RETURN_ON_ERROR(err, TAG, "Failed to register WiFi provisioning event handler");
  // return prov_start();
}

esp_err_t prov_start(void) {
  nimble_port_freertos_init(prov_host_task);
  return ESP_OK;
  // esp_err_t err;
  // bool provisioned = false;
  // ESP_RETURN_ON_ERROR(wifi_prov_mgr_is_provisioned(&provisioned), TAG, "Failed to check if WiFi is provisioned");
  // if (provisioned) {
  //   ESP_LOGI(TAG, "WiFi already provisioned. Skipping");
  //   prov_deinit();
  //   return ESP_OK;
  // }
  // char service_name[12] = {0};
  // uint8_t mac[6];
  // ESP_RETURN_ON_ERROR(esp_wifi_get_mac(WIFI_IF_STA, mac), TAG, "Failed to get MAC");
  // snprintf(service_name, sizeof(service_name), "%s%02X%02X%02X", "PROV_", mac[3], mac[4], mac[5]);
  // ESP_LOGI(TAG, "Sec: %d\tPop: %s\tName: %s", WIFI_PROV_SECURITY_1, POP, service_name);
  // err = wifi_prov_mgr_start_provisioning(WIFI_PROV_SECURITY_1, POP, service_name, NULL);
  // ESP_RETURN_ON_ERROR(err, TAG, "Failed to start WiFi provisioning");
  // return ESP_OK;
}

void prov_stop(void) { /* wifi_prov_mgr_stop_provisioning(); */ }
void prov_reset(void) { /* wifi_prov_mgr_reset_provisioning(); */ }

void prov_deinit(void) {
  // if (!did_init) {
  //   ESP_LOGW(TAG, "Provisioning already deinitialized");
  //   return;
  // }
  // wifi_prov_mgr_deinit();
  // if (prov_handler != NULL) {
  //   esp_event_handler_instance_unregister(WIFI_PROV_EVENT, ESP_EVENT_ANY_ID, prov_handler);
  // }
  // prov_handler = NULL;
}

// static void prov_on_event(void *arg, esp_event_base_t event_base, int32_t event_id, void *event_data) {
//   esp_err_t err;
//   switch (event_id) {
//     case WIFI_PROV_INIT:
//       ESP_LOGI(TAG, "Event: WIFI_PROV_INIT");
//       break;
//     case WIFI_PROV_START:
//       ESP_LOGI(TAG, "Event: WIFI_PROV_START");
//       break;
//     case WIFI_PROV_CRED_RECV:
//       ESP_LOGI(TAG, "Event: WIFI_PROV_CRED_RECV");
//       wifi_sta_config_t *wifi_sta_cfg = (wifi_sta_config_t *)event_data;
//       ESP_LOGI(TAG,
//                "Received Wi-Fi credentials"
//                "\n\tSSID     : %s\n\tPassword : %s",
//                (const char *)wifi_sta_cfg->ssid, (const char *)wifi_sta_cfg->password);
//       break;
//     case WIFI_PROV_CRED_SUCCESS:
//       ESP_LOGI(TAG, "Event: WIFI_PROV_CRED_SUCCESS");
//       break;
//     case WIFI_PROV_CRED_FAIL:
//       ESP_LOGI(TAG, "Event: WIFI_PROV_CRED_FAIL");
//       wifi_prov_sta_fail_reason_t *reason = (wifi_prov_sta_fail_reason_t *)event_data;
//       ESP_LOGE(TAG, "Provisioning failed!\n\tReason : %s",
//                (*reason == WIFI_PROV_STA_AUTH_ERROR) ? "Wi-Fi station authentication failed"
//                                                      : "Wi-Fi access-point not found");
//       err = wifi_prov_mgr_reset_sm_state_on_failure();
//       if(err != ESP_OK) {
//         ESP_LOGE(TAG, "Failed to restart provisioning after failure");
//       }
//       break;
//     case WIFI_PROV_END:
//       ESP_LOGI(TAG, "Event: WIFI_PROV_END");
//       bool provisioned = true;
//       err = wifi_prov_mgr_is_provisioned(&provisioned);
//       if (err != ESP_OK) {
//         ESP_LOGW(TAG, "Failed to check if provisioning was successful");
//         break;
//       }
//       if (provisioned) prov_deinit();
//       break;
//     case WIFI_PROV_DEINIT:
//       ESP_LOGI(TAG, "Event: WIFI_PROV_DEINIT");
//       break;
//   }
// }
