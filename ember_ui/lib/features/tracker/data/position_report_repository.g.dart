// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'position_report_repository.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$positionReportRepositoryHash() =>
    r'0e4e864d2abe8c58430c19ca5a0f103a111d578b';

/// See also [PositionReportRepository].
@ProviderFor(PositionReportRepository)
final positionReportRepositoryProvider = AutoDisposeStreamNotifierProvider<
    PositionReportRepository, List<AprsPosition>>.internal(
  PositionReportRepository.new,
  name: r'positionReportRepositoryProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$positionReportRepositoryHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$PositionReportRepository
    = AutoDisposeStreamNotifier<List<AprsPosition>>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
