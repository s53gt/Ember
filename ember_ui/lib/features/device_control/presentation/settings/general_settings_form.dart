import 'package:ember_ui/features/aprs_symbol/presentation/aprs_symbol_picker_button.dart';
import 'package:ember_ui/features/device_control/application/device_service.dart';
import 'package:ember_ui/features/device_control/presentation/settings/general_settings_form_controller.dart';
import 'package:ember_ui/features/device_control/presentation/settings/save_button.dart';
import 'package:ember_ui/features/device_control/presentation/settings/settings_text_form_field.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class GeneralSettingsForm extends ConsumerStatefulWidget {
  const GeneralSettingsForm({super.key});

  @override
  ConsumerState<ConsumerStatefulWidget> createState() =>
      _GeneralSettingsFormState();
}

class _GeneralSettingsFormState extends ConsumerState<GeneralSettingsForm> {
  final _formKey = GlobalKey<FormState>();

  String? _validateCallsign(String? value) {
    final localizations = AppLocalizations.of(context)!;
    return value != null && value.isNotEmpty
        ? null
        : localizations.callsignInvalid;
  }

  String? _validatePasscode(String? value) {
    final localizations = AppLocalizations.of(context)!;
    return value != null && value.isNotEmpty
        ? null
        : localizations.passcodeInvalid;
  }

  @override
  Widget build(BuildContext context) {
    final localizations = AppLocalizations.of(context)!;
    final controller = ref.watch(generalSettingsFormControllerProvider);
    final controllerNotif =
        ref.read(generalSettingsFormControllerProvider.notifier);
    controllerNotif.formKey = _formKey;
    final deviceService = ref.watch(deviceServiceProvider);
    return Form(
      key: _formKey,
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                localizations.general,
                style: const TextStyle(fontSize: 18),
              ),
              SaveButton(
                tooltip: localizations.saveChanges,
                disabled: deviceService.connectedDevice == null ||
                    controller.isLoading,
                onPressed: controllerNotif.onSave,
                loading: controller.isLoading,
              ),
            ],
          ),
          const SizedBox(height: 10),
          SettingsTextFormField(
            name: localizations.callsign,
            help: localizations.callsignHelp,
            readOnly:
                deviceService.connectedDevice == null || controller.isLoading,
            controller: controller.value?.callsignController,
            validator: _validateCallsign,
            textCapitalization: TextCapitalization.characters,
            inputFormatters: [
              FilteringTextInputFormatter.allow(RegExp(r'[A-Z0-9\-]')),
              LengthLimitingTextInputFormatter(10),
            ],
            suffix: const Padding(
              padding: EdgeInsets.only(left: 3),
              child: AprsSymbolPickerButton(),
            ),
          ),
          const SizedBox(height: 20),
          SettingsTextFormField(
            name: localizations.aprsIsPasscode,
            help: localizations.passcodeHelp,
            readOnly:
                deviceService.connectedDevice == null || controller.isLoading,
            controller: controller.value?.passcodeController,
            password: true,
            validator: _validatePasscode,
            keyboardType: TextInputType.number,
            inputFormatters: [
              FilteringTextInputFormatter.allow(RegExp(r'^-?[0-9]*')),
              LengthLimitingTextInputFormatter(5),
            ],
          ),
        ],
      ),
    );
  }
}
