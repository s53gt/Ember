#include "services/ble_svc_wifi.h"

#include <stdint.h>
#include <string.h>

#include "cc.h"
#include "ember_ble_private.h"
#include "esp_check.h"
#include "esp_event.h"
#include "esp_log.h"
#include "esp_mac.h"
#include "esp_netif_ip_addr.h"
#include "esp_netif_types.h"
#include "esp_wifi_types.h"
#include "freertos/idf_additions.h"
#include "freertos/projdefs.h"
#include "host/ble_att.h"
#include "host/ble_gatt.h"
#include "host/ble_hs_mbuf.h"
#include "os/os_mbuf.h"
#include "portmacro.h"
#include "wifi.h"

static const char *TAG = "ble_svc_wifi";

static int ble_wifi_access(uint16_t conn_handle, uint16_t attr_handle, struct ble_gatt_access_ctxt *ctx, void *arg);

static esp_event_handler_instance_t wifi_events;
static esp_event_handler_instance_t ip_events;
static ble_wifi_net_state_t state = {.status = BLE_WIFI_CONN_STATE_DISCONNECTED, .rssi = -100};
static SemaphoreHandle_t state_mutex = NULL;

static uint16_t wifi_scan_chr_val_handle;
static uint16_t wifi_connect_chr_val_handle;
const struct ble_gatt_svc_def wifi_svc = {
    .type = BLE_GATT_SVC_TYPE_PRIMARY,
    .uuid = &wifi_svc_uuid.u,
    .characteristics =
        (struct ble_gatt_chr_def[]){
            {
                .uuid = &wifi_scan_chr_uuid.u,
                .access_cb = ble_wifi_access,
                .val_handle = &wifi_scan_chr_val_handle,
                .flags = BLE_GATT_CHR_F_READ | BLE_GATT_CHR_F_READ_ENC,
            },
            {
                .uuid = &wifi_connect_chr_uuid.u,
                .access_cb = ble_wifi_access,
                .val_handle = &wifi_connect_chr_val_handle,
                .flags = BLE_GATT_CHR_F_READ | BLE_GATT_CHR_F_READ_ENC | BLE_GATT_CHR_F_WRITE |
                         BLE_GATT_CHR_F_WRITE_ENC | BLE_GATT_CHR_F_NOTIFY,
            },
            {0},
        },
};

static int ble_wifi_scan(uint16_t conn_handle, struct os_mbuf *om) {
  // TODO Mutex lock? Or even better...use one characteristic to start a scan and another to read results. This could
  // potentially handle concurrent reads better. However, this is only an edge case which shouldn't occur with the
  // accompanying Ember UI app.
  static int long_read_cnt = 0;
  static uint16_t nr_aps = 32;
  static ble_wifi_scan_result_t *results = NULL;
  static uint16_t total_len;
  // First call, start the scan
  if (long_read_cnt == 0) {
    wifi_ap_record_t *ap_info = malloc(nr_aps * sizeof(wifi_ap_record_t));
    if (!ap_info) return BLE_ATT_ERR_INSUFFICIENT_RES;
    memset(ap_info, 0, nr_aps * sizeof(wifi_ap_record_t));
    esp_err_t err = wifi_scan(&nr_aps, ap_info);
    if (err != ESP_OK) {
      ESP_LOGE(TAG, "WiFi scan failed");
      free(ap_info);
      return BLE_ATT_ERR_INSUFFICIENT_RES;
    }
    wifi_print_scan_results(nr_aps, ap_info);
    total_len = nr_aps * sizeof(ble_wifi_scan_result_t) + sizeof(ble_wifi_header_t);
    results = malloc(total_len);
    if (!results) {
      ESP_LOGE(TAG, "Failed to malloc scan results response");
      free(ap_info);
      return BLE_ATT_ERR_INSUFFICIENT_RES;
    }
    memset(results, 0, total_len);
    for (uint8_t i = 0; i < nr_aps; i++) {
      wifi_ap_record_t *ap = &ap_info[i];
      ble_wifi_scan_result_t *res = &((ble_wifi_scan_result_t *)((void *)results + sizeof(ble_wifi_header_t)))[i];
      memcpy(res->ssid, ap->ssid, MAX_SSID_LEN);
      res->rssi = ap->rssi;
      res->auth_mode = ap->authmode;
    }
    free(ap_info);
  }
  int mtu = ble_att_mtu(conn_handle);
  int rc = os_mbuf_append(om, results, total_len);
  // Last call in a long read or end of single call; time to cleanup & reset
  // Adapted from https://github.com/espressif/esp-idf/issues/12112#issuecomment-1702467586
  if (long_read_cnt++ == total_len / (mtu - 1)) {
    free(results);
    nr_aps = 32;
    total_len = 0;
    long_read_cnt = 0;
  }
  if (rc != 0) {
    ESP_LOGE(TAG, "Failed to send WiFi scan results over BLE");
    return BLE_ATT_ERR_INSUFFICIENT_RES;
  }
  return 0;
}

static int ble_wifi_read_net_state(uint16_t conn_handle, struct os_mbuf *om) {
  static ble_wifi_net_state_t *cpy = NULL;
  static int long_read_cnt = 0;
  if (cpy == NULL) {
    cpy = malloc(sizeof(ble_wifi_net_state_t));
    if (xSemaphoreTake(state_mutex, 10000 / portTICK_PERIOD_MS) != pdPASS) return BLE_ATT_ERR_INSUFFICIENT_RES;
    memcpy(cpy, &state, sizeof(ble_wifi_net_state_t));
    xSemaphoreGive(state_mutex);
  }
  int mtu = ble_att_mtu(conn_handle);
  int rc = os_mbuf_append(om, cpy, sizeof(ble_wifi_net_state_t));
  // Last call in a long read or end of single call; time to cleanup & reset
  // Adapted from https://github.com/espressif/esp-idf/issues/12112#issuecomment-1702467586
  // TODO Is this correct? we're doing integer division, so any remainder during division will be truncated. Should it
  // not be rounded up?
  if (long_read_cnt++ == sizeof(ble_wifi_net_state_t) / (mtu - 1)) {
    free(cpy);
    cpy = NULL;
    long_read_cnt = 0;
  }
  return rc == 0 ? 0 : BLE_ATT_ERR_INSUFFICIENT_RES;
}

static int ble_wifi_connect(struct os_mbuf *om) {
  esp_err_t err;
  int rc;
  uint16_t len = OS_MBUF_PKTLEN(om);
  if (len != sizeof(ble_wifi_connect_request_t)) return BLE_ATT_ERR_INVALID_ATTR_VALUE_LEN;
  ble_wifi_connect_request_t request = {0};
  rc = ble_hs_mbuf_to_flat(om, &request, sizeof(ble_wifi_connect_request_t), &len);
  if (rc != 0) return BLE_ATT_ERR_UNLIKELY;
  err = wifi_set_ap_creds((uint8_t *)request.name, (uint8_t *)request.password, WIFI_AUTH_OPEN);
  if (err != ESP_OK) return BLE_ATT_ERR_INSUFFICIENT_RES;
  err = wifi_connect();
  if (err != ESP_OK) return BLE_ATT_ERR_INSUFFICIENT_RES;
  return 0;
}

static int ble_wifi_access(uint16_t conn_handle, uint16_t attr_handle, struct ble_gatt_access_ctxt *ctx, void *arg) {
  switch (ctx->op) {
    case BLE_GATT_ACCESS_OP_READ_CHR:
      if (attr_handle == wifi_scan_chr_val_handle) {
        return ble_wifi_scan(conn_handle, ctx->om);
      } else if (attr_handle == wifi_connect_chr_val_handle) {
        return ble_wifi_read_net_state(conn_handle, ctx->om);
      }
      return BLE_ATT_ERR_UNLIKELY;
      break;
    case BLE_GATT_ACCESS_OP_WRITE_CHR:
      if (attr_handle == wifi_connect_chr_val_handle) {
        return ble_wifi_connect(ctx->om);
      }
      return BLE_ATT_ERR_UNLIKELY;
      break;
  }
  return BLE_ATT_ERR_UNLIKELY;
}

static void ble_svc_wifi_update_conn_state(ble_wifi_conn_status_t conn_state) {
  xSemaphoreTake(state_mutex, portMAX_DELAY);
  esp_err_t err = wifi_get_ap_ssid((uint8_t *)state.ssid);
  if (err != ESP_OK) {
    ESP_LOGW(TAG, "Failed to read network state");
    memset(state.ssid, 0, MAX_SSID_LEN);
  }
  int rssi = -100;
  wifi_get_rssi(&rssi);  // Ignore errors; call fails if we're not connected to AP
  state.rssi = (int8_t)rssi;
  state.status = conn_state;
  if (conn_state == BLE_WIFI_CONN_STATE_DISCONNECTED) {
    state.ip_address = 0;
    state.netmask = 0;
    state.gateway = 0;
  }
  ESP_LOGI(TAG, "Updated WiFi state, ssid=%.*s, rssi=%d, status=0x%X", MAX_SSID_LEN, state.ssid, rssi, state.status);
  xSemaphoreGive(state_mutex);
  ember_ble_notify(wifi_connect_chr_val_handle);
}

static void ble_svc_wifi_update_net_state(uint32_t ip, uint32_t netmask, uint32_t gw) {
  if (ip && state.status != BLE_WIFI_CONN_STATE_CONNECTED) {
    ble_svc_wifi_update_conn_state(BLE_WIFI_CONN_STATE_CONNECTED);
  }
  xSemaphoreTake(state_mutex, portMAX_DELAY);
  state.ip_address = ip;
  state.netmask = 0;
  // Compress netmask into CIDR suffix
  for (uint32_t mask = netmask; mask > 0; mask >>= 1) {
    state.netmask++;
  }
  state.gateway = gw;
  xSemaphoreGive(state_mutex);
  ember_ble_notify(wifi_connect_chr_val_handle);
}

static void ble_svc_wifi_on_event(void *arg, esp_event_base_t event_base, int32_t event_id, void *event_data) {
  switch (event_id) {
    case WIFI_EVENT_STA_CONNECTED:
      ble_svc_wifi_update_conn_state(BLE_WIFI_CONN_STATE_CONNECTED);
      break;
    case WIFI_EVENT_STA_DISCONNECTED:
      ble_svc_wifi_update_conn_state(BLE_WIFI_CONN_STATE_DISCONNECTED);
      break;
  }
}

static void ble_svc_wifi_on_ip_event(void *arg, esp_event_base_t event_base, int32_t event_id, void *event_data) {
  switch (event_id) {
    case IP_EVENT_STA_GOT_IP: {
      ip_event_got_ip_t *e = (ip_event_got_ip_t *)event_data;
      ble_svc_wifi_update_net_state(e->ip_info.ip.addr, e->ip_info.netmask.addr, e->ip_info.gw.addr);
      break;
    }
    case IP_EVENT_STA_LOST_IP:
      ble_svc_wifi_update_net_state(0, 0, 0);
      break;
  }
}

esp_err_t ble_svc_wifi_init(void) {
  state_mutex = xSemaphoreCreateMutex();
  esp_err_t err = esp_read_mac(state.mac_address, ESP_MAC_WIFI_STA);
  ESP_RETURN_ON_ERROR(err, TAG, "Failed to read WiFi STA MAC address");
  err = esp_event_handler_instance_register(WIFI_EVENT, ESP_EVENT_ANY_ID, &ble_svc_wifi_on_event, NULL, &wifi_events);
  ESP_RETURN_ON_ERROR(err, TAG, "Failed to register WiFi event handler");
  err = esp_event_handler_instance_register(IP_EVENT, ESP_EVENT_ANY_ID, &ble_svc_wifi_on_ip_event, NULL, &ip_events);
  ESP_RETURN_ON_ERROR(err, TAG, "Failed to register IP event handler");
  return ESP_OK;
}
