import 'package:ember_ui/features/aprs_symbol/presentation/aprs_symbol.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class AprsSymbolButton extends ConsumerWidget {
  final String tableChr;
  final String symbolChr;
  final VoidCallback onPressed;
  final bool selected;

  const AprsSymbolButton({
    super.key,
    required this.tableChr,
    required this.symbolChr,
    required this.onPressed,
    this.selected = false,
  });

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return IconButton(
      onPressed: onPressed,
      style: ButtonStyle(
        backgroundColor: selected
            ? WidgetStatePropertyAll(
                Theme.of(context).colorScheme.primary,
              )
            : null,
        shape: WidgetStatePropertyAll(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5.0),
          ),
        ),
      ),
      tooltip: "$tableChr$symbolChr".trim(),
      icon: AprsSymbol(tableChr: tableChr, symbolChr: symbolChr),
      color: Theme.of(context).colorScheme.primary,
      padding: const EdgeInsets.all(8),
    );
  }
}
