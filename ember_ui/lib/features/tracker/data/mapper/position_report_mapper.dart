import 'package:ember_ui/features/database/data/database.dart';
import 'package:ember_ui/features/tracker/domain/aprs_position.dart';
import 'package:ember_ui/features/tracker/domain/tnc2_header.dart';

class PositionReportMapper {
  static AprsPosition transformToModel(PositionReportData report) =>
      AprsPosition(
        header: Tnc2Header(
          from: report.fromCall,
          to: report.toCall,
          path: report.path,
        ),
        comment: report.comment,
        latitude: report.latitude,
        longitude: report.longitude,
        tableChr: report.tableChr,
        symbolChr: report.symbolChr,
        altitude: report.altitude,
        ambiguity: report.ambiguity,
        timestamp: report.timestamp,
      );

  static List<AprsPosition> transformToModelList(
          List<PositionReportData> report) =>
      report.map(transformToModel).toList();
}
