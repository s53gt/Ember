import 'package:ember_ui/features/aprs_symbol/presentation/aprs_symbol_editor.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

Future<String?> showAprsSymbolDialog(BuildContext context) async {
  // Is this an anti-pattern? ...guess we'll find out :)
  final TextEditingController controller = TextEditingController();
  final symbol = await showDialog(
    context: context,
    builder: (context) {
      final localizations = AppLocalizations.of(context)!;
      return LayoutBuilder(
        builder: (context, constraints) => AlertDialog(
          title: Text(localizations.aprsSymbol),
          content: SizedBox(
            width: 200,
            height: 300,
            child: AprsSymbolEditor(controller: controller),
          ),
          actions: <Widget>[
            TextButton(
              onPressed: () => Navigator.pop(context, controller.text),
              child: Text(localizations.confirm),
            ),
            TextButton(
              onPressed: () => Navigator.pop(context),
              child: Text(localizations.cancel),
            ),
          ],
        ),
      );
    },
  );
  controller.dispose();
  return symbol;
}
