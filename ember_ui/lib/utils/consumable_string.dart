class ConsumableString with Iterable implements Iterator {
  final String _value;
  int _index = -1;
  int _mark = -1;

  ConsumableString(this._value);

  @override
  get current => _value[_index];

  @override
  Iterator get iterator => this;

  @override
  bool get isNotEmpty => _index < _value.length;

  @override
  bool get isEmpty => !isNotEmpty;

  @override
  bool moveNext() => ++_index < _value.length;

  void mark() => _mark = _index;
  void rollback() => _index = _mark;
}
