#include "ember_ble_gatts.h"

#include <assert.h>
#include <stdio.h>
#include <string.h>

#include "esp_app_desc.h"
#include "esp_check.h"
#include "esp_log.h"
#include "host/ble_gatt.h"
#include "services/ans/ble_svc_ans.h"
#include "services/bas/ble_svc_bas.h"
#include "services/ble_svc_ham_sta.h"
#include "services/ble_svc_tnc2.h"
#include "services/ble_svc_wifi.h"
#include "services/dis/ble_svc_dis.h"
#include "services/gap/ble_svc_gap.h"
#include "services/gatt/ble_svc_gatt.h"

static const char *TAG = "ember_ble_gatts";

#define NR_SERVICES 3

static struct ble_gatt_svc_def ember_ble_gatts_svcs[NR_SERVICES + 1] = {0};

void ember_ble_on_gatts_register(struct ble_gatt_register_ctxt *ctx, void *arg) {
  char buf[BLE_UUID_STR_LEN];

  switch (ctx->op) {
    case BLE_GATT_REGISTER_OP_SVC:
      ESP_LOGD(TAG, "registered service %s with handle=%d\n", ble_uuid_to_str(ctx->svc.svc_def->uuid, buf),
               ctx->svc.handle);
      break;

    case BLE_GATT_REGISTER_OP_CHR:
      ESP_LOGD(TAG,
               "registering characteristic %s with "
               "def_handle=%d val_handle=%d\n",
               ble_uuid_to_str(ctx->chr.chr_def->uuid, buf), ctx->chr.def_handle, ctx->chr.val_handle);
      break;

    case BLE_GATT_REGISTER_OP_DSC:
      ESP_LOGD(TAG, "registering descriptor %s with handle=%d\n", ble_uuid_to_str(ctx->dsc.dsc_def->uuid, buf),
               ctx->dsc.handle);
      break;

    default:
      ESP_LOGW(TAG, "Unknown GATTS ctx op: 0x%02X", ctx->op);
      break;
  }
}

esp_err_t ember_ble_gatts_set_batt_level(uint8_t level) {
  int rc = ble_svc_bas_battery_level_set(level);
  ESP_RETURN_ON_FALSE(rc == 0, ESP_FAIL, TAG, "Failed to set battery level, lvl=%d", level);
  return ESP_OK;
}

esp_err_t ember_ble_gatts_init(void) {
  ble_svc_gap_init();
  ble_svc_gatt_init();
  ble_svc_ans_init();
  ble_svc_bas_init();
  ble_svc_dis_init();
  ble_svc_wifi_init();
  ble_svc_tnc2_init();
  ble_svc_ham_sta_init();
  const esp_app_desc_t *app_desc = esp_app_get_description();
  assert(app_desc != NULL);
  char *fw_revision = malloc(66);
  assert(fw_revision != NULL);
  memset(fw_revision, 0, 66);
  int rc = sprintf(fw_revision, "%s %s", app_desc->project_name, app_desc->version);
  assert(rc > 0);
  rc = ble_svc_dis_model_number_set(fw_revision);
  assert(rc == 0);
  // Copy all service definitions
  memcpy(&ember_ble_gatts_svcs[0], &ham_sta_svc, sizeof(const struct ble_gatt_svc_def));
  memcpy(&ember_ble_gatts_svcs[1], &wifi_svc, sizeof(const struct ble_gatt_svc_def));
  memcpy(&ember_ble_gatts_svcs[2], &tnc2_svc, sizeof(const struct ble_gatt_svc_def));
  // ----------------------------
  rc = ble_gatts_count_cfg(ember_ble_gatts_svcs);
  ESP_RETURN_ON_FALSE(rc == 0, ESP_FAIL, TAG, "Failed to count gatts cfg, rc=%d", rc);
  rc = ble_gatts_add_svcs(ember_ble_gatts_svcs);
  ESP_RETURN_ON_FALSE(rc == 0, ESP_FAIL, TAG, "Failed to add gatts svcs, rc=%d", rc);
  return ESP_OK;
}
