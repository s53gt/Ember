// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'aprs_symbol_repository.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$aprsSymbolRepositoryHash() =>
    r'e3fa658582e475542ea06292243a46190c9c6766';

/// See also [aprsSymbolRepository].
@ProviderFor(aprsSymbolRepository)
final aprsSymbolRepositoryProvider =
    AutoDisposeProvider<AprsSymbolRepository>.internal(
  aprsSymbolRepository,
  name: r'aprsSymbolRepositoryProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$aprsSymbolRepositoryHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef AprsSymbolRepositoryRef = AutoDisposeProviderRef<AprsSymbolRepository>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
