import 'package:ember_ui/exceptions/malformed_exception.dart';

class VersionedStructArray extends Iterable implements Iterator<List<int>> {
  late final int version;
  late final List<int> data;
  final int structSize;
  int _idx = -1;

  VersionedStructArray(this.version, this.data, this.structSize) {
    if (data.length % structSize != 0) {
      throw MalformedException();
    }
  }

  VersionedStructArray.fromBytes(List<int> bytes, this.structSize) {
    version = bytes.first;
    data = bytes.sublist(4); // Skip 1 version byte + 3 reserved
    if (data.length % structSize != 0) {
      throw MalformedException();
    }
  }

  List<int> operator [](int index) {
    return data.sublist(index * structSize, (index + 1) * structSize);
  }

  @override
  int get length => data.length ~/ structSize;

  @override
  List<int> get current => this[_idx];

  @override
  bool moveNext() {
    if (++_idx >= length) return false;
    return true;
  }

  @override
  Iterator get iterator => this;
}
