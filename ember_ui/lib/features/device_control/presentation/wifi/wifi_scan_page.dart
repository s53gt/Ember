import 'package:ember_ui/features/device_control/presentation/wifi/wifi_scan_page_controller.dart';
import 'package:ember_ui/features/device_control/presentation/wifi/wifi_scan_result.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class WifiScanPage extends ConsumerStatefulWidget {
  const WifiScanPage({super.key});

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _WifiScanPageState();
}

class _WifiScanPageState extends ConsumerState<WifiScanPage> {
  @override
  Widget build(BuildContext context) {
    final localizations = AppLocalizations.of(context)!;
    final scan = ref.watch(wifiScanPageControllerProvider);
    final controller = ref.read(wifiScanPageControllerProvider.notifier);
    return Padding(
      padding: const EdgeInsets.fromLTRB(10, 20, 10, 20),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                localizations.wifiNetworks,
                style: const TextStyle(fontSize: 23),
              ),
              IconButton(
                onPressed: scan.isLoading ? null : controller.refresh,
                icon: const Icon(Icons.refresh),
              ),
            ],
          ),
          const Divider(),
          if (scan.valueOrNull == null || scan.valueOrNull!.isEmpty)
            Padding(
                padding: const EdgeInsets.all(10),
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      if (scan.isLoading) ...[
                        const SizedBox(
                          width: 16,
                          height: 16,
                          child: CircularProgressIndicator(
                            strokeWidth: 2,
                          ),
                        ),
                        const SizedBox(
                          width: 10,
                        ),
                      ],
                      Text(scan.isLoading
                          ? localizations.lookingForNetworks
                          : localizations.noResults)
                    ]))
          else
            Expanded(
                child: ListView(
              shrinkWrap: true,
              children: (scan.valueOrNull ?? [])
                  .map((r) => WifiScanResult(net: r))
                  .toList(),
            )),
        ],
      ),
    );
  }
}
