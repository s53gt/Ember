// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'wifi_settings_controller.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$wifiSettingsControllerHash() =>
    r'bb3ac8eaf0db75a83fe392b893c6236281a40f9b';

/// See also [WifiSettingsController].
@ProviderFor(WifiSettingsController)
final wifiSettingsControllerProvider = AutoDisposeStreamNotifierProvider<
    WifiSettingsController, WifiState>.internal(
  WifiSettingsController.new,
  name: r'wifiSettingsControllerProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$wifiSettingsControllerHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$WifiSettingsController = AutoDisposeStreamNotifier<WifiState>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
