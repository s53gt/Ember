import 'package:freezed_annotation/freezed_annotation.dart';

part 'wifi_network.freezed.dart';

// Order matters; taken from ESP-IDF
enum WifiAuthMode {
  wifiAuthOpen,
  wifiAuthWep,
  wifiAuthWpaPsk,
  wifiAuthWpa2Psk,
  wifiAuthWpaWpa2Psk,
  wifiAuthEnterprise,
  wifiAuthWpa3Psk,
  wifiAuthWpa2Wpa3Psk,
  wifiAuthWapiPsk,
  wifiAuthOwe,
  wifiAuthWpa3Ent192,
  wifiAuthWpa3RxtPsk,
  wifiAuthWpa3RxtPskMixedMode,
  wifiAuthMax
}

/// 32 byte name + 1 byte rssi + 1 byte auth mode
const wifiNetworkSize = 32 + 1 + 1;

@freezed
class WifiNetwork with _$WifiNetwork {
  const factory WifiNetwork({
    required String ssid,
    required int rssi,
    required WifiAuthMode authMode,
  }) = _WifiNetwork;
}
