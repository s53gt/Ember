// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'current_aprs_symbol.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$currentAprsSymbolHash() => r'6469468c9ce1d723f46da413ebdee880de4f5ca1';

/// See also [CurrentAprsSymbol].
@ProviderFor(CurrentAprsSymbol)
final currentAprsSymbolProvider =
    AutoDisposeAsyncNotifierProvider<CurrentAprsSymbol, String>.internal(
  CurrentAprsSymbol.new,
  name: r'currentAprsSymbolProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$currentAprsSymbolHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$CurrentAprsSymbol = AutoDisposeAsyncNotifier<String>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
