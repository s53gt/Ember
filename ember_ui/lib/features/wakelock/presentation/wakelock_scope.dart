import 'package:ember_ui/features/wakelock/data/wakelock_repository.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class WakelockScope extends ConsumerStatefulWidget {
  final Widget child;

  const WakelockScope({super.key, required this.child});

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _WakelockScopeState();
}

class _WakelockScopeState extends ConsumerState<WakelockScope> {
  @override
  void initState() {
    super.initState();
    final wakelockRepository = ref.read(wakelockRepositoryProvider);
    wakelockRepository.enable();
  }

  @override
  void activate() {
    super.activate();
    final wakelockRepository = ref.read(wakelockRepositoryProvider);
    wakelockRepository.enable();
  }

  @override
  void deactivate() {
    final wakelockRepository = ref.read(wakelockRepositoryProvider);
    wakelockRepository.disable();
    super.deactivate();
  }

  @override
  Widget build(BuildContext context) => widget.child;
}
