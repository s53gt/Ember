#include "wifi.h"

#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "esp_check.h"
#include "esp_err.h"
#include "esp_event.h"
#include "esp_event_base.h"
#include "esp_log.h"
#include "esp_netif_ip_addr.h"
#include "esp_netif_types.h"
#include "esp_wifi.h"
#include "esp_wifi_default.h"
#include "esp_wifi_types.h"
#include "freertos/FreeRTOS.h"
#include "freertos/event_groups.h"
#include "freertos/task.h"

#define WARN_ON_ERR(X)                                                                   \
  {                                                                                      \
    esp_err_t err = (X);                                                                 \
    if (err != ESP_OK) ESP_LOGW(TAG, #X " returned %d (%s)", err, esp_err_to_name(err)); \
  }

static const char* TAG = "wifi";

static EventGroupHandle_t events = NULL;
static esp_event_handler_instance_t wifi_events;
static esp_event_handler_instance_t ip_events;
static esp_netif_t* netif = NULL;
static bool is_init = false;

static void on_wifi_event(void* arg, esp_event_base_t event_base, int32_t event_id, void* event_data) {
  switch (event_id) {
    case WIFI_EVENT_STA_START:
      ESP_LOGI(TAG, "WiFi station started");
      break;
    case WIFI_EVENT_STA_STOP:
      ESP_LOGI(TAG, "WiFi station stopped");
      break;
    case WIFI_EVENT_HOME_CHANNEL_CHANGE:
      ESP_LOGI(TAG, "WiFi home channel changed");
      break;
    case WIFI_EVENT_SCAN_DONE:
      ESP_LOGI(TAG, "WiFi scan done");
      break;
    case WIFI_EVENT_STA_CONNECTED:
      ESP_LOGI(TAG, "WiFi connected");
      break;
    case WIFI_EVENT_STA_DISCONNECTED:
      ESP_LOGI(TAG, "WiFi disconnected");
      break;
    default:
      ESP_LOGI(TAG, "Unknown WiFi event ID %ld", event_id);
      break;
  }
}

static void on_ip_event(void* arg, esp_event_base_t event_base, int32_t event_id, void* event_data) {
  switch (event_id) {
    case IP_EVENT_STA_LOST_IP: {
      ESP_LOGI(TAG, "WiFi lost IP address");
      break;
    }
    case IP_EVENT_GOT_IP6: {
      ip_event_got_ip6_t* e = (ip_event_got_ip6_t*)event_data;
      ESP_LOGI(TAG, "Got IPv6, ip=" IPV6STR, IPV62STR(e->ip6_info.ip));
      break;
    }
    case IP_EVENT_STA_GOT_IP: {
      ip_event_got_ip_t* e = (ip_event_got_ip_t*)event_data;
      ESP_LOGI(TAG, "WiFi got IP, ip=" IPSTR ", netmask=" IPSTR ", gw=" IPSTR, IP2STR(&e->ip_info.ip),
               IP2STR(&e->ip_info.netmask), IP2STR(&e->ip_info.gw));
      break;
    }
    default: {
      ESP_LOGI(TAG, "Unknown IP event ID %ld", event_id);
      break;
    }
  }
}

esp_err_t wifi_enable(void) {
  if (is_init) return ESP_OK;
  is_init = true;
  esp_err_t err = ESP_OK;
  events = xEventGroupCreate();
  ESP_RETURN_ON_FALSE(events, ESP_ERR_NO_MEM, TAG, "Failed to allocate heap for WiFi event group");
  netif = esp_netif_create_default_wifi_sta();
  assert(netif);
  wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
  ESP_RETURN_ON_ERROR(esp_wifi_init(&cfg), TAG, "Failed to init WiFi");
  err = esp_event_handler_instance_register(WIFI_EVENT, ESP_EVENT_ANY_ID, &on_wifi_event, NULL, &wifi_events);
  ESP_RETURN_ON_ERROR(err, TAG, "Failed to register WiFi event handler");
  err = esp_event_handler_instance_register(IP_EVENT, ESP_EVENT_ANY_ID, &on_ip_event, NULL, &ip_events);
  ESP_RETURN_ON_ERROR(err, TAG, "Failed to register IP event handler");
  ESP_RETURN_ON_ERROR(esp_wifi_set_mode(WIFI_MODE_STA), TAG, "Failed to switch WiFi into station mode");
  ESP_RETURN_ON_ERROR(esp_wifi_start(), TAG, "Failed to start WiFi");
  return ESP_OK;
}

esp_err_t wifi_disable(void) {
  if (!is_init) return ESP_OK;
  is_init = false;
  WARN_ON_ERR(esp_wifi_disconnect());
  WARN_ON_ERR(esp_wifi_stop());
  WARN_ON_ERR(esp_wifi_deinit());
  esp_netif_destroy_default_wifi(netif);
  netif = NULL;
  WARN_ON_ERR(esp_event_handler_instance_unregister(WIFI_EVENT, ESP_EVENT_ANY_ID, wifi_events));
  wifi_events = NULL;
  WARN_ON_ERR(esp_event_handler_instance_unregister(IP_EVENT, ESP_EVENT_ANY_ID, ip_events));
  ip_events = NULL;
  return ESP_OK;
}

esp_err_t wifi_scan(uint16_t* len, wifi_ap_record_t ap_info[*len]) {
  if (!ap_info || !*len) return ESP_ERR_INVALID_ARG;
  ESP_RETURN_ON_ERROR(esp_wifi_scan_start(NULL, true), TAG, "Failed to start WiFi scan");
  ESP_RETURN_ON_ERROR(esp_wifi_scan_get_ap_records(len, ap_info), TAG, "Failed to get scan AP records");
  return ESP_OK;
}

void wifi_print_scan_results(uint16_t number, wifi_ap_record_t ap_info[number]) {
  ESP_LOGI(TAG, "Total APs scanned = %u", number);
  for (int i = 0; i < number; i++) {
    ESP_LOGI(TAG, "SSID \t\t%s", ap_info[i].ssid);
    ESP_LOGI(TAG, "RSSI \t\t%d", ap_info[i].rssi);
    ESP_LOGI(TAG, "Authmode \t%s", wifi_auth_mode_to_str(ap_info[i].authmode));
    if (ap_info[i].authmode != WIFI_AUTH_WEP) {
      ESP_LOGI(TAG, "Pairwise Cipher \t%s", wifi_cipher_to_str(ap_info[i].pairwise_cipher));
      ESP_LOGI(TAG, "Group Cipher \t%s", wifi_cipher_to_str(ap_info[i].group_cipher));
    }
    ESP_LOGI(TAG, "Channel \t\t%d", ap_info[i].primary);
  }
}

esp_err_t wifi_disconnect(void) {
  ESP_RETURN_ON_ERROR(esp_wifi_disconnect(), TAG, "Failed to disconnect from WiFi");
  return ESP_OK;
}

esp_err_t wifi_connect(void) {
  ESP_RETURN_ON_ERROR(esp_wifi_connect(), TAG, "Failed to connect to WiFi");
  return ESP_OK;
}

esp_err_t wifi_set_ap_creds(uint8_t ssid[MAX_SSID_LEN], uint8_t password[MAX_PASSPHRASE_LEN],
                            wifi_auth_mode_t auth_mode) {
  wifi_config_t wifi_cfg = {.sta = {.threshold.authmode = auth_mode}};
  memcpy(wifi_cfg.sta.ssid, ssid, MAX_SSID_LEN);
  memcpy(wifi_cfg.sta.password, password, MAX_PASSPHRASE_LEN);
  ESP_RETURN_ON_ERROR(esp_wifi_set_config(WIFI_IF_STA, &wifi_cfg), TAG, "Failed to update WiFi config");
  return ESP_OK;
}

esp_err_t wifi_get_ap_ssid(uint8_t ssid[MAX_SSID_LEN]) {
  wifi_config_t cfg = {0};
  ESP_RETURN_ON_ERROR(esp_wifi_get_config(WIFI_IF_STA, &cfg), TAG, "Failed to get WiFi config");
  memcpy(ssid, cfg.sta.ssid, MAX_SSID_LEN);
  return ESP_OK;
}

esp_err_t wifi_get_rssi(int* rssi) { return esp_wifi_sta_get_rssi(rssi); }

const char* wifi_auth_mode_to_str(wifi_auth_mode_t authmode) {
  switch (authmode) {
    case WIFI_AUTH_OPEN:
      return "WIFI_AUTH_OPEN";
    case WIFI_AUTH_OWE:
      return "WIFI_AUTH_OWE";
    case WIFI_AUTH_WEP:
      return "WIFI_AUTH_WEP";
    case WIFI_AUTH_WPA_PSK:
      return "WIFI_AUTH_WPA_PSK";
    case WIFI_AUTH_WPA2_PSK:
      return "WIFI_AUTH_WPA2_PSK";
    case WIFI_AUTH_WPA_WPA2_PSK:
      return "WIFI_AUTH_WPA_WPA2_PSK";
    case WIFI_AUTH_ENTERPRISE:
      return "WIFI_AUTH_ENTERPRISE";
    case WIFI_AUTH_WPA3_PSK:
      return "WIFI_AUTH_WPA3_PSK";
    case WIFI_AUTH_WPA2_WPA3_PSK:
      return "WIFI_AUTH_WPA2_WPA3_PSK";
    case WIFI_AUTH_WPA3_ENT_192:
      return "WIFI_AUTH_WPA3_ENT_192";
    case WIFI_AUTH_WPA3_EXT_PSK:
      return "WIFI_AUTH_WPA3_EXT_PSK";
    case WIFI_AUTH_WPA3_EXT_PSK_MIXED_MODE:
      return "WIFI_AUTH_WPA3_EXT_PSK_MIXED_MODE";
    default:
      return "WIFI_AUTH_UNKNOWN";
  }
}

const char* wifi_cipher_to_str(wifi_cipher_type_t cipher) {
  switch (cipher) {
    case WIFI_CIPHER_TYPE_NONE:
      return "WIFI_CIPHER_TYPE_NONE";
    case WIFI_CIPHER_TYPE_WEP40:
      return "WIFI_CIPHER_TYPE_WEP40";
    case WIFI_CIPHER_TYPE_WEP104:
      return "WIFI_CIPHER_TYPE_WEP104";
    case WIFI_CIPHER_TYPE_TKIP:
      return "WIFI_CIPHER_TYPE_TKIP";
    case WIFI_CIPHER_TYPE_CCMP:
      return "WIFI_CIPHER_TYPE_CCMP";
    case WIFI_CIPHER_TYPE_TKIP_CCMP:
      return "WIFI_CIPHER_TYPE_TKIP_CCMP";
    case WIFI_CIPHER_TYPE_AES_CMAC128:
      return "WIFI_CIPHER_TYPE_AES_CMAC128";
    case WIFI_CIPHER_TYPE_SMS4:
      return "WIFI_CIPHER_TYPE_SMS4";
    case WIFI_CIPHER_TYPE_GCMP:
      return "WIFI_CIPHER_TYPE_GCMP";
    case WIFI_CIPHER_TYPE_GCMP256:
      return "WIFI_CIPHER_TYPE_GCMP256";
    default:
      return "WIFI_CIPHER_TYPE_UNKNOWN";
  }
}
