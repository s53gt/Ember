import 'package:freezed_annotation/freezed_annotation.dart';

part 'tnc2_header.freezed.dart';

@freezed
class Tnc2Header with _$Tnc2Header {
  const factory Tnc2Header({
    required String from,
    required String to,
    required String path,
  }) = _Tnc2Header;
}
