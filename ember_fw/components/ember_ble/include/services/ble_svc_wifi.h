#pragma once

#include "esp_err.h"
#include "esp_wifi_types.h"
#include "host/ble_uuid.h"

// 0b1fec6a-127e-49e6-971d-f37bacb01d79
static const ble_uuid128_t wifi_svc_uuid =
    BLE_UUID128_INIT(0x79, 0x1d, 0xb0, 0xac, 0x7b, 0xf3, 0x1d, 0x97, 0xe6, 0x49, 0x7e, 0x12, 0x6a, 0xec, 0x1f, 0x0b);

// 6c23eac7-cced-4c1f-8faa-ca46a2efd2af
static const ble_uuid128_t wifi_scan_chr_uuid =
    BLE_UUID128_INIT(0xaf, 0xd2, 0xef, 0xa2, 0x46, 0xca, 0xaa, 0x8f, 0x1f, 0x4c, 0xed, 0xcc, 0xc7, 0xea, 0x23, 0x6c);

// 59ef7c38-632c-48a6-94e8-05b5cd0ef94f
static const ble_uuid128_t wifi_connect_chr_uuid =
    BLE_UUID128_INIT(0x4f, 0xf9, 0x0e, 0xcd, 0xb5, 0x05, 0xe8, 0x94, 0xa6, 0x48, 0x2c, 0x63, 0x38, 0x7c, 0xef, 0x59);

extern const struct ble_gatt_svc_def wifi_svc;

typedef struct __attribute__((__packed__)) {
  uint8_t version;
  uint8_t __reserved[3];
} ble_wifi_header_t;

typedef struct __attribute__((__packed__)) {
  char ssid[MAX_SSID_LEN];
  int8_t rssi;
  uint8_t auth_mode;
} ble_wifi_scan_result_t;

typedef struct __attribute__((__packed__)) {
  ble_wifi_header_t header;
  char name[MAX_SSID_LEN];
  char password[MAX_PASSPHRASE_LEN];
} ble_wifi_connect_request_t;

typedef enum {
  BLE_WIFI_CONN_STATE_DISCONNECTED = 0,
  BLE_WIFI_CONN_STATE_CONNECTED,
  BLE_WIFI_CONN_STATE_MAX,
} __attribute__((__packed__)) ble_wifi_conn_status_t;

typedef struct __attribute__((__packed__)) {
  ble_wifi_header_t header;
  char ssid[MAX_SSID_LEN];
  uint32_t ip_address;
  uint32_t gateway;
  uint8_t mac_address[6];
  uint8_t netmask;
  int8_t rssi;
  ble_wifi_conn_status_t status;
} ble_wifi_net_state_t;

esp_err_t ble_svc_wifi_init(void);
