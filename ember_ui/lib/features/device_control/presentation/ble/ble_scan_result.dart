import 'package:ember_ui/features/device_control/application/device_service.dart';
import 'package:ember_ui/features/device_control/domain/device.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class BleScanResult extends ConsumerStatefulWidget {
  final Device device;
  final void Function(Device device) onConnected;

  const BleScanResult(
      {super.key, required this.device, required this.onConnected});

  @override
  ConsumerState<BleScanResult> createState() => _BleScanResultState();
}

class _BleScanResultState extends ConsumerState<BleScanResult> {
  Future<void>? _connectFuture;

  Icon _buildRssiIcon() {
    switch (widget.device.linkQuality) {
      case LinkQuality.poor:
        return const Icon(Icons.signal_cellular_alt_1_bar);
      case LinkQuality.good:
        return const Icon(Icons.signal_cellular_alt_2_bar);
      case LinkQuality.excellent:
        return const Icon(Icons.signal_cellular_alt);
      default:
        return const Icon(Icons.signal_cellular_off);
    }
  }

  Widget? _getSubtitle(AsyncSnapshot snapshot) {
    final localizations = AppLocalizations.of(context)!;
    switch (snapshot.connectionState) {
      case ConnectionState.waiting:
        return Text(localizations.connecting);
      case ConnectionState.done:
        if (snapshot.hasError) {
          return Text(localizations.connectionFailed);
        } else {
          return Text(localizations.connected);
        }
      default:
        if (widget.device.connected == true) {
          return Text(localizations.connected);
        }
        return null;
    }
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: _connectFuture,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done &&
              !snapshot.hasError) {
            WidgetsBinding.instance.addPostFrameCallback((_) {
              widget.onConnected(widget.device);
            });
          } else if (snapshot.connectionState == ConnectionState.done &&
              snapshot.hasError) {
            WidgetsBinding.instance.addPostFrameCallback((_) {
              ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(
                  content: Text(snapshot.error.toString()),
                ),
              );
            });
          }
          return ListTile(
            onTap: (snapshot.connectionState == ConnectionState.waiting)
                ? null
                : () => setState(() {
                      final service = ref.read(deviceServiceProvider);
                      _connectFuture = service.connect(widget.device);
                    }),
            title: Text(widget.device.name),
            subtitle: _getSubtitle(snapshot),
            trailing: _buildRssiIcon(),
          );
        });
  }
}
