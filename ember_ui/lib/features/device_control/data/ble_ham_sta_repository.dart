import 'dart:convert';

import 'package:ember_ui/constants/ble_constants.dart';
import 'package:ember_ui/features/device_control/data/ham_sta_repository.dart';
import 'package:ember_ui/features/device_control/domain/device.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:universal_ble/universal_ble.dart';

part 'ble_ham_sta_repository.g.dart';

class BleHamStaRepository implements HamStaRepository {
  @override
  Future<String> getCallsign(Device device) async {
    final buff = await UniversalBle.readValue(
      device.id,
      bleHamStaServiceUid,
      bleHamStaCallsignChrUid,
    );
    return utf8.decode(buff.toList());
  }

  @override
  Future<void> setCallsign(Device device, String callsign) async {
    await UniversalBle.writeValue(
      device.id,
      bleHamStaServiceUid,
      bleHamStaCallsignChrUid,
      utf8.encode(callsign),
      BleOutputProperty.withResponse,
    );
  }

  @override
  Future<String> getPasscode(Device device) async {
    final buff = await UniversalBle.readValue(
      device.id,
      bleHamStaServiceUid,
      bleHamStaPasscodeChrUid,
    );
    return utf8.decode(buff.toList());
  }

  @override
  Future<void> setPasscode(Device device, String passcode) async {
    await UniversalBle.writeValue(
      device.id,
      bleHamStaServiceUid,
      bleHamStaPasscodeChrUid,
      utf8.encode(passcode),
      BleOutputProperty.withResponse,
    );
  }

  @override
  Future<String> getSymbol(Device device) async {
    final buff = await UniversalBle.readValue(
      device.id,
      bleHamStaServiceUid,
      bleHamStaSymbolChrUid,
    );
    return utf8.decode(buff.toList());
  }

  @override
  Future<void> setSymbol(Device device, String symbol) async {
    await UniversalBle.writeValue(
      device.id,
      bleHamStaServiceUid,
      bleHamStaSymbolChrUid,
      utf8.encode(symbol),
      BleOutputProperty.withResponse,
    );
  }
}

@riverpod
BleHamStaRepository bleHamStaRepository(BleHamStaRepositoryRef ref) {
  return BleHamStaRepository();
}
