#include "repl.h"

#include <stdio.h>

#include "commands/gnss.h"
#include "commands/ota.h"
#include "commands/provisioning.h"
#include "commands/pwr.h"
#include "commands/settings.h"
#include "commands/wifi.h"
#include "esp_check.h"
#include "esp_console.h"
#include "esp_err.h"
#include "esp_log.h"
#include "gnss.h"
#include "linenoise/linenoise.h"
#include "lora.h"
#include "portmacro.h"

static int repl_clear_cmd(int argc, char **argv);
// static int repl_test_cmd(int argc, char **argv);

static esp_console_repl_t *repl = NULL;
static esp_console_cmd_t clear_cmd = {
    .command = "clear",
    .help = "Clears the screen",
    .hint = NULL,
    .func = &repl_clear_cmd,
    .argtable = NULL,
};
// static esp_console_cmd_t test_cmd = {
//   .command = "test",
//   .help = "Test APRS",
//   .hint = NULL,
//   .func = &repl_test_cmd,
//   .argtable = NULL,
// };

static int repl_clear_cmd(int argc, char **argv) {
  linenoiseClearScreen();
  return 0;
}

// static int repl_test_cmd(int argc, char **argv) {
//   gnss_fix_t fix;
//   esp_err_t err = gnss_get_fix(&fix, 10000 / portTICK_PERIOD_MS);
//   ESP_RETURN_ON_ERROR(err, "repl", "Failed to get GNSS fix");
//   if(!fix.fix) {
//     ESP_LOGE("repl", "GNSS has no fix");
//     return 0;
//   }
//   char lat[9] = {0};
//   char lon[10] = {0};
//   char *msg = "Hello LoRa!";
//   char time[8] = {0};
//   sprintf(lat, "%02d%05.2lf%c", fix.latitude.degrees, fix.latitude.minutes, fix.latitude.cardinal);
//   sprintf(lon, "%03d%05.2lf%c", fix.longitude.degrees, fix.longitude.minutes, fix.longitude.cardinal);
//   sprintf(time, "%02d%02d%02dz", fix.time.tm_mday, fix.time.tm_hour, fix.time.tm_min);
//   lora_send_aprs(lat, lon, msg, time);
//   return 0;
// }

esp_err_t repl_init(void) {
  esp_console_repl_config_t repl_cfg = ESP_CONSOLE_REPL_CONFIG_DEFAULT();
  repl_cfg.prompt = "LoRa>";
  repl_cfg.max_cmdline_length = 256;
  ESP_ERROR_CHECK(esp_console_register_help_command());
  ESP_ERROR_CHECK(esp_console_cmd_register(&clear_cmd));
  // ESP_ERROR_CHECK(esp_console_cmd_register(&test_cmd));
  ESP_ERROR_CHECK(repl_settings_register());
  ESP_ERROR_CHECK(repl_pwr_register());
  ESP_ERROR_CHECK(repl_wifi_register());
  ESP_ERROR_CHECK(repl_prov_register());
  ESP_ERROR_CHECK(repl_gnss_register());
  ESP_ERROR_CHECK(repl_ota_register());
  esp_console_dev_uart_config_t uart_cfg = ESP_CONSOLE_DEV_UART_CONFIG_DEFAULT();
  ESP_ERROR_CHECK(esp_console_new_repl_uart(&uart_cfg, &repl_cfg, &repl));
  ESP_ERROR_CHECK(esp_console_start_repl(repl));
  return ESP_OK;
}
