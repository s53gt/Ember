import 'package:ember_ui/features/device_control/domain/device.dart';

abstract class HamStaRepository {
  Future<String> getCallsign(Device device);
  Future<void> setCallsign(Device device, String callsign);
  Future<String> getPasscode(Device device);
  Future<void> setPasscode(Device device, String passcode);
  Future<String> getSymbol(Device device);
  Future<void> setSymbol(Device device, String symbol);
}
