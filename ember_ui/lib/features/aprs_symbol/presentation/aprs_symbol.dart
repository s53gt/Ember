import 'package:ember_ui/features/aprs_symbol/presentation/aprs_symbol_graphic.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class AprsSymbol extends ConsumerWidget {
  final String tableChr;
  final String symbolChr;

  const AprsSymbol({
    super.key,
    required this.tableChr,
    required this.symbolChr,
  });

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final aprsSymbolGraphic =
        ref.watch(aprsSymbolGraphicProvider(tableChr, symbolChr));
    return aprsSymbolGraphic.hasValue
        ? Image.memory(
            aprsSymbolGraphic.value!,
            scale: 2,
          )
        : const CircularProgressIndicator();
  }
}
