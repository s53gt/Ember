// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ble_ham_sta_repository.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$bleHamStaRepositoryHash() =>
    r'4a7dfe5ccb24354b4040cdde7c220638fcfae3f7';

/// See also [bleHamStaRepository].
@ProviderFor(bleHamStaRepository)
final bleHamStaRepositoryProvider =
    AutoDisposeProvider<BleHamStaRepository>.internal(
  bleHamStaRepository,
  name: r'bleHamStaRepositoryProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$bleHamStaRepositoryHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef BleHamStaRepositoryRef = AutoDisposeProviderRef<BleHamStaRepository>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
