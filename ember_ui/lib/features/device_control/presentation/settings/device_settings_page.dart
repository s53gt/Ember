import 'package:ember_ui/features/device_control/presentation/current_device_stream_builder.dart';
import 'package:ember_ui/features/device_control/presentation/settings/general_settings_form.dart';
import 'package:ember_ui/features/device_control/presentation/wifi/wifi_settings.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class DeviceSettingsPage extends ConsumerWidget {
  const DeviceSettingsPage({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final localizations = AppLocalizations.of(context)!;
    return SingleChildScrollView(
      padding: const EdgeInsets.all(20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          CurrentDeviceStreamBuilder(
            builder: (_, snapshot) => Text(
              snapshot.data?.name ?? localizations.noDevice,
              style: TextStyle(
                color: Theme.of(context).colorScheme.primary,
                fontSize: 20,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          const SizedBox(height: 10),
          const Divider(),
          const GeneralSettingsForm(),
          const SizedBox(height: 10),
          const Divider(),
          const WifiSettings(),
        ],
      ),
    );
  }
}
