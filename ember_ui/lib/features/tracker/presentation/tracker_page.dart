import 'package:ember_ui/features/permission/presentation/ble_permission_middleware.dart';
import 'package:ember_ui/features/permission/presentation/location_permission_middleware.dart';
import 'package:ember_ui/features/tracker/presentation/tracker_map.dart';
import 'package:ember_ui/features/wakelock/presentation/wakelock_scope.dart';
import 'package:flutter/material.dart';

class TrackerPage extends StatelessWidget {
  const TrackerPage({super.key});

  @override
  Widget build(BuildContext context) {
    return const WakelockScope(
      child: LocationPermissionMiddleware(
        child: BlePermissionMiddleware(child: TrackerMap()),
      ),
    );
  }
}
