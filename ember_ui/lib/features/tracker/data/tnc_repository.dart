import 'package:ember_ui/features/device_control/domain/device.dart';

abstract class TncRepository {
  Stream<String> getPacketStream(Device device);
}
