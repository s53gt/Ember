#include "commands/gnss.h"

#include "argtable3/argtable3.h"
#include "esp_console.h"
#include "esp_err.h"
#include "esp_log.h"
#include "gnss.h"
#include "repl_private.h"

static const char* TAG = "gnss";

static int repl_gnss_cmd(int argc, char** argv);

static struct {
  struct arg_lit* start;
  struct arg_lit* stop;
  struct arg_end* end;
} gnss_args;

static esp_console_cmd_t gnss_cmd = {
    .command = "gnss",
    .help = "Manage GNSS",
    .hint = NULL,
    .func = &repl_gnss_cmd,
    .argtable = &gnss_args,
};

static void repl_gnss_init_arg_tables(void) {
  gnss_args.start = arg_lit0("s", "start", "Start GNSS parser");
  gnss_args.stop = arg_lit0("x", "stop", "Stop GNSS parser");
  gnss_args.end = arg_end(2);
}

esp_err_t repl_gnss_register(void) {
  repl_gnss_init_arg_tables();
  return esp_console_cmd_register(&gnss_cmd);
}

static int repl_gnss_cmd(int argc, char** argv) {
  REPL_ARGS_CHECK(gnss_args, argc, argv);
  if (gnss_args.start->count > 0) {
    gnss_start();
    return 0;
  }
  if (gnss_args.stop->count > 0) {
    gnss_stop();
    return 0;
  }
  ESP_LOGE(TAG, "Invalid arguments");
  return 1;
}
