#include "ota.h"

#include "esp_check.h"
#include "esp_crt_bundle.h"
#include "esp_http_client.h"
#include "esp_https_ota.h"

static const char* TAG = "ota";

esp_err_t ota_update_https(const char* url) {
  esp_http_client_config_t http_cfg = {
      .url = url,
      .crt_bundle_attach = esp_crt_bundle_attach,
  };
  esp_https_ota_config_t cfg = {
      .http_config = &http_cfg,
  };
  ESP_RETURN_ON_ERROR(esp_https_ota(&cfg), TAG, "Failed to perform OTA");
  return ESP_OK;
}
