#include "repl_private.h"

#include "linenoise/linenoise.h"

char* repl_getpass(const char* prompt) { return linenoise(prompt); }
