import 'package:freezed_annotation/freezed_annotation.dart';

part 'device.freezed.dart';

enum LinkQuality {
  // Order of enum entries matters! Should be sorted from worst to best. Enum index is used for sorting in some parts of
  // the UI
  poor,
  good,
  excellent,
}

/// Type of link used to control the device. Currently only BLE is supported, but TCP/UDP or others may be added in the
/// future
enum LinkType {
  ble,
}

enum DeviceConnectionState {
  connected,
  disconnected,
}

@freezed
class Device with _$Device {
  const factory Device({
    required String id,
    required String name,
    required bool connected,
    required LinkType linkType,
    LinkQuality? linkQuality,
  }) = _Device;
}
