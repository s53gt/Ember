// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ham_sta_service.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$hamStaServiceHash() => r'797b223de3914245980dafd7d4b42856e871d3d0';

/// See also [hamStaService].
@ProviderFor(hamStaService)
final hamStaServiceProvider = AutoDisposeProvider<HamStaService>.internal(
  hamStaService,
  name: r'hamStaServiceProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$hamStaServiceHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef HamStaServiceRef = AutoDisposeProviderRef<HamStaService>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
