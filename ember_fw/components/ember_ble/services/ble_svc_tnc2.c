#include "services/ble_svc_tnc2.h"

#include <stdlib.h>
#include <string.h>

#include "ember_ble_private.h"
#include "esp_check.h"
#include "esp_log.h"
#include "host/ble_gatt.h"
#include "tnc2.h"

static const char *TAG = "ble_svc_tnc2";

static int tnc2_access(uint16_t conn_handle, uint16_t attr_handle, struct ble_gatt_access_ctxt *ctx, void *arg);

static uint16_t tnc2_serial_val_handle;
const struct ble_gatt_svc_def tnc2_svc = {
    .type = BLE_GATT_SVC_TYPE_PRIMARY,
    .uuid = &tnc2_svc_uuid.u,
    .characteristics =
        (struct ble_gatt_chr_def[]){
            {
                .uuid = &tnc2_svc_serial_chr.u,
                .access_cb = tnc2_access,
                .val_handle = &tnc2_serial_val_handle,
                .flags = BLE_GATT_CHR_F_READ | BLE_GATT_CHR_F_READ_ENC | BLE_GATT_CHR_F_WRITE |
                         BLE_GATT_CHR_F_WRITE_ENC | BLE_GATT_CHR_F_NOTIFY,
            },
            {0},
        },
};

static int tnc2_access(uint16_t conn_handle, uint16_t attr_handle, struct ble_gatt_access_ctxt *ctx, void *arg) {
  ESP_LOGI(TAG, "TNC-2 BLE access, conn=%d, attr=%d", conn_handle, attr_handle);
  return 0;
}

static void tnc2_process_raw_packet(const uint8_t *packet, size_t len) {
  // FIXME A little messy that we're copying the entire buffer, just to suffix it with CRLF. Lines should probably be
  // passed including CRLF from L1
  void *buffer = malloc(len + 2);
  if (!buffer) {
    ESP_LOGW(TAG, "Failed to malloc buffer for raw TNC-2 packet");
    return;
  }
  memcpy(buffer, packet, len);
  ((char *)buffer)[len] = '\r';
  ((char *)buffer)[len + 1] = '\n';
  ember_ble_notify_custom(tnc2_serial_val_handle, buffer, len + 2);
  free(buffer);
}

esp_err_t ble_svc_tnc2_init(void) {
  tnc2_tap_t tap = {
      .name = "BLE_TNC-2",
      .process_raw_packet = tnc2_process_raw_packet,
  };
  esp_err_t err = tnc2_register_tap(&tap);
  ESP_RETURN_ON_ERROR(err, TAG, "Failed to register TNC2 raw packet handler");
  return ESP_OK;
}
