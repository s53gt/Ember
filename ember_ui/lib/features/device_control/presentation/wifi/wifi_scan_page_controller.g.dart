// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'wifi_scan_page_controller.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$wifiScanPageControllerHash() =>
    r'cc3d8decbcc5ce1c6e630e4989053fe1f1128ec5';

/// See also [WifiScanPageController].
@ProviderFor(WifiScanPageController)
final wifiScanPageControllerProvider = AutoDisposeAsyncNotifierProvider<
    WifiScanPageController, List<WifiNetwork>>.internal(
  WifiScanPageController.new,
  name: r'wifiScanPageControllerProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$wifiScanPageControllerHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$WifiScanPageController = AutoDisposeAsyncNotifier<List<WifiNetwork>>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
