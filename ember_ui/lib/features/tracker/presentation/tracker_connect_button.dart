import 'package:ember_ui/features/device_control/application/device_service.dart';
import 'package:ember_ui/features/device_control/presentation/ble/ble_scan_dialog.dart';
import 'package:ember_ui/features/device_control/presentation/current_device_stream_builder.dart';
import 'package:ember_ui/features/tracker/presentation/tracker_connect_button_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class TrackerConnectButton extends ConsumerStatefulWidget {
  const TrackerConnectButton({super.key});

  @override
  ConsumerState<ConsumerStatefulWidget> createState() =>
      _TrackerConnectButtonState();
}

class _TrackerConnectButtonState extends ConsumerState<TrackerConnectButton> {
  @override
  Widget build(BuildContext context) {
    final localizations = AppLocalizations.of(context)!;
    final deviceService = ref.watch(deviceServiceProvider);
    final state = ref.watch(trackerConnectButtonControllerProvider);
    final controller =
        ref.read(trackerConnectButtonControllerProvider.notifier);
    ref.listen(trackerConnectButtonControllerProvider, (_, state) {
      if (!state.isLoading && state.hasError) {
        ScaffoldMessenger.of(context)
            .showSnackBar(SnackBar(content: Text(state.error.toString())));
      }
    });
    return InkWell(
      onDoubleTap: () {
        if (!state.isLoading) {
          controller.resolveFuture(deviceService.disconnect());
        }
      },
      child: FloatingActionButton(
        shape: RoundedRectangleBorder(
          side: BorderSide(
            color: Theme.of(context).colorScheme.primary,
          ),
          borderRadius: BorderRadius.circular(10.0),
        ),
        tooltip: localizations.connectDevice,
        onPressed: () {
          if (!state.isLoading) {
            controller.resolveFuture(showBleScanDialog(context));
          }
        },
        child: state.isLoading
            ? SizedBox(
                width: IconTheme.of(context).size ?? 24,
                height: IconTheme.of(context).size ?? 24,
                child: const CircularProgressIndicator(),
              )
            : CurrentDeviceStreamBuilder(
                builder: (context, snapshot) => Icon(
                  snapshot.data != null
                      ? Icons.bluetooth_connected
                      : Icons.bluetooth,
                  color: snapshot.data != null
                      ? Theme.of(context).colorScheme.primary
                      : null,
                ),
              ),
      ),
    );
  }
}
