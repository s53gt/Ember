final bleAllServices = [
  bleGapServiceUid,
  bleBatServiceUid,
  bleHamStaServiceUid,
  bleWifiServiceUid,
  bleTncServiceUid,
];

// Generic Access
final bleGapServiceUid = _uuid16("1800");
final bleGapDeviceNameChrUid = _uuid16("2A00");

// Battery
final bleBatServiceUid = _uuid16("180F");
final bleBatLevelChrUid = _uuid16("2A19");

// HAM STA
const bleHamStaServiceUid = "d59b68bf-8fa9-4691-9eed-84e027eb50a4";
const bleHamStaCallsignChrUid = "0f78c5a6-8180-469e-a0ba-960ed71d6e46";
const bleHamStaPasscodeChrUid = "2cb0f3c3-49b2-49e2-8c46-78967d84a1b1";
const bleHamStaSymbolChrUid = "47b4bf5b-f353-4ced-9329-bd97ddd318b4";

// WiFi
const bleWifiServiceUid = "0b1fec6a-127e-49e6-971d-f37bacb01d79";
const bleWifiScanChrUid = "6c23eac7-cced-4c1f-8faa-ca46a2efd2af";
const bleWifiConnectChrUid = "59ef7c38-632c-48a6-94e8-05b5cd0ef94f";

// TNC
const bleTncServiceUid = "f79cfaa0-cf02-4e85-903c-11ff004014e9";
const bleTncSerialChrUid = "4730ee79-7049-48a9-8fff-c4460683380f";

// 7 char call + dash (-), + 2 char SSID
const bleHamStaCallsignMaxLen = 7 + 1 + 2;
const bleHamStaPasscodeMaxLen = 5;

const bleWifiApMaxLen = 32;
const bleWifiPassMaxLen = 64;

String _uuid16(String uuid16) => "0000$uuid16-0000-1000-8000-00805f9b34fb";
