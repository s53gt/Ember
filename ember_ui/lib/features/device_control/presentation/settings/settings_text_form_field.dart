import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class SettingsTextFormField extends StatefulWidget {
  final TextEditingController? controller;
  final bool readOnly;
  final bool password;
  final String? Function(String?)? validator;
  final String name;
  final String? help;
  final List<TextInputFormatter>? inputFormatters;
  final TextCapitalization? textCapitalization;
  final TextInputType? keyboardType;
  final Widget? suffix;

  const SettingsTextFormField({
    super.key,
    required this.name,
    this.controller,
    this.validator,
    this.help,
    this.inputFormatters,
    this.textCapitalization,
    this.keyboardType,
    this.readOnly = false,
    this.password = false,
    this.suffix,
  });

  @override
  State<StatefulWidget> createState() => _SettingsTextFormField();
}

class _SettingsTextFormField extends State<SettingsTextFormField> {
  bool _obscurePasscode = false;

  @override
  void initState() {
    super.initState();
    setState(() => _obscurePasscode = widget.password);
  }

  @override
  void didUpdateWidget(covariant SettingsTextFormField oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (oldWidget.password != widget.password) {
      setState(() => _obscurePasscode = widget.password);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            Text(widget.name),
            if (widget.help != null) ...[
              const SizedBox(width: 5),
              Tooltip(
                triggerMode: TooltipTriggerMode.tap,
                showDuration: const Duration(seconds: 10),
                message: widget.help,
                child: const Icon(
                  Icons.info,
                  size: 18,
                ),
              ),
            ],
          ],
        ),
        const SizedBox(height: 5),
        Row(
          children: [
            Expanded(
              child: TextFormField(
                controller: widget.controller,
                readOnly: widget.readOnly,
                validator: widget.validator,
                obscureText: _obscurePasscode,
                inputFormatters: widget.inputFormatters,
                keyboardType: widget.keyboardType,
                textCapitalization:
                    widget.textCapitalization ?? TextCapitalization.none,
                decoration: InputDecoration(
                  constraints: const BoxConstraints(maxHeight: 48),
                  suffixIcon: widget.password
                      ? Padding(
                          padding: const EdgeInsets.all(5),
                          child: InkWell(
                            customBorder: const CircleBorder(),
                            onTap: () => setState(
                                () => _obscurePasscode = !_obscurePasscode),
                            child: Icon(
                              _obscurePasscode
                                  ? Icons.visibility
                                  : Icons.visibility_off,
                            ),
                          ),
                        )
                      : null,
                  border: OutlineInputBorder(
                    borderSide: BorderSide(
                      color: Theme.of(context).colorScheme.primary,
                    ),
                  ),
                ),
              ),
            ),
            if (widget.suffix != null) widget.suffix!,
          ],
        ),
      ],
    );
  }
}
