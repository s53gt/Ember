// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'language_model.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$languageHash() => r'fa0e66be71399249f72503349863ec996450abdf';

/// See also [Language].
@ProviderFor(Language)
final languageProvider = AutoDisposeNotifierProvider<Language, String>.internal(
  Language.new,
  name: r'languageProvider',
  debugGetCreateSourceHash:
      const bool.fromEnvironment('dart.vm.product') ? null : _$languageHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$Language = AutoDisposeNotifier<String>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
