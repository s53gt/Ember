#include "lora.h"

#include <RadioLib.h>
#include <string.h>

#include <cmath>
#include <cstdint>
#include <cstdlib>
#include <cstring>

#include "EspHal.h"
#include "TypeDef.h"
#include "esp_attr.h"
#include "esp_err.h"
#include "esp_log.h"
#include "sdkconfig.h"

extern "C" {
#include "esp_check.h"
#include "freertos/FreeRTOS.h"
#include "freertos/event_groups.h"
#include "freertos/idf_additions.h"
#include "modules/SX127x/SX1276.h"
#include "portmacro.h"
#include "settings_default.h"

#define EVENT_INT (1 << 0)
#define EVENT_STOP_RX_TASK (1 << 1)

#define ENABLE_CAD (1)

static void lora_interrupt_dio0(void);

static const char* TAG = "LoRa";
static const int DISTANCE_ESTIMATION_ENV_FACTOR = 2;

static EspHal* hal = nullptr;
static SX1276 radio = nullptr;
static SemaphoreHandle_t radio_mutex = NULL;
static SemaphoreHandle_t cad_semaphore = NULL;
static EventGroupHandle_t event_group = NULL;
static QueueHandle_t rx_queue = NULL;
static QueueHandle_t tx_queue = NULL;
static TaskHandle_t rx_task = NULL;
static TaskHandle_t tx_task = NULL;
static TaskHandle_t proto_task = NULL;
static volatile bool transmitting = false;
static volatile bool performing_cad = false;
static volatile bool channel_busy = false;
static lora_proto_t protos[CONFIG_LORA_PROTOS_MAX];
static uint8_t protos_idx = 0;

static IRAM_ATTR void lora_interrupt_dio0(void) {
  if (performing_cad) {
    channel_busy = true;
    performing_cad = false;
    BaseType_t higher_priority_task_woken = pdFALSE;
    BaseType_t result = xSemaphoreGiveFromISR(cad_semaphore, &higher_priority_task_woken);
    if (result == pdTRUE) portYIELD_FROM_ISR(higher_priority_task_woken);
  } else {
    BaseType_t higher_priority_task_woken = pdFALSE;
    BaseType_t result = xEventGroupSetBitsFromISR(event_group, EVENT_INT, &higher_priority_task_woken);
    if (result == pdTRUE) portYIELD_FROM_ISR(higher_priority_task_woken);
  }
}

static IRAM_ATTR void lora_interrupt_dio1(void) {
  if (performing_cad) {
    channel_busy = false;
    performing_cad = false;
    BaseType_t higher_priority_task_woken = pdFALSE;
    BaseType_t result = xSemaphoreGiveFromISR(cad_semaphore, &higher_priority_task_woken);
    if (result == pdTRUE) portYIELD_FROM_ISR(higher_priority_task_woken);
  }
}

static void lora_proto_task(void* arg) {
  lora_packet_t packet;
  while (1) {  // TODO Make it able to exit?
    memset(&packet, 0, sizeof(lora_packet_t));
    if (xQueueReceive(rx_queue, &packet, portMAX_DELAY) != pdPASS) {
      ESP_LOGE(TAG, "Failed to receive from Rx queue");
      continue;
    }
    for (uint8_t i = 0; i < protos_idx; i++) {
      if (protos[i].detect_packet(&packet)) {
        ESP_LOGD(TAG, "Passing packet to proto %s", protos[i].name);
        protos[i].process_packet(&packet);
        goto end;
      }
    }
    ESP_LOGW(TAG, "Received unknown packet type");
    ESP_LOG_BUFFER_HEXDUMP(TAG, packet.data, packet.len, ESP_LOG_DEBUG);
  end:;
    free(packet.data);
  }
  proto_task = NULL;
  vTaskDelete(NULL);
}

static void lora_rx_task(void* arg) {
  EventBits_t bits = 0;
  int16_t state;
  float rssi = 0;
  float snr = 0;
  float freq_err = 0;
  float rssi_at_1m = -23;
  float distance = 0;
  lora_packet_t packet;
  do {
    ESP_LOGI(TAG, "Waiting for LoRa packet...");
    bits = xEventGroupWaitBits(event_group, EVENT_INT | EVENT_STOP_RX_TASK, pdTRUE, pdFALSE, portMAX_DELAY);
    if (!(bits & EVENT_INT)) continue;
    xSemaphoreTake(radio_mutex, portMAX_DELAY);
    if (transmitting) {
      transmitting = false;
      ESP_LOGI(TAG, "Packet transmitted. Switching to Rx...");
      state = radio.startReceive();
      if (state != RADIOLIB_ERR_NONE) ESP_LOGE(TAG, "Failed to start receive, err=%d", state);
    }
    memset(&packet, 0, sizeof(lora_packet_t));
    packet.len = radio.getPacketLength();
    packet.data = NULL;
    if (!packet.len) goto end;
    packet.data = (uint8_t*)malloc(packet.len);  // This buffer must be deallocated in lora_proto_task()
    if (!packet.data) {
      ESP_LOGW(TAG, "Failed to malloc buffer for rx packet of len %u", packet.len);
      goto end;
    }
    state = radio.readData(packet.data, packet.len);
    if (state == RADIOLIB_ERR_CRC_MISMATCH) {
      ESP_LOGW(TAG, "Received a packet with invalid CRC");
      ESP_LOG_BUFFER_HEXDUMP(TAG, packet.data, packet.len, ESP_LOG_DEBUG);
      goto error;
    } else if (state != RADIOLIB_ERR_NONE) {
      ESP_LOGE(TAG, "Failed to read packet, err=%d", state);
      goto error;
    }
    rssi = radio.getRSSI();
    snr = radio.getSNR();
    freq_err = radio.getFrequencyError();
    distance = (float)pow(10, (rssi_at_1m - rssi) / (10 * DISTANCE_ESTIMATION_ENV_FACTOR));
    ESP_LOGI(TAG,
             "Received packet of length %u, RSSI = %ddBm, SNR = %.2fdB, Frequency Error = %.2fHz, Estimated Distance = "
             "%.2fm",
             packet.len, (int)rssi, snr, freq_err, distance);
    ESP_LOG_BUFFER_HEXDUMP(TAG, packet.data, packet.len, ESP_LOG_DEBUG);
    if (xQueueSendToBack(rx_queue, &packet, 0) != pdPASS) {
      ESP_LOGW(TAG, "Rx queue full. Dropping packet...");
      goto error;
    }
    goto end;
  error:;
    if (packet.data) free(packet.data);
  end:;
    xSemaphoreGive(radio_mutex);
  } while (!(bits & EVENT_STOP_RX_TASK));
  rx_task = NULL;
  vTaskDelete(NULL);
}

static bool lora_channel_busy(void) {
  xSemaphoreTake(radio_mutex, portMAX_DELAY);
  performing_cad = true;
  int16_t state = radio.startChannelScan();
  if (state != RADIOLIB_ERR_NONE) {
    ESP_LOGW(TAG, "CAD failed, state=%d", state);
    return false;
  }
  xSemaphoreTake(cad_semaphore, portMAX_DELAY);
  xSemaphoreGive(radio_mutex);
  return channel_busy;
}

static void lora_tx_task(void* arg) {
  lora_packet_t packet;
  while (1) {  // TODO Make it able to exit?
    if (xQueueReceive(tx_queue, &packet, portMAX_DELAY) != pdPASS) {
      ESP_LOGE(TAG, "Failed to read from LoRa Tx queue");
      continue;
    }
#if ENABLE_CAD
    while (lora_channel_busy()) portYIELD();
#endif
    xSemaphoreTake(radio_mutex, portMAX_DELAY);
    int16_t state = radio.startTransmit(packet.data, packet.len);
    if (state != RADIOLIB_ERR_NONE) goto end;
    transmitting = true;
  end:;
    xSemaphoreGive(radio_mutex);
  }
  tx_task = NULL;
  vTaskDelete(NULL);
}

esp_err_t lora_init() {
  event_group = xEventGroupCreate();
  radio_mutex = xSemaphoreCreateMutex();
  cad_semaphore = xSemaphoreCreateBinary();
  rx_queue = xQueueCreate(LORA_RX_QUEUE_SIZE, sizeof(lora_packet_t));
  tx_queue = xQueueCreate(LORA_TX_QUEUE_SIZE, sizeof(lora_packet_t));
  hal = new EspHal(CONFIG_LORA_SCK, CONFIG_LORA_MISO, CONFIG_LORA_MOSI);
  radio = new Module(hal, CONFIG_LORA_CS, CONFIG_LORA_DIO0, CONFIG_LORA_RST, CONFIG_LORA_DIO1);
  int err = radio.begin(LORA_FREQ_MHZ, LORA_BANDWIDTH_KHZ, LORA_SPREAD_FACTOR, LORA_CODING_RATE);
  ESP_RETURN_ON_FALSE(err == RADIOLIB_ERR_NONE, ESP_FAIL, TAG, "Failed to initialize radio, err=%d", err);
  radio.setDio0Action(lora_interrupt_dio0, hal->GpioInterruptRising);
  radio.setDio1Action(lora_interrupt_dio1, hal->GpioInterruptRising);
  int state = radio.setCRC(true);
  ESP_RETURN_ON_FALSE(state == RADIOLIB_ERR_NONE, ESP_FAIL, TAG, "Failed to enable CRC, err=%d", state);
  state = radio.explicitHeader();
  ESP_RETURN_ON_FALSE(state == RADIOLIB_ERR_NONE, ESP_FAIL, TAG, "Failed to enable explicit header, err=%d", state);
  state = radio.setOutputPower(20);
  ESP_RETURN_ON_FALSE(state == RADIOLIB_ERR_NONE, ESP_FAIL, TAG, "Failed to set output power, err=%d", state);
  // Allowed values range from 45 to 120 mA in 5 mA steps and 120 to 240 mA in 10 mA steps.
  state = radio.setCurrentLimit(120);
  ESP_RETURN_ON_FALSE(state == RADIOLIB_ERR_NONE, ESP_FAIL, TAG, "Failed to set current limit, err=%d", state);
  state = radio.startReceive();
  ESP_RETURN_ON_FALSE(state == RADIOLIB_ERR_NONE, ESP_FAIL, TAG, "Failed to start receiving, err=%d", state);
  BaseType_t res = xTaskCreate(lora_rx_task, "lora-rx", 4096, NULL, 12, &rx_task);
  ESP_RETURN_ON_FALSE(res == pdPASS, ESP_FAIL, TAG, "Failed to start LoRa Rx task");
  res = xTaskCreate(lora_tx_task, "lora-tx", 4096, NULL, 12, &tx_task);
  ESP_RETURN_ON_FALSE(res == pdPASS, ESP_FAIL, TAG, "Failed to start LoRa Tx task");
  res = xTaskCreate(lora_proto_task, "lora-proto", 4096, NULL, 12, &proto_task);
  ESP_RETURN_ON_FALSE(res == pdPASS, ESP_FAIL, TAG, "Failed to start LoRa Proto task");
  return ESP_OK;
}

esp_err_t lora_transmit(uint8_t* data, size_t len, TickType_t xTicksToWait) {
  lora_packet_t packet = {.data = data, .len = len};
  BaseType_t ret = xQueueSendToBack(tx_queue, &packet, xTicksToWait);
  ESP_RETURN_ON_FALSE(ret == pdPASS, ESP_FAIL, TAG, "Failed to queue packet for TX");
  return ESP_OK;
}

esp_err_t lora_register_proto(const lora_proto_t* proto) {
  if (protos_idx >= CONFIG_LORA_PROTOS_MAX) {
    ESP_LOGE(TAG,
             "Not enough space to register LoRa proto %s. Increase CONFIG_LORA_PROTOS_MAX in sdkconfig to allocate "
             "more space",
             proto->name);
    return ESP_FAIL;
  }
  memcpy(&protos[protos_idx++], proto, sizeof(lora_proto_t));
  return ESP_OK;
}
}
