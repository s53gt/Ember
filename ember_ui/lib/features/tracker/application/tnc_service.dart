import 'package:ember_ui/exceptions/invalid_state_exception.dart';
import 'package:ember_ui/features/device_control/application/device_service.dart';
import 'package:ember_ui/features/device_control/domain/device.dart';
import 'package:ember_ui/features/tracker/data/ble_tnc_repository.dart';
import 'package:ember_ui/features/tracker/data/tnc_repository.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'tnc_service.g.dart';

class TncService {
  final DeviceService _deviceService;
  final Map<LinkType, TncRepository> _repos;

  TncService(this._repos, this._deviceService);

  TncRepository get _repo {
    if (_deviceService.connectedDevice == null) throw InvalidStateException();
    final repo = _repos[_deviceService.connectedDevice!.linkType];
    if (repo == null) throw UnimplementedError();
    return repo;
  }

  Stream<String> packets() =>
      _repo.getPacketStream(_deviceService.connectedDevice!);
}

@riverpod
TncService tncService(TncServiceRef ref) {
  return TncService({
    LinkType.ble: ref.watch(bleTncRepositoryProvider),
  }, ref.watch(deviceServiceProvider));
}
