import 'package:ember_ui/features/navigation/presentation/nav_drawer.dart';
import 'package:flutter/material.dart';

class RootLayout extends StatelessWidget {
  final Widget title;
  final Widget body;

  const RootLayout({super.key, required this.title, required this.body});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const NavDrawer(),
      appBar: AppBar(
        title: title,
      ),
      body: body,
    );
  }
}
