#include "tnc2.h"

#include <string.h>

#include "aprs_is.h"
#include "esp_check.h"
#include "esp_log.h"
#include "lora.h"
#include "settings_default.h"

// This is the currently most used LoRa APRS "magic" prefix
#define LORA_APRS_MAGIC "\x3c\xff\x01"
#define LORA_APRS_MAGIC_LEN 3

#define TNC2_CALL_SSID_SEP '-'
#define TNC2_SRC_DEST_SEP '>'
#define TNC2_PATH_SEP ','
#define TNC2_DATA_SEP ':'

#define IS_DIGIT(C) ((C) >= '0' && (C) <= '9')
#define IS_ALPHANUM(C) (IS_DIGIT(C) || ((C) >= 'A' && (C) <= 'Z') || ((C) >= 'a' && (C) <= 'z'))

static bool tnc2_detect_lora_packet(const lora_packet_t *packet);
static void tnc2_process_lora_packet(const lora_packet_t *packet);
static void tnc2_process_aprs_is_packet(const aprs_is_packet_t *packet);

static tnc2_proto_t protos[CONFIG_TNC2_PROTOS_MAX];
static uint8_t protos_idx = 0;

static tnc2_tap_t taps[CONFIG_TNC2_TAPS_MAX];
static uint8_t taps_idx = 0;

static const char *TAG = "tnc2";

esp_err_t tnc2_init(void) {
  aprs_is_proto_t aprs_is_proto = {
      .name = "TNC-2",
      .process_packet = tnc2_process_aprs_is_packet,
  };
  esp_err_t err = aprs_is_register_proto(&aprs_is_proto);
  ESP_RETURN_ON_ERROR(err, TAG, "Failed to register APRS-IS data handler, err=%d", err);
  lora_proto_t lora_proto = {
      .name = "TNC-2", .detect_packet = tnc2_detect_lora_packet, .process_packet = tnc2_process_lora_packet};
  return lora_register_proto(&lora_proto);
}

static inline bool tnc2_detect_lora_packet(const lora_packet_t *packet) {
  return packet->len > LORA_APRS_MAGIC_LEN && memcmp(packet->data, LORA_APRS_MAGIC, LORA_APRS_MAGIC_LEN) == 0;
}

static esp_err_t tnc2_parse_callsign(const uint8_t *data, size_t len, size_t *idx, char call[HAM_STA_CALLSIGN_MAX_LEN],
                                     char ssid[HAM_STA_SSID_MAX_LEN]) {
  uint8_t i = 0;
  for (; *idx < len; (*idx)++) {
    char c = data[*idx];
    if (!IS_ALPHANUM(c)) break;
    if (i >= HAM_STA_CALLSIGN_MAX_LEN) {
      ESP_LOGE(TAG, "Callsign too long");
      return ESP_FAIL;
    }
    call[i++] = c;
  }
  i = 0;
  if (data[*idx] == TNC2_CALL_SSID_SEP) {
    (*idx)++;
    for (; *idx < len && i < HAM_STA_SSID_MAX_LEN; (*idx)++) {
      char c = data[*idx];
      if (IS_ALPHANUM(c))
        ssid[i++] = c;
      else
        break;
    }
  }
  return ESP_OK;
}

static esp_err_t tnc2_parse_header(const uint8_t *data, size_t len, size_t *idx, tnc2_packet_t *out) {
  ESP_RETURN_ON_ERROR(tnc2_parse_callsign(data, len, idx, out->fromcall, out->fromcall_ssid), TAG,
                      "Failed to parse src callsign");
  if (data[(*idx)++] != TNC2_SRC_DEST_SEP) {
    ESP_LOGE(TAG, "Expected src/dest call separator '>', instead got '%c' (%x)", data[(*idx) - 1], data[(*idx) - 1]);
    return ESP_FAIL;
  }
  ESP_RETURN_ON_ERROR(tnc2_parse_callsign(data, len, idx, out->tocall, out->tocall_ssid), TAG,
                      "Failed to parse dest callsign");
  if (data[(*idx)++] != TNC2_PATH_SEP) {
    ESP_LOGE(TAG, "Expected dest/path separator ',', instead got '%c' (%x)", data[(*idx) - 1], data[(*idx) - 1]);
    return ESP_FAIL;
  }
  uint8_t path_len = 0;
  uint8_t i = *idx;
  for (; i < len && data[i++] != TNC2_DATA_SEP; path_len++);
  if (i >= len) {
    ESP_LOGE(TAG, "Overran packet buffer looking for path/data separator ':'");
    return ESP_FAIL;
  }
  out->path = malloc(path_len + 1);
  memset(out->path, 0, path_len + 1);
  memcpy(out->path, data + *idx, path_len);
  *idx += path_len + 1;
  return ESP_OK;
}

esp_err_t tnc2_parse_packet(tnc2_src_proto_t proto, const uint8_t *data, size_t len, tnc2_packet_t *out) {
  // Skip the 3 byte prefix @see aprs_detect_packet()
  size_t idx = proto == TNC2_SRC_PROTO_LORA ? 3 : 0;
  if (tnc2_parse_header(data, len, &idx, out) != ESP_OK) {
    ESP_LOGE(TAG, "Invalid TNC-2 header");
    ESP_LOG_BUFFER_HEXDUMP(TAG, data, len, ESP_LOG_DEBUG);
    return ESP_FAIL;
  }
  out->data_len = len - idx;
  out->data = malloc(out->data_len);
  memcpy(out->data, data + idx, out->data_len);
  out->proto = proto;
  return ESP_OK;
}

static void tnc2_process_packet(tnc2_src_proto_t proto, const uint8_t *data, size_t len) {
  tnc2_packet_t out = {0};
  if (tnc2_parse_packet(proto, data, len, &out) != ESP_OK) return;
  ESP_LOGD(TAG, "FROMCALL: %s SSID %s\tTOCALL: %s SSID %s\tPATH: %s", out.fromcall, out.fromcall_ssid, out.tocall,
           out.tocall_ssid, out.path);
  for (uint8_t i = 0; i < taps_idx; i++) {
    ESP_LOGD(TAG, "Passing raw packet to tap %s", taps[i].name);
    taps[i].process_raw_packet(data, len);
  }
  for (uint8_t i = 0; i < protos_idx; i++) {
    ESP_LOGD(TAG, "Passing packet to proto %s", protos[i].name);
    protos[i].process_packet(&out);
  }
  free(out.data);
  free(out.path);
}

static void tnc2_process_lora_packet(const lora_packet_t *packet) {
  tnc2_process_packet(TNC2_SRC_PROTO_LORA, packet->data, packet->len);
}

static void tnc2_process_aprs_is_packet(const aprs_is_packet_t *packet) {
  tnc2_process_packet(TNC2_SRC_PROTO_APRS_IS, packet->data, packet->len);
}

esp_err_t tnc2_register_proto(const tnc2_proto_t *proto) {
  if (protos_idx >= CONFIG_TNC2_PROTOS_MAX) {
    ESP_LOGE(TAG,
             "Not enough space to register TNC-2 proto %s. Increase TNC2_PROTOS_MAX in settings_default.h to allocate "
             "more space",
             proto->name);
    return ESP_FAIL;
  }
  memcpy(&protos[protos_idx++], proto, sizeof(tnc2_proto_t));
  return ESP_OK;
}

esp_err_t tnc2_register_tap(const tnc2_tap_t *tap) {
  if (taps_idx >= CONFIG_TNC2_TAPS_MAX) {
    ESP_LOGE(TAG,
             "Not enough space to register TNC-2 tap %s. Increase CONFIG_TNC2_TAPS_MAX in menuconfig to allocate "
             "more space",
             tap->name);
    return ESP_FAIL;
  }
  memcpy(&taps[taps_idx++], tap, sizeof(tnc2_tap_t));
  return ESP_OK;
}
