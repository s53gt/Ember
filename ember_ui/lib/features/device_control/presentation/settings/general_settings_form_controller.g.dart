// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'general_settings_form_controller.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$generalSettingsFormControllerHash() =>
    r'28a6f020d3d85a75d8834b2c88ac50cffd0e40da';

/// See also [GeneralSettingsFormController].
@ProviderFor(GeneralSettingsFormController)
final generalSettingsFormControllerProvider = AutoDisposeAsyncNotifierProvider<
    GeneralSettingsFormController, GeneralSettingsFormState>.internal(
  GeneralSettingsFormController.new,
  name: r'generalSettingsFormControllerProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$generalSettingsFormControllerHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$GeneralSettingsFormController
    = AutoDisposeAsyncNotifier<GeneralSettingsFormState>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
