#pragma once

#include <stdint.h>

#include "esp_err.h"
#include "esp_wifi_types.h"

esp_err_t wifi_enable(void);
esp_err_t wifi_disable(void);
esp_err_t wifi_connect(void);
esp_err_t wifi_set_ap_creds(uint8_t ssid[MAX_SSID_LEN], uint8_t password[MAX_PASSPHRASE_LEN], wifi_auth_mode_t auth_mode);
esp_err_t wifi_get_ap_ssid(uint8_t ssid[MAX_SSID_LEN]);
esp_err_t wifi_get_rssi(int* rssi);
esp_err_t wifi_disconnect(void);
esp_err_t wifi_scan(uint16_t* len, wifi_ap_record_t ap_info[*len]);
void wifi_print_scan_results(uint16_t number, wifi_ap_record_t ap_info[number]);
const char* wifi_auth_mode_to_str(wifi_auth_mode_t authmode);
const char* wifi_cipher_to_str(wifi_cipher_type_t cipher);
