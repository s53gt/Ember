import 'dart:async';

import 'package:ember_ui/features/device_control/application/device_service.dart';
import 'package:ember_ui/features/device_control/application/ham_sta_service.dart';
import 'package:ember_ui/features/device_control/domain/device.dart';
import 'package:ember_ui/features/device_control/presentation/settings/general_settings_form_state.dart';
import 'package:flutter/material.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'general_settings_form_controller.g.dart';

@riverpod
class GeneralSettingsFormController extends _$GeneralSettingsFormController {
  GlobalKey<FormState>? _formKey;
  TextEditingController? _callsignController;
  TextEditingController? _passcodeController;
  StreamSubscription<Device?>? _subscription;
  Device? _lastDevice;
  String? _currentCallsign;
  String? _currentPasscode;

  set formKey(GlobalKey<FormState> formKey) {
    _formKey = formKey;
  }

  Future<void> _onDeviceChanged(Device? device) async {
    if (device?.id != null && device!.id == _lastDevice?.id) return;
    if (device == null) {
      _callsignController?.clear();
      _passcodeController?.clear();
      _currentCallsign = null;
      _currentPasscode = null;
      _lastDevice = null;
      return;
    }
    state = const AsyncLoading();
    final hamStaService = ref.read(hamStaServiceProvider);
    final callsign = await hamStaService.getCallsign();
    final passcode = await hamStaService.getPasscode();
    _callsignController?.text = callsign;
    _passcodeController?.text = passcode;
    _currentCallsign = callsign;
    _currentPasscode = passcode;
    state = AsyncData(
      GeneralSettingsFormState(
        callsignController: _callsignController!,
        passcodeController: _passcodeController!,
      ),
    );
  }

  void _dispose() {
    _subscription?.cancel();
    _subscription = null;
    _callsignController?.dispose();
    _callsignController = null;
    _passcodeController?.dispose();
    _passcodeController = null;
  }

  void onSave() async {
    if (_formKey?.currentState?.validate() != true) return;
    state = const AsyncLoading();
    try {
      final callsign = _callsignController!.text;
      final passcode = _passcodeController!.text;
      final hamStaService = ref.read(hamStaServiceProvider);
      if (_currentCallsign != callsign) {
        await hamStaService.setCallsign(callsign);
        _currentCallsign = callsign;
      }
      if (_currentPasscode != passcode) {
        await hamStaService.setPasscode(passcode);
        _currentPasscode = passcode;
      }
    } finally {
      state = AsyncData(
        GeneralSettingsFormState(
          callsignController: _callsignController!,
          passcodeController: _passcodeController!,
        ),
      );
    }
  }

  @override
  Future<GeneralSettingsFormState> build() async {
    _callsignController ??= TextEditingController();
    _passcodeController ??= TextEditingController();
    if (_subscription == null) {
      final deviceService = ref.read(deviceServiceProvider);
      await _onDeviceChanged(deviceService.connectedDevice);
      _subscription =
          deviceService.connectedDeviceStream.listen(_onDeviceChanged);
      ref.onDispose(_dispose);
    }
    return GeneralSettingsFormState(
      callsignController: _callsignController!,
      passcodeController: _passcodeController!,
    );
  }
}
