#include "ui.h"

#if CONFIG_DISPLAY_ENABLE
#include "core/lv_disp.h"
#include "core/lv_obj.h"
#include "core/lv_obj_pos.h"
#include "esp_check.h"
#include "esp_err.h"
#include "extra/layouts/flex/lv_flex.h"
#include "font/lv_symbol_def.h"
#include "gnss.h"
#include "widgets/lv_label.h"

static const char *TAG = "ui";

static lv_obj_t *root_layout = NULL;
static lv_obj_t *fix_lbl = NULL;
static lv_obj_t *lat_lbl = NULL;
static lv_obj_t *lon_lbl = NULL;
static lv_obj_t *alt_lbl = NULL;

static esp_err_t ui_init_label(lv_obj_t **lbl, lv_obj_t *parent) {
  *lbl = lv_label_create(parent);
  ESP_RETURN_ON_FALSE(*lbl, ESP_ERR_NO_MEM, TAG, "Failed to create label");
  lv_label_set_long_mode(*lbl, LV_LABEL_LONG_SCROLL_CIRCULAR);
  return ESP_OK;
}

esp_err_t ui_init(void) {
  root_layout = lv_obj_create(lv_scr_act());
  lv_obj_set_size(root_layout, 128, 64);
  lv_obj_set_flex_flow(root_layout, LV_FLEX_FLOW_COLUMN_WRAP);
  ESP_RETURN_ON_ERROR(ui_init_label(&fix_lbl, root_layout), TAG, "Fix label init failed");
  ESP_RETURN_ON_ERROR(ui_init_label(&alt_lbl, root_layout), TAG, "Altitude label init failed");
  ESP_RETURN_ON_ERROR(ui_init_label(&lat_lbl, root_layout), TAG, "Latitude label init failed");
  ESP_RETURN_ON_ERROR(ui_init_label(&lon_lbl, root_layout), TAG, "Longitude label init failed");
  return ESP_OK;
}

void ui_update(gnss_fix_t *fix) {
  lv_label_set_text_fmt(fix_lbl, "Fix: %s    Sats: %d", fix->fix ? LV_SYMBOL_OK : LV_SYMBOL_CLOSE, fix->n_satellites);
  double lat = fix->latitude.degrees + (fix->latitude.minutes / 60);
  char lat_cardinal = fix->latitude.cardinal == '\0' ? '/' : fix->latitude.cardinal;
  lv_label_set_text_fmt(lat_lbl, "Lat: %.4lf° %c", lat, lat_cardinal);
  double lon = fix->longitude.degrees + (fix->longitude.minutes / 60);
  char lon_cardinal = fix->longitude.cardinal == '\0' ? '/' : fix->longitude.cardinal;
  lv_label_set_text_fmt(lon_lbl, "Lon: %.4lf° %c", lon, lon_cardinal);
  lv_label_set_text_fmt(alt_lbl, "Alt: %.1lf %c", fix->altitude, fix->altitude_unit);
}
#else
esp_err_t ui_init(void) { return ESP_OK; }
void ui_update(gnss_fix_t *fix) {}
#endif
