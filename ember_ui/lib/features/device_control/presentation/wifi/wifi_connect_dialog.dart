import 'package:ember_ui/constants/ble_constants.dart';
import 'package:ember_ui/features/device_control/domain/wifi_network.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

Future<String?> showWifiConnectDialog(
    BuildContext context, WifiNetwork net) async {
  final localizations = AppLocalizations.of(context)!;
  final passwordController = TextEditingController();
  final password = await showDialog<String>(
    context: context,
    builder: (context) => AlertDialog.adaptive(
      title: Text(
        localizations.connectTo(net.ssid),
        style: const TextStyle(fontSize: 17),
      ),
      content: _WifiConnectDialog(passwordController),
      actions: [
        TextButton(
          onPressed: () => Navigator.pop(context, passwordController.text),
          child: Text(localizations.connect),
        ),
        TextButton(
          onPressed: () => Navigator.pop(context),
          child: Text(localizations.cancel),
        ),
      ],
    ),
  );
  passwordController.dispose();
  return password;
}

class _WifiConnectDialog extends StatefulWidget {
  final TextEditingController passwordController;

  const _WifiConnectDialog(this.passwordController);

  @override
  State<StatefulWidget> createState() => _WifiConnectDialogState();
}

class _WifiConnectDialogState extends State<_WifiConnectDialog> {
  @override
  void dispose() {
    widget.passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final localizations = AppLocalizations.of(context)!;
    return TextField(
      maxLength: bleWifiPassMaxLen,
      obscureText: true,
      autocorrect: false,
      decoration: InputDecoration(hintText: localizations.password),
      controller: widget.passwordController,
    );
  }
}
