import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:universal_io/io.dart';

part 'language_model.g.dart';

@riverpod
class Language extends _$Language {
  String? _locale;

  @override
  String build() {
    _locale ??= Platform.localeName.split('_')[0];
    return _locale!;
  }

  void setLocale(String locale) {
    _locale = locale;
    ref.invalidateSelf();
  }
}
