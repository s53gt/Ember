// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'general_settings_form_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$GeneralSettingsFormState {
  TextEditingController get callsignController =>
      throw _privateConstructorUsedError;
  TextEditingController get passcodeController =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $GeneralSettingsFormStateCopyWith<GeneralSettingsFormState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $GeneralSettingsFormStateCopyWith<$Res> {
  factory $GeneralSettingsFormStateCopyWith(GeneralSettingsFormState value,
          $Res Function(GeneralSettingsFormState) then) =
      _$GeneralSettingsFormStateCopyWithImpl<$Res, GeneralSettingsFormState>;
  @useResult
  $Res call(
      {TextEditingController callsignController,
      TextEditingController passcodeController});
}

/// @nodoc
class _$GeneralSettingsFormStateCopyWithImpl<$Res,
        $Val extends GeneralSettingsFormState>
    implements $GeneralSettingsFormStateCopyWith<$Res> {
  _$GeneralSettingsFormStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? callsignController = null,
    Object? passcodeController = null,
  }) {
    return _then(_value.copyWith(
      callsignController: null == callsignController
          ? _value.callsignController
          : callsignController // ignore: cast_nullable_to_non_nullable
              as TextEditingController,
      passcodeController: null == passcodeController
          ? _value.passcodeController
          : passcodeController // ignore: cast_nullable_to_non_nullable
              as TextEditingController,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$GeneralSettingsFormStateImplCopyWith<$Res>
    implements $GeneralSettingsFormStateCopyWith<$Res> {
  factory _$$GeneralSettingsFormStateImplCopyWith(
          _$GeneralSettingsFormStateImpl value,
          $Res Function(_$GeneralSettingsFormStateImpl) then) =
      __$$GeneralSettingsFormStateImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {TextEditingController callsignController,
      TextEditingController passcodeController});
}

/// @nodoc
class __$$GeneralSettingsFormStateImplCopyWithImpl<$Res>
    extends _$GeneralSettingsFormStateCopyWithImpl<$Res,
        _$GeneralSettingsFormStateImpl>
    implements _$$GeneralSettingsFormStateImplCopyWith<$Res> {
  __$$GeneralSettingsFormStateImplCopyWithImpl(
      _$GeneralSettingsFormStateImpl _value,
      $Res Function(_$GeneralSettingsFormStateImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? callsignController = null,
    Object? passcodeController = null,
  }) {
    return _then(_$GeneralSettingsFormStateImpl(
      callsignController: null == callsignController
          ? _value.callsignController
          : callsignController // ignore: cast_nullable_to_non_nullable
              as TextEditingController,
      passcodeController: null == passcodeController
          ? _value.passcodeController
          : passcodeController // ignore: cast_nullable_to_non_nullable
              as TextEditingController,
    ));
  }
}

/// @nodoc

class _$GeneralSettingsFormStateImpl implements _GeneralSettingsFormState {
  const _$GeneralSettingsFormStateImpl(
      {required this.callsignController, required this.passcodeController});

  @override
  final TextEditingController callsignController;
  @override
  final TextEditingController passcodeController;

  @override
  String toString() {
    return 'GeneralSettingsFormState(callsignController: $callsignController, passcodeController: $passcodeController)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$GeneralSettingsFormStateImpl &&
            (identical(other.callsignController, callsignController) ||
                other.callsignController == callsignController) &&
            (identical(other.passcodeController, passcodeController) ||
                other.passcodeController == passcodeController));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, callsignController, passcodeController);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$GeneralSettingsFormStateImplCopyWith<_$GeneralSettingsFormStateImpl>
      get copyWith => __$$GeneralSettingsFormStateImplCopyWithImpl<
          _$GeneralSettingsFormStateImpl>(this, _$identity);
}

abstract class _GeneralSettingsFormState implements GeneralSettingsFormState {
  const factory _GeneralSettingsFormState(
          {required final TextEditingController callsignController,
          required final TextEditingController passcodeController}) =
      _$GeneralSettingsFormStateImpl;

  @override
  TextEditingController get callsignController;
  @override
  TextEditingController get passcodeController;
  @override
  @JsonKey(ignore: true)
  _$$GeneralSettingsFormStateImplCopyWith<_$GeneralSettingsFormStateImpl>
      get copyWith => throw _privateConstructorUsedError;
}
