{
  "requestBleAccess": "Request BLE Access",
  "@requestBleAccess": {
    "description": "Button displayed when BLE access hasn't been granted"
  },
  "blePermissionError": "Permission Error: BLE access is required for device provisioning. Please allow access to BLE and precise location before continuing.",
  "@blePermissionError": {
    "description": "Error message displayed when BLE access hasn't been granted"
  },
  "nearbyDevices": "Nearby Devices",
  "language": "Language",
  "cancel": "Cancel",
  "ok": "Ok",
  "deviceSettings": "Device Settings",
  "deviceList": "My Devices",
  "tracker": "Tracker",
  "igate": "iGate",
  "digipeater": "Digipeater",
  "noResults": "No Results",
  "wifiNetworks": "WiFi Networks",
  "lookingForNetworks": "Looking for networks...",
  "lookingForDevices": "Looking for devices...",
  "connecting": "Connecting...",
  "connectionFailed": "Connection Failed",
  "connected": "Connected",
  "connect": "Connect",
  "connectTo": "Connect to {ssid}",
  "@connectTo": {
    "description": "Title of the popup dialog when connecting the device to WiFi. {ssid} is a placeholder for the WiFi network name",
    "placeholders": {
      "ssid": {
        "type": "String",
        "example": "My Home WiFi"
      }
    }
  },
  "password": "Password",
  "openMenu": "Open Menu",
  "followMe": "Follow Me",
  "requestPerm": "Request Perm.",
  "@requestPerm": {
    "description": "Button text, displayed in the banner error when a permission is missing. It should be kept as short as possible."
  },
  "missingLocationPerm": "Some features may not work. You need to grant location permissions first.",
  "@missingLocationPerm": {
    "description": "The banner error, displayed when location permission is missing"
  },
  "missingBlePerm": "Some features may not work. You need to grant BLE permissions first.",
  "@missingBlePerm": {
    "description": "The banner error, displayed when BLE permission is missing"
  },
  "connectDevice": "Connect Device",
  "noDevice": "No Device",
  "@noDevice": {
    "description": "Shown is the status bar at the top of the tracker screen. Should be short"
  },
  "general": "General",
  "saveChanges": "Save Changes",
  "confirm": "Confirm",
  "callsign": "Callsign",
  "aprsIsPasscode": "APRS-IS Passcode",
  "callsignHelp": "Your HAM callsign with SSID (e.g. S53GT-10)",
  "passcodeHelp": "Only required by the iGate component (can be -1 for RX-only)",
  "callsignInvalid": "Please enter a valid callsign",
  "passcodeInvalid": "Please enter a valid passcode",
  "wifi": "WiFi",
  "ssid": "SSID",
  "ipAddress": "IP Address",
  "gwAddress": "Gateway",
  "macAddress": "MAC Address",
  "netmask": "Netmask",
  "rssi": "RSSI",
  "status": "Status",
  "chooseAccessPoint": "Choose Access Point",
  "aprsSymbol": "APRS Symbol",
  "primary": "Primary",
  "secondary": "Secondary",
  "overlay": "Overlay",
  "pickSecondarySymbolFirst": "Pick a secondary symbol first",
  "somethingWentWrong": "Oops, something unexpected went wrong :/"
}
