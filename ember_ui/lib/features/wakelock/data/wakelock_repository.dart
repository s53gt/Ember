import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:wakelock_plus/wakelock_plus.dart';

part 'wakelock_repository.g.dart';

class WakelockRepository {
  WakelockRepository();

  Future<void> enable() => WakelockPlus.enable();
  Future<void> disable() => WakelockPlus.disable();
}

@riverpod
WakelockRepository wakelockRepository(WakelockRepositoryRef ref) {
  return WakelockRepository();
}
