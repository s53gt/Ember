# Ember

Ember is a set of LoRa APRS tools for licensed [HAM](https://en.wikipedia.org/wiki/Amateur_radio) operators. It consists
of a firmware package for an ESP32 board (pre-built or custom\*) and a cross-platform app which runs on Android, iOS\*\*,
   macOS\*\*, Windows, Linux, and the web.

\* You'll need to configure and compile the firmware yourself for custom boards.

\*\* iOS and macOS builds aren't tested or distributed by the maintainer. To run the app on an Apple platform, you
need to build it yourself. Instructions on how to do this are out of the scope of this project.

## Features

A list of features and their current state.

> **NOTE:** This project is currently in the early stages of development. If a feature you want isn't supported yet,
> check back in a few weeks.

**Done:**

 - Build configuration of low-level parameters via `idf.py menuconfig`
 - Runtime configuration via BLE + app
 - GPS support
 - LoRa APRS Rx iGate
 - REPL over USB for debugging and manual configuration
 - OTA firmware upgrade via REPL

**In Progress:**

 - Support for a number of popular LoRa boards and custom boards
 - OLED display support
 - Support most popular LoRa ICs
 - LoRa APRS tracker with Rx capability
 - Act as a KISS terminal over USB & BLE

 **TODO:**

 - OTA firmware upgrade via BLE and WiFi
 - LoRa APRS Tx iGate
 - LoRa APRS digipeater
 - LoRa APRS messaging
 - Logging to SD card

**Ideas:**

 - REPL over BLE
 - REPL over WiFi
 - Logging to network service
 - Authenticated device management over LoRa, while remaining conformant to the "no encryption" rule
 - Secure device management over the network
 - Support for multiple configuration profiles
