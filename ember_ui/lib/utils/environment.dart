import 'package:package_info_plus/package_info_plus.dart';

class Environment {
  static Environment? _instance;
  static Environment get instance => _instance!;

  PackageInfo? _packageInfo;
  PackageInfo get packageInfo => _packageInfo!;

  Environment();

  Future<void> _init() async {
    _packageInfo = await PackageInfo.fromPlatform();
  }

  static Future<void> init() async {
    if (_instance != null) return;
    _instance = Environment();
    await _instance!._init();
  }
}
