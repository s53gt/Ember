import 'package:ember_ui/features/permission/application/location_permission.dart';
import 'package:ember_ui/features/permission/domain/permission_result.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class LocationPermissionMiddleware extends ConsumerStatefulWidget {
  final Widget child;

  const LocationPermissionMiddleware({super.key, required this.child});

  @override
  ConsumerState<LocationPermissionMiddleware> createState() =>
      _LocationPermissionMiddlewareState();
}

class _LocationPermissionMiddlewareState
    extends ConsumerState<LocationPermissionMiddleware> {
  late final AppLifecycleListener _listener;
  Future<void>? _requestLocationPermissionFuture;
  ScaffoldFeatureController<MaterialBanner, MaterialBannerClosedReason>?
      _banner;

  @override
  void initState() {
    super.initState();
    _listener = AppLifecycleListener(
      onStateChange: _onLifecycleStateChanged,
    );
  }

  @override
  void dispose() {
    _listener.dispose();
    super.dispose();
  }

  void _onLifecycleStateChanged(AppLifecycleState state) {
    final locPermRes = ref.read(locationPermissionProvider);
    if (state == AppLifecycleState.resumed &&
        locPermRes == PermissionResult.permanentlyDenied) {
      final locPermNotif = ref.read(locationPermissionProvider.notifier);
      _requestLocationPermissionFuture = locPermNotif.request(false);
    }
  }

  @override
  Widget build(BuildContext context) {
    final locPermNotif = ref.read(locationPermissionProvider.notifier);
    _requestLocationPermissionFuture ??= locPermNotif.request(false);
    final locPermRes = ref.watch(locationPermissionProvider);
    if (isLocPermDenied(locPermRes) && _banner == null) {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        setState(() {
          _banner = ScaffoldMessenger.of(context).showMaterialBanner(
            MaterialBanner(
              backgroundColor: Theme.of(context).colorScheme.errorContainer,
              content: Builder(builder: (context) {
                final localizations = AppLocalizations.of(context)!;
                return Text(localizations.missingLocationPerm);
              }),
              actions: [
                TextButton(
                  style: ButtonStyle(
                    foregroundColor: WidgetStatePropertyAll(
                        Theme.of(context).colorScheme.error),
                  ),
                  onPressed: () => locPermNotif.request(true),
                  child: Builder(builder: (context) {
                    final localizations = AppLocalizations.of(context)!;
                    return Text(localizations.requestPerm);
                  }),
                ),
              ],
            ),
          );
        });
      });
    } else if (!isLocPermDenied(locPermRes) && _banner != null) {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        setState(() {
          _banner!.close();
          _banner = null;
        });
      });
    }
    return widget.child;
  }
}
