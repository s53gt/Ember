#pragma once

#include "esp_err.h"
#include "settings_default.h"

typedef enum {
  TNC2_SRC_PROTO_UNKNOWN = 0,
  TNC2_SRC_PROTO_LORA,
  TNC2_SRC_PROTO_APRS_IS,
} tnc2_src_proto_t;

typedef struct {
  char fromcall[HAM_STA_CALLSIGN_MAX_LEN + 1];
  char fromcall_ssid[HAM_STA_SSID_MAX_LEN + 1];
  char tocall[HAM_STA_CALLSIGN_MAX_LEN + 1];
  char tocall_ssid[HAM_STA_SSID_MAX_LEN + 1];
  char *path;
  char *data;
  size_t data_len;
  tnc2_src_proto_t proto;
} tnc2_packet_t;

typedef void (*tnc2_process_packet_cb)(const tnc2_packet_t *packet);
typedef struct {
  char name[10];
  tnc2_process_packet_cb process_packet;
} tnc2_proto_t;

typedef void (*tnc2_process_raw_packet_cb)(const uint8_t *packet, size_t len);
typedef struct {
  char name[10];
  tnc2_process_raw_packet_cb process_raw_packet;
} tnc2_tap_t;

esp_err_t tnc2_init(void);
esp_err_t tnc2_register_proto(const tnc2_proto_t *proto);
esp_err_t tnc2_register_tap(const tnc2_tap_t *tap);
esp_err_t tnc2_parse_packet(tnc2_src_proto_t proto, const uint8_t *data, size_t len, tnc2_packet_t *out);
