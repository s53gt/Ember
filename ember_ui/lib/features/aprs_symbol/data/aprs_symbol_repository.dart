import 'dart:convert';

import 'package:ember_ui/exceptions/malformed_exception.dart';
import 'package:flutter/services.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:image/image.dart' as img;

part 'aprs_symbol_repository.g.dart';

// Order matters!
enum AprsSymbolTable {
  primary,
  secondary,
  overlay,
}

String _symbolPath(AprsSymbolTable table, int size) =>
    "assets/aprs-symbols/aprs-symbols-$size-${table.index}.png";

class AprsSymbolRepository {
  final Map<AprsSymbolTable, Map<int, img.Image>> _tables = {};

  AprsSymbolRepository();

  Future<img.Image> _loadTable(AprsSymbolTable table, int size) async {
    if (_tables[table]?[size] != null) return _tables[table]![size]!;
    final path = _symbolPath(table, size);
    final tableSprite = await rootBundle.loadStructuredBinaryData(
      path,
      (data) => img.decodeImage(data.buffer.asUint8List()),
    );
    if (tableSprite == null) throw MalformedException();
    _tables[table] ??= {};
    _tables[table]![size] = tableSprite;
    return tableSprite;
  }

  img.Image _cropSymbol(img.Image tableSprite, String symbolChr) {
    const symbolsPerRow = 16;
    final size = (tableSprite.width / symbolsPerRow).floor();
    final index = utf8.encode(symbolChr)[0] - utf8.encode("!")[0];
    final xIdx = index % symbolsPerRow;
    final yIdx = (index / symbolsPerRow).floor();
    final x = xIdx * size;
    final y = yIdx * size;
    return img.copyCrop(tableSprite, x: x, y: y, width: size, height: size);
  }

  bool _isValidSymbolChr(String chr) {
    return utf8.encode(chr)[0] >= utf8.encode('!')[0] &&
        utf8.encode(chr)[0] <= utf8.encode('~')[0];
  }

  Future<Uint8List> getSymbol(String tableChr, String symbolChr,
      {int size = 64}) async {
    // Explicitly allow \x20 (space) for the table; this isn't part of the official spec, but rather an internal
    // extension, allowing overlay symbols to be displayed, without a base symbol
    if ((!_isValidSymbolChr(tableChr) && tableChr != "\x20") ||
        !_isValidSymbolChr(symbolChr)) {
      throw MalformedException();
    }
    AprsSymbolTable table = tableChr == "/"
        ? AprsSymbolTable.primary
        : tableChr == "\x20"
            ? AprsSymbolTable.overlay
            : AprsSymbolTable.secondary;
    final tableSprite = await _loadTable(table, size);
    var symbol = _cropSymbol(tableSprite, symbolChr);
    if (tableChr != "/" && tableChr != "\\" && tableChr != "\x20") {
      final tableSprite = await _loadTable(AprsSymbolTable.overlay, size);
      final overlay = _cropSymbol(tableSprite, tableChr);
      symbol = img.compositeImage(symbol, overlay);
    }
    symbol = img.invert(symbol); // Quasi icon dark mode
    return img.encodePng(symbol);
  }
}

@riverpod
AprsSymbolRepository aprsSymbolRepository(AprsSymbolRepositoryRef ref) {
  return AprsSymbolRepository();
}
