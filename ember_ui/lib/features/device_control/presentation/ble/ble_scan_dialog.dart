import 'package:ember_ui/features/device_control/presentation/ble/ble_scan_list.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

Future<void> showBleScanDialog(BuildContext context) {
  return showDialog(
    context: context,
    builder: (context) => AlertDialog(
      content: SizedBox(
        width: 300,
        height: 200,
        child: BleScanList(
          onConnected: (_) => Navigator.of(context).pop(),
        ),
      ),
      actions: <Widget>[
        TextButton(
          onPressed: () => Navigator.of(context).pop(),
          child: Text(AppLocalizations.of(context)!.cancel),
        ),
      ],
    ),
  );
}
