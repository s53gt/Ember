import 'package:ember_ui/features/navigation/presentation/floating_app_bar.dart';
import 'package:ember_ui/features/navigation/presentation/nav_drawer.dart';
import 'package:ember_ui/features/tracker/presentation/tracker_connect_button.dart';
import 'package:ember_ui/features/tracker/presentation/tracker_device_status.dart';
import 'package:flutter/material.dart';

class TrackerLayout extends StatelessWidget {
  final Widget body;

  const TrackerLayout({
    super.key,
    required this.body,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const NavDrawer(),
      body: Stack(
        children: [
          body,
          Padding(
            padding: EdgeInsets.only(
              left: 10,
              right: 10,
              top: MediaQuery.of(context).viewPadding.top + 5,
            ),
            child: const Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                FloatingAppBar(),
                Expanded(
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: TrackerDeviceStatus(),
                  ),
                ),
                TrackerConnectButton(),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
