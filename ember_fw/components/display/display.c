#include "display.h"
#include "sdkconfig.h"

#if CONFIG_DISPLAY_ENABLE
#include "driver/i2c_master.h"
#include "driver/i2c_types.h"
#include "esp_check.h"
#include "esp_err.h"
#include "esp_lcd_panel_io.h"
#include "esp_lcd_panel_ops.h"
#include "esp_lcd_panel_vendor.h"
#include "esp_lcd_types.h"
#include "esp_lvgl_port.h"
#include "ui.h"

static const char *TAG = "display";

static esp_err_t display_lvgl_init();

static i2c_master_bus_handle_t i2c_bus = NULL;
static esp_lcd_panel_io_handle_t io_handle = NULL;
static esp_lcd_panel_handle_t panel_handle = NULL;

esp_err_t display_init(void) {
  esp_err_t err;
  i2c_master_bus_config_t bus_config = {
      .clk_source = I2C_CLK_SRC_DEFAULT,
      .glitch_ignore_cnt = 7,
      .i2c_port = I2C_NUM_0,
      .sda_io_num = CONFIG_DISPLAY_SDA,
      .scl_io_num = CONFIG_DISPLAY_SCL,
      .flags.enable_internal_pullup = true,
  };
  err = i2c_new_master_bus(&bus_config, &i2c_bus);
  ESP_RETURN_ON_ERROR(err, TAG, "Failed to create I2C master bus");
  esp_lcd_panel_io_i2c_config_t io_config = {
      .dev_addr = CONFIG_DISPLAY_I2C_ADDR,
      .scl_speed_hz = CONFIG_DISPLAY_I2C_FREQ_HZ,
      .control_phase_bytes = 1,
      .dc_bit_offset = 6,
      .lcd_cmd_bits = 8,
      .lcd_param_bits = 8,
  };
  err = esp_lcd_new_panel_io_i2c(i2c_bus, &io_config, &io_handle);
  ESP_RETURN_ON_ERROR(err, TAG, "Failed to init LCD panel I/O");
  esp_lcd_panel_dev_config_t panel_config = {
      .bits_per_pixel = 1,
      .reset_gpio_num = -1,
  };
  err = esp_lcd_new_panel_ssd1306(io_handle, &panel_config, &panel_handle);
  ESP_RETURN_ON_ERROR(err, TAG, "Failed to init SSD1306 driver");
  ESP_RETURN_ON_ERROR(esp_lcd_panel_reset(panel_handle), TAG, "Failed to reset LCD panel");
  ESP_RETURN_ON_ERROR(esp_lcd_panel_init(panel_handle), TAG, "Failed to init LCD panel");
  ESP_RETURN_ON_ERROR(esp_lcd_panel_disp_on_off(panel_handle, true), TAG, "Failed to turn display on");
  ESP_RETURN_ON_ERROR(display_lvgl_init(), TAG, "Failed to init LVGL");
  ESP_RETURN_ON_ERROR(ui_init(), TAG, "Failed to init UI");
  return ESP_OK;
}

static esp_err_t display_lvgl_init() {
  const lvgl_port_cfg_t lvgl_cfg = ESP_LVGL_PORT_INIT_CONFIG();
  lvgl_port_init(&lvgl_cfg);
  const lvgl_port_display_cfg_t disp_cfg = {.io_handle = io_handle,
                                            .panel_handle = panel_handle,
                                            .buffer_size = CONFIG_DISPLAY_HEIGHT_PX * CONFIG_DISPLAY_WIDTH_PX,
                                            .double_buffer = true,
                                            .hres = CONFIG_DISPLAY_WIDTH_PX,
                                            .vres = CONFIG_DISPLAY_HEIGHT_PX,
                                            .monochrome = true,
                                            .rotation = {
                                                .swap_xy = false,
                                                .mirror_x = true,
                                                .mirror_y = true,
                                            }};
  lv_disp_t *disp = lvgl_port_add_disp(&disp_cfg);
  lv_disp_set_rotation(disp, LV_DISP_ROT_NONE);
  return ESP_OK;
}
#else
esp_err_t display_init(void) { return ESP_OK; }
#endif
