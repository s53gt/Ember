import 'package:ember_ui/features/device_control/domain/device.dart';
import 'package:ember_ui/features/device_control/domain/wifi_network.dart';
import 'package:ember_ui/features/device_control/domain/wifi_state.dart';

abstract class WifiRepository {
  Future<List<WifiNetwork>> scan(Device device);
  Future<void> connect(Device device, WifiNetwork network, String? password);
  Future<WifiState> getState(Device device);
  Stream<WifiState> subscribeWifiState(Device device);
}
