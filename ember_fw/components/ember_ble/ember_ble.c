#include "ember_ble.h"

#include <math.h>
#include <stdint.h>

#include "ember_ble_gatts.h"
#include "esp_check.h"
#include "esp_err.h"
#include "esp_event.h"
#include "esp_event_base.h"
#include "esp_log.h"
#include "host/ble_gap.h"
#include "host/ble_gatt.h"
#include "host/ble_hs.h"
#include "host/ble_hs_adv.h"
#include "host/ble_hs_mbuf.h"
#include "host/ble_sm.h"
#include "host/ble_store.h"
#include "host/ble_uuid.h"
#include "host/util/util.h"
#include "nimble/nimble_port.h"
#include "nimble/nimble_port_freertos.h"
#include "os/os_mbuf.h"
#include "sdkconfig.h"
#include "services/gap/ble_svc_gap.h"
#include "settings.h"
#include "settings_default.h"

#define MIN(X, Y) ((X) < (Y) ? (X) : (Y))

static const char* TAG = "ember_ble";

static uint8_t own_addr_type;
static char name[32] = {0};
static esp_event_handler_instance_t settings_event_handler;
static uint16_t conn_handle_subs[CONFIG_BT_NIMBLE_MAX_CONNECTIONS + 1][EMBER_BLE_MAX_NOTIFY] = {0};

void ble_store_config_init(void);
static void ember_ble_advertise(void);

static void ember_ble_on_reset(int reason) { ESP_LOGE(TAG, "Resetting BLE state, reason=%d", reason); }

static int ember_ble_gap_event(struct ble_gap_event* event, void* arg) {
  struct ble_gap_conn_desc desc;
  int rc;
  switch (event->type) {
    case BLE_GAP_EVENT_CONNECT:
      ESP_LOGI(TAG, "connection %s; status=%d ", event->connect.status == 0 ? "established" : "failed",
               event->connect.status);
      if (event->connect.status != 0) {
        ember_ble_advertise();
      }
      return 0;

    case BLE_GAP_EVENT_DISCONNECT:
      ESP_LOGI(TAG, "disconnect; reason=%d ", event->disconnect.reason);
      ember_ble_advertise();
      return 0;

    case BLE_GAP_EVENT_CONN_UPDATE:
      ESP_LOGI(TAG, "connection updated; status=%d ", event->conn_update.status);
      return 0;

    case BLE_GAP_EVENT_ENC_CHANGE:
      ESP_LOGI(TAG, "encryption change event; status=%d ", event->enc_change.status);
      return 0;

    case BLE_GAP_EVENT_ADV_COMPLETE:
      ESP_LOGI(TAG, "advertise complete; reason=%d", event->adv_complete.reason);
      ember_ble_advertise();
      return 0;

    case BLE_GAP_EVENT_NOTIFY_TX:
      ESP_LOGI(TAG,
               "notify_tx event; conn_handle=%d attr_handle=%d "
               "status=%d is_indication=%d",
               event->notify_tx.conn_handle, event->notify_tx.attr_handle, event->notify_tx.status,
               event->notify_tx.indication);
      return 0;

    case BLE_GAP_EVENT_SUBSCRIBE:
      ESP_LOGI(TAG,
               "subscribe event; conn_handle=%d attr_handle=%d "
               "reason=%d prevn=%d curn=%d previ=%d curi=%d\n",
               event->subscribe.conn_handle, event->subscribe.attr_handle, event->subscribe.reason,
               event->subscribe.prev_notify, event->subscribe.cur_notify, event->subscribe.prev_indicate,
               event->subscribe.cur_indicate);
      uint16_t* subs = conn_handle_subs[event->subscribe.conn_handle];
      for (int8_t i = 0; i < EMBER_BLE_MAX_NOTIFY; i++) {
        if (subs[i] == event->subscribe.attr_handle) {
          if (event->subscribe.cur_notify) return 0;  // Already subscribed
          subs[i] = 0;                                // Subscribed, but got request to unsubscribe
          return 0;
        }
      }
      // Not yet subscribed, but got request to subscribe
      for (int8_t i = 0; i < EMBER_BLE_MAX_NOTIFY; i++) {
        if (!subs[i]) {
          subs[i] = event->subscribe.attr_handle;
          return 0;
        }
      }
      return 0;

    case BLE_GAP_EVENT_MTU:
      ESP_LOGI(TAG, "mtu update event; conn_handle=%d cid=%d mtu=%d\n", event->mtu.conn_handle, event->mtu.channel_id,
               event->mtu.value);
      return 0;

    case BLE_GAP_EVENT_REPEAT_PAIRING:
      ESP_LOGW(TAG, "Repeat pairing...");
      rc = ble_gap_conn_find(event->repeat_pairing.conn_handle, &desc);
      assert(rc == 0);
      ble_store_util_delete_peer(&desc.peer_id_addr);
      return BLE_GAP_REPEAT_PAIRING_RETRY;

    case BLE_GAP_EVENT_PASSKEY_ACTION:
      ESP_LOGI(TAG, "PASSKEY_ACTION_EVENT started");
      struct ble_sm_io pkey = {0};
      rc = 0;
      if (event->passkey.params.action == BLE_SM_IOACT_DISP) {
        pkey.action = event->passkey.params.action;
        pkey.passkey = 123456;  // This is the passkey to be entered on peer
        ESP_LOGI(TAG, "Enter passkey %" PRIu32 "on the peer side", pkey.passkey);
        rc = ble_sm_inject_io(event->passkey.conn_handle, &pkey);
        ESP_LOGI(TAG, "ble_sm_inject_io result: %d", rc);
      } else {
        ESP_LOGE(TAG, "Unsupported passkey action, action=0x%X", event->type);
        return 1;
      }
      rc = ble_sm_inject_io(event->passkey.conn_handle, &pkey);
      ESP_LOGI(TAG, "ble_sm_inject_io result: %d", rc);
      return 0;

    case BLE_GAP_EVENT_AUTHORIZE:
      ESP_LOGI(TAG, "authorize event: conn_handle=%d attr_handle=%d is_read=%d", event->authorize.conn_handle,
               event->authorize.attr_handle, event->authorize.is_read);

      /* The default behaviour for the event is to reject authorize request */
      event->authorize.out_response = BLE_GAP_AUTHORIZE_REJECT;
      return 0;

    default:
      ESP_LOGW(TAG, "Unknown GAP event: 0x%02X", event->type);
      return 0;
  }

  return 0;
}

void ember_ble_notify(uint16_t chr_val_handle) {
  for (int i = 0; i < CONFIG_BT_NIMBLE_MAX_CONNECTIONS; i++) {
    for (int8_t j = 0; j < EMBER_BLE_MAX_NOTIFY; j++) {
      if (conn_handle_subs[i][j] == chr_val_handle) {
        int rc = ble_gatts_notify(i, chr_val_handle);
        if (rc != 0) ESP_LOGW(TAG, "Failed to send notification, rc=%d", rc);
        break;
      }
    }
  }
}

void ember_ble_notify_custom(uint16_t chr_val_handle, void* buf, uint16_t len) {
  for (int i = 0; i < CONFIG_BT_NIMBLE_MAX_CONNECTIONS; i++) {
    for (int8_t j = 0; j < EMBER_BLE_MAX_NOTIFY; j++) {
      if (conn_handle_subs[i][j] == chr_val_handle) {
        uint16_t mtu = ble_att_mtu(i) - 3 /* NOTIFY uses up 3 bytes per packet */;
        if (!mtu) {
          ESP_LOGW(TAG, "Failed to get MTU for connection handle %d", mtu);
          break;
        }
        uint16_t fragments = (uint16_t)ceil((double)len / mtu);
        for (uint16_t k = 0; k < fragments; k++) {
          struct os_mbuf* txom = ble_hs_mbuf_from_flat(buf + mtu * k, MIN(len - mtu * k, mtu));
          int rc = ble_gatts_notify_custom(i, chr_val_handle, txom);
          if (rc != 0) {
            ESP_LOGW(TAG, "Failed to send notification, rc=%d", rc);
            break;
          }
        }
        break;
      }
    }
  }
}

static void ember_ble_advertise(void) {
  struct ble_gap_adv_params adv_params;
  struct ble_hs_adv_fields fields;
  const char* name;
  int rc;
  memset(&fields, 0, sizeof(fields));
  fields.flags = BLE_HS_ADV_F_DISC_GEN | BLE_HS_ADV_F_BREDR_UNSUP;
  fields.tx_pwr_lvl_is_present = 1;
  fields.tx_pwr_lvl = BLE_HS_ADV_TX_PWR_LVL_AUTO;
  name = ble_svc_gap_device_name();
  fields.name = (uint8_t*)name;
  fields.name_len = strlen(name);
  fields.name_is_complete = 1;
  rc = ble_gap_adv_set_fields(&fields);
  if (rc != 0) {
    ESP_LOGE(TAG, "error setting advertisement fields; rc=%d\n", rc);
    return;
  }
  memset(&adv_params, 0, sizeof adv_params);
  adv_params.conn_mode = BLE_GAP_CONN_MODE_UND;
  adv_params.disc_mode = BLE_GAP_DISC_MODE_GEN;
  rc = ble_gap_adv_start(own_addr_type, NULL, BLE_HS_FOREVER, &adv_params, ember_ble_gap_event, NULL);
  if (rc != 0) {
    ESP_LOGE(TAG, "error starting advertisement; rc=%d\n", rc);
    return;
  }
}

static void ember_ble_on_sync(void) {
  ESP_LOGD(TAG, "BLE sync");
  int rc;
  rc = ble_hs_util_ensure_addr(0);
  assert(rc == 0);
  rc = ble_hs_id_infer_auto(0, &own_addr_type);
  if (rc != 0) {
    ESP_LOGE(TAG, "error determining address type; rc=%d\n", rc);
    return;
  }
  uint8_t addr_val[6] = {0};
  rc = ble_hs_id_copy_addr(own_addr_type, addr_val, NULL);
  ESP_LOGI(TAG, "Device Address: %02X:%02X:%02X:%02X:%02X:%02X", addr_val[5], addr_val[4], addr_val[3], addr_val[2],
           addr_val[1], addr_val[0]);
  ember_ble_advertise();
}

static void ember_ble_host_task(void* arg) {
  ESP_LOGI(TAG, "Provisioning BLE host task starting...");
  nimble_port_run();
  nimble_port_freertos_deinit();
}

static esp_err_t ember_ble_set_name(void) {
  size_t len = HAM_STA_CALL_SSID_MAX_LEN;
  char callsign[HAM_STA_CALL_SSID_MAX_LEN + 1] = {0};
  esp_err_t err = settings_get_str_default(SETTINGS_KEY_CALLSIGN, callsign, &len, HAM_STA_CALLSIGN_DEFAULT);
  ESP_RETURN_ON_ERROR(err, TAG, "Failed to get callsign from settings");
  int ret = sprintf(name, "ember_%s", callsign);
  ESP_RETURN_ON_FALSE(ret > 0, ESP_FAIL, TAG, "Failed to format BLE name");
  ret = ble_svc_gap_device_name_set(name);
  ESP_RETURN_ON_FALSE(ret == 0, ESP_FAIL, TAG, "Failed to set BLE device name, ret=%d", ret);
  return ESP_OK;
}

static void ember_ble_on_setting_updated(void* event_handler_arg, esp_event_base_t event_base, int32_t event_id,
                                         void* event_data) {
  if (event_id == SETTINGS_EVENT_UPDATED) {
    settings_updated_event_t* event = (settings_updated_event_t*)event_data;
    if (strcmp(event->key, SETTINGS_KEY_CALLSIGN) == 0) {
      esp_err_t err = ember_ble_set_name();
      if (err != ESP_OK) ESP_LOGW(TAG, "Failed to update BLE name");
    }
  }
}

esp_err_t ember_ble_init(void) {
  ESP_RETURN_ON_ERROR(nimble_port_init(), TAG, "Failed to init NimBLE stack");
  ble_hs_cfg.reset_cb = ember_ble_on_reset;
  ble_hs_cfg.sync_cb = ember_ble_on_sync;
  ble_hs_cfg.gatts_register_cb = ember_ble_on_gatts_register;
  ble_hs_cfg.store_status_cb = ble_store_util_status_rr;
#if DISPLAY_ENABLE
  ble_hs_cfg.sm_io_cap = BLE_SM_IO_CAP_DISP_ONLY;
#else
  ble_hs_cfg.sm_io_cap = BLE_SM_IO_CAP_NO_IO;
#endif
  ble_hs_cfg.sm_bonding = 1;
  ble_hs_cfg.sm_our_key_dist |= BLE_SM_PAIR_KEY_DIST_ID | BLE_SM_PAIR_KEY_DIST_ENC;
  ble_hs_cfg.sm_their_key_dist |= BLE_SM_PAIR_KEY_DIST_ID | BLE_SM_PAIR_KEY_DIST_ENC;
  ble_hs_cfg.sm_mitm = 1;
  ble_hs_cfg.sm_sc = 1;
  ESP_RETURN_ON_ERROR(ember_ble_gatts_init(), TAG, "Failed to init BLE GATT SVR");
  ESP_RETURN_ON_ERROR(ember_ble_set_name(), TAG, "Failed to set BLE name");
  esp_err_t err = esp_event_handler_instance_register(SETTINGS_EVENT, SETTINGS_EVENT_UPDATED,
                                                      ember_ble_on_setting_updated, NULL, settings_event_handler);
  ESP_RETURN_ON_ERROR(err, TAG, "Failed to register settings event handler");
  ble_store_config_init();
  nimble_port_freertos_init(ember_ble_host_task);
  return ESP_OK;
}
