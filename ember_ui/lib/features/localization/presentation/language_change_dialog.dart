import 'dart:math';

import 'package:circle_flags/circle_flags.dart';
import 'package:ember_ui/features/localization/domain/language_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final List<Locale> _supportedLanguages = [
  const Locale('en', "us"),
  const Locale('sl', "si"),
];

Widget _languageDialogList(
    BuildContext context, BoxConstraints constraints, WidgetRef ref) {
  var lang = ref.read(languageProvider.notifier);
  return SizedBox(
      width: min(300, constraints.maxWidth * .9),
      height: min(200, constraints.maxHeight * .7),
      child: ListView(
        shrinkWrap: true,
        children: _supportedLanguages
            .map<Widget>((locale) => ListTile(
                  onTap: () {
                    lang.setLocale(locale.languageCode);
                    Navigator.of(context).pop();
                  },
                  title: Text(locale.languageCode.toUpperCase()),
                  leading: CircleFlag(
                    locale.countryCode ?? '',
                    size: 32,
                  ),
                ))
            .toList(),
      ));
}

Future<void> showLanguageChangeDialog(BuildContext context) {
  return showDialog(
    context: context,
    builder: (BuildContext context) => LayoutBuilder(
      builder: (context, constraints) => AlertDialog(
        title: Text(AppLocalizations.of(context)!.language),
        content: Consumer(
          builder: (context, ref, _) =>
              _languageDialogList(context, constraints, ref),
        ),
        actions: <Widget>[
          TextButton(
            onPressed: () => Navigator.of(context).pop(),
            child: Text(AppLocalizations.of(context)!.cancel),
          ),
        ],
      ),
    ),
  );
}
