#pragma once

#include "esp_err.h"
#include "freertos/FreeRTOS.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct {
  uint8_t *data;
  size_t len;
} lora_packet_t;

typedef bool (*lora_detect_packet_cb)(const lora_packet_t *packet);
typedef void (*lora_process_packet_cb)(const lora_packet_t *packet);
typedef struct {
  char name[10];
  lora_detect_packet_cb detect_packet;
  lora_process_packet_cb process_packet;
} lora_proto_t;

esp_err_t lora_init(void);
esp_err_t lora_transmit(uint8_t *data, size_t len, TickType_t xTicksToWait);
esp_err_t lora_register_proto(const lora_proto_t *proto);
#ifdef __cplusplus
}
#endif
