// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ble_device_repository.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$bleDeviceRepositoryHash() =>
    r'00c1796295abe1aedc9fb55dff5b90040b7f7ccb';

/// See also [bleDeviceRepository].
@ProviderFor(bleDeviceRepository)
final bleDeviceRepositoryProvider =
    AutoDisposeProvider<BleDeviceRepository>.internal(
  bleDeviceRepository,
  name: r'bleDeviceRepositoryProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$bleDeviceRepositoryHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef BleDeviceRepositoryRef = AutoDisposeProviderRef<BleDeviceRepository>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
