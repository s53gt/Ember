#include "services/ble_svc_ham_sta.h"

#include <stdint.h>

#include "ember_ble_private.h"
#include "esp_check.h"
#include "esp_err.h"
#include "esp_event.h"
#include "esp_event_base.h"
#include "host/ble_att.h"
#include "host/ble_gatt.h"
#include "host/ble_hs.h"
#include "host/ble_hs_mbuf.h"
#include "os/os_mbuf.h"
#include "settings.h"
#include "settings_default.h"

static const char *TAG = "ble_svc_ham_sta";

static int ham_sta_access(uint16_t conn_handle, uint16_t attr_handle, struct ble_gatt_access_ctxt *ctx, void *arg);

static esp_event_handler_instance_t settings_updated_handler;

static uint16_t ham_sta_callsign_chr_val_handle;
static uint16_t ham_sta_passcode_chr_val_handle;
static uint16_t ham_sta_symbol_chr_val_handle;
const struct ble_gatt_svc_def ham_sta_svc = {
    .type = BLE_GATT_SVC_TYPE_PRIMARY,
    .uuid = &ham_sta_svc_uuid.u,
    .characteristics =
        (struct ble_gatt_chr_def[]){
            {
                .uuid = &ham_sta_callsign_chr_uuid.u,
                .access_cb = ham_sta_access,
                .val_handle = &ham_sta_callsign_chr_val_handle,
                .flags = BLE_GATT_CHR_F_READ | BLE_GATT_CHR_F_WRITE | BLE_GATT_CHR_F_READ_ENC |
                         BLE_GATT_CHR_F_WRITE_ENC | BLE_GATT_CHR_F_NOTIFY,
            },
            {
                .uuid = &ham_sta_passcode_chr_uuid.u,
                .access_cb = ham_sta_access,
                .val_handle = &ham_sta_passcode_chr_val_handle,
                .flags = BLE_GATT_CHR_F_READ | BLE_GATT_CHR_F_WRITE | BLE_GATT_CHR_F_READ_ENC |
                         BLE_GATT_CHR_F_WRITE_ENC | BLE_GATT_CHR_F_NOTIFY,
            },
            {
                .uuid = &ham_sta_symbol_chr_uuid.u,
                .access_cb = ham_sta_access,
                .val_handle = &ham_sta_symbol_chr_val_handle,
                .flags = BLE_GATT_CHR_F_READ | BLE_GATT_CHR_F_WRITE | BLE_GATT_CHR_F_READ_ENC |
                         BLE_GATT_CHR_F_WRITE_ENC | BLE_GATT_CHR_F_NOTIFY,
            },
            {0},
        },
};

static int ham_sta_read_str_chr(struct os_mbuf *om, const char *key, char *str, size_t *len, const char *def) {
  esp_err_t err;
  int rc;
  err = settings_get_str_default(key, str, len, def);
  if (err != ESP_OK) return BLE_ATT_ERR_INSUFFICIENT_RES;
  rc = os_mbuf_append(om, str, *len - 1);
  return rc == 0 ? 0 : BLE_ATT_ERR_INSUFFICIENT_RES;
}

static int ham_sta_write_str_chr(struct os_mbuf *om, uint16_t attr_handle, const char *key, char *str,
                                 uint16_t max_len) {
  esp_err_t err;
  int rc;
  uint16_t len = OS_MBUF_PKTLEN(om);
  if (len > max_len - 1) return BLE_ATT_ERR_INVALID_ATTR_VALUE_LEN;
  rc = ble_hs_mbuf_to_flat(om, str, max_len - 1, &len);
  if (rc != 0) return BLE_ATT_ERR_INSUFFICIENT_RES;
  err = settings_set_str(key, str);
  if (err != ESP_OK) return BLE_ATT_ERR_INSUFFICIENT_RES;
  ble_gatts_chr_updated(attr_handle);
  return 0;
}

static int ham_sta_access(uint16_t conn_handle, uint16_t attr_handle, struct ble_gatt_access_ctxt *ctx, void *arg) {
  switch (ctx->op) {
    case BLE_GATT_ACCESS_OP_READ_CHR:
      if (conn_handle == BLE_HS_CONN_HANDLE_NONE) {
        ESP_LOGI(TAG, "Characteristic read by NimBLE stack, attr_handle=%d", attr_handle);
      } else {
        ESP_LOGI(TAG, "Characteristic read, conn_handle=%d, attr_handle=%d", conn_handle, attr_handle);
      }
      if (attr_handle == ham_sta_callsign_chr_val_handle) {
        size_t len = HAM_STA_CALL_SSID_MAX_LEN + 1;
        char callsign[HAM_STA_CALL_SSID_MAX_LEN + 1] = {0};
        return ham_sta_read_str_chr(ctx->om, SETTINGS_KEY_CALLSIGN, callsign, &len, HAM_STA_CALLSIGN_DEFAULT);
      } else if (attr_handle == ham_sta_passcode_chr_val_handle) {
        size_t len = APRS_IS_PASSCODE_MAX_LEN + 1;
        char passcode[APRS_IS_PASSCODE_MAX_LEN + 1] = {0};
        return ham_sta_read_str_chr(ctx->om, SETTINGS_KEY_PASSCODE, passcode, &len, APRS_IS_PASSCODE_DEFAULT);
      } else if (attr_handle == ham_sta_symbol_chr_val_handle) {
        size_t len = HAM_STA_SYMBOL_LEN + 1;
        char symbol[HAM_STA_SYMBOL_LEN + 1] = {0};
        return ham_sta_read_str_chr(ctx->om, SETTINGS_KEY_SYMBOL, symbol, &len, HAM_STA_SYMBOL_DEFAULT);
      }
      break;
    case BLE_GATT_ACCESS_OP_WRITE_CHR:
      if (conn_handle == BLE_HS_CONN_HANDLE_NONE) {
        ESP_LOGI(TAG, "Characteristic write by NimBLE stack, attr_handle=%d", attr_handle);
      } else {
        ESP_LOGI(TAG, "Characteristic write, conn_handle=%d, attr_handle=%d", conn_handle, attr_handle);
      }
      if (attr_handle == ham_sta_callsign_chr_val_handle) {
        size_t len = HAM_STA_CALL_SSID_MAX_LEN + 1;
        char callsign[HAM_STA_CALL_SSID_MAX_LEN + 1] = {0};
        return ham_sta_write_str_chr(ctx->om, attr_handle, SETTINGS_KEY_CALLSIGN, callsign, len);
      } else if (attr_handle == ham_sta_passcode_chr_val_handle) {
        size_t len = APRS_IS_PASSCODE_MAX_LEN + 1;
        char passcode[APRS_IS_PASSCODE_MAX_LEN + 1] = {0};
        return ham_sta_write_str_chr(ctx->om, attr_handle, SETTINGS_KEY_PASSCODE, passcode, len);
      } else if (attr_handle == ham_sta_symbol_chr_val_handle) {
        size_t len = HAM_STA_SYMBOL_LEN + 1;
        char symbol[HAM_STA_SYMBOL_LEN + 1] = {0};
        return ham_sta_write_str_chr(ctx->om, attr_handle, SETTINGS_KEY_SYMBOL, symbol, len);
      }
      break;
  }
  return BLE_ATT_ERR_UNLIKELY;
}

void ble_svc_ham_sta_on_setting_updated(void *arg, esp_event_base_t base, int32_t id, void *event_data) {
  settings_updated_event_t *event = (settings_updated_event_t *)event_data;
  if (strcmp(event->key, SETTINGS_KEY_CALLSIGN) == 0) {
    size_t len = HAM_STA_CALL_SSID_MAX_LEN + 1;
    char callsign[HAM_STA_CALL_SSID_MAX_LEN + 1] = {0};
    esp_err_t err = settings_get_str(SETTINGS_KEY_CALLSIGN, callsign, &len);
    if (err == ESP_OK) {
      ember_ble_notify_custom(ham_sta_callsign_chr_val_handle, callsign, len);
    }
  } else if (strcmp(event->key, SETTINGS_KEY_PASSCODE) == 0) {
    size_t len = APRS_IS_PASSCODE_MAX_LEN + 1;
    char passcode[APRS_IS_PASSCODE_MAX_LEN + 1] = {0};
    esp_err_t err = settings_get_str(SETTINGS_KEY_PASSCODE, passcode, &len);
    if (err == ESP_OK) {
      ember_ble_notify_custom(ham_sta_passcode_chr_val_handle, passcode, len);
    }
  } else if (strcmp(event->key, SETTINGS_KEY_SYMBOL) == 0) {
    size_t len = HAM_STA_SYMBOL_LEN + 1;
    char symbol[HAM_STA_SYMBOL_LEN + 1] = {0};
    esp_err_t err = settings_get_str(SETTINGS_KEY_SYMBOL, symbol, &len);
    if (err == ESP_OK) {
      ember_ble_notify_custom(ham_sta_symbol_chr_val_handle, symbol, len);
    }
  }
}

esp_err_t ble_svc_ham_sta_init(void) {
  esp_err_t ret = esp_event_handler_instance_register(
      SETTINGS_EVENT, SETTINGS_EVENT_UPDATED, ble_svc_ham_sta_on_setting_updated, NULL, &settings_updated_handler);
  ESP_RETURN_ON_ERROR(ret, TAG, "Failed to register SETTINGS_EVENT_UPDATED event handler");
  return ESP_OK;
  ;
}
