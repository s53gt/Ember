import 'package:freezed_annotation/freezed_annotation.dart';

part 'wifi_state.freezed.dart';

enum WifiStatus {
  disconnected,
  connected,
}

@freezed
class WifiState with _$WifiState {
  const factory WifiState({
    required String ssid,
    required String ipAddress,
    required String gwAddress,
    required String netmask,
    required String macAddress,
    required int rssi,
    required WifiStatus status,
  }) = _WifiState;
}
