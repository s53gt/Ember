import 'dart:async';

import 'package:ember_ui/constants/ble_constants.dart';
import 'package:ember_ui/exceptions/malformed_exception.dart';
import 'package:ember_ui/features/device_control/application/device_service.dart';
import 'package:ember_ui/features/device_control/domain/device.dart';
import 'package:ember_ui/features/tracker/application/tnc_service.dart';
import 'package:ember_ui/features/tracker/domain/aprs_position.dart';
import 'package:ember_ui/features/tracker/domain/tnc2_header.dart';
import 'package:ember_ui/utils/consumable_string.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'aprs_service.g.dart';

class AprsService {
  final DeviceService _deviceService;
  final TncService _tncService;
  late final StreamController<AprsPosition> _positionReportController;
  StreamSubscription<Device?>? _connectedDeviceSubscription;
  StreamSubscription<String>? _tncSubscription;
  Device? _lastDevice;
  bool _positionReportHasListeners = false;

  AprsService(this._deviceService, this._tncService) {
    _positionReportController = StreamController.broadcast(
      onListen: () => _positionReportHasListeners = true,
      onCancel: () => _positionReportHasListeners = false,
    );
  }

  Stream<AprsPosition> get positionReports => _positionReportController.stream;

  void start() {
    if (_connectedDeviceSubscription != null) return;
    _connectedDeviceSubscription =
        _deviceService.connectedDeviceStream.listen((device) {
      if (_lastDevice?.id == device?.id) return;
      _lastDevice = device;
      _listenAprs();
    });
    _listenAprs();
  }

  void _listenAprs() {
    _tncSubscription?.cancel();
    _tncSubscription = null;
    if (_deviceService.connectedDevice == null) return;
    _tncSubscription = _tncService.packets().listen(_onPacket);
  }

  void dispose() {
    _connectedDeviceSubscription?.cancel();
    _tncSubscription?.cancel();
  }

  void _addPosition(AprsPosition position) {
    if (_positionReportHasListeners) {
      _positionReportController.add(position);
    }
  }

  String _parseCallsign(ConsumableString buffer) {
    var callsign =
        buffer.takeWhile((c) => ![">", ",", ":"].contains(c)).join("");
    if (callsign.isEmpty || callsign.length > bleHamStaCallsignMaxLen) {
      throw MalformedException();
    }
    return callsign;
  }

  String _parsePath(ConsumableString buffer) {
    // Path may be omitted
    if (buffer.current == ":") return "";
    var path = "";
    while (buffer.moveNext() && buffer.current != ":") {
      path += buffer.current;
    }
    return path;
  }

  Tnc2Header _parseTnc2Header(ConsumableString buffer) {
    final from = _parseCallsign(buffer);
    if (buffer.isEmpty) throw MalformedException();
    final to = _parseCallsign(buffer);
    if (buffer.isEmpty) throw MalformedException();
    var path = _parsePath(buffer);
    if (buffer.isEmpty) throw MalformedException();
    return Tnc2Header(from: from, to: to, path: path);
  }

  String _parseDti(ConsumableString buffer) {
    buffer.mark(); // Save the current position, so we can later rollback
    // The '!' DTI is a bit special; It may occur anywhere in the first 40 characters
    // See page 18 of the spec http://www.aprs.org/doc/APRS101.PDF
    if (buffer.take(40).contains("!")) return "!";
    buffer.rollback();
    if (!buffer.moveNext()) throw MalformedException();
    return buffer.current;
  }

  DateTime _parseTimestamp(ConsumableString buffer) {
    List<int> digits = [];
    var tmp = "";
    // Timestamp can be up to 8 characters long, depending on the format
    for (var c in buffer.take(8)) {
      if (!RegExp(r'[0-9]').hasMatch(c) && !["z", "/", "h"].contains(c)) {
        throw MalformedException();
      }
      switch (c) {
        case "z": // Zulu DHM
        case "/": // Local DHM
          if (digits.length != 3 || tmp.isNotEmpty) throw MalformedException();
          DateTime dateTime = c == "z" ? DateTime.timestamp() : DateTime.now();
          return dateTime.copyWith(
              day: digits[0],
              hour: digits[1],
              minute: digits[2],
              second: 0,
              millisecond: 0,
              microsecond: 0);
        case "h": // Zulu HMS
          if (digits.length != 3 || tmp.isNotEmpty) throw MalformedException();
          return DateTime.timestamp().copyWith(
              hour: digits[0],
              minute: digits[1],
              second: digits[2],
              millisecond: 0,
              microsecond: 0);
        default:
          tmp += c;
          // All formats encode data in two digit chunks
          if (tmp.length == 2) {
            digits.add(int.parse(tmp));
            tmp = "";
          }
          break;
      }
    }
    // MDHM format
    return DateTime.timestamp().copyWith(
        month: digits[0],
        day: digits[1],
        hour: digits[2],
        minute: digits[3],
        second: 0,
        millisecond: 0,
        microsecond: 0);
  }

  double _parseLatitude(ConsumableString buffer) {
    double degrees = double.parse(buffer.take(2).join());
    double minutes =
        double.parse(buffer.take(5).map((c) => c == " " ? "0" : c).join());
    if (!buffer.moveNext() || !["N", "S"].contains(buffer.current)) {
      throw MalformedException();
    }
    int sign = buffer.current == "S" ? -1 : 1;
    return sign * (degrees + (minutes / 60));
  }

  double _parseLongitude(ConsumableString buffer, int ambiguity) {
    double degrees = double.parse(buffer.take(3).join());
    // If we need to truncate all decimals, add +1 to also skip the '.'
    final effectiveAmbiguity = ambiguity + (ambiguity >= 2 ? 1 : 0);
    String minutesStr = buffer.take(5 - effectiveAmbiguity).join();
    if (minutesStr == "") minutesStr = "0";
    double minutes = double.parse(minutesStr);
    for (var i = 0; i < effectiveAmbiguity; i++) {
      buffer.moveNext();
    }
    if (!buffer.moveNext() || !["E", "W"].contains(buffer.current)) {
      throw MalformedException();
    }
    int sign = buffer.current == "W" ? -1 : 1;
    return sign * (degrees + (minutes / 60));
  }

  // TODO extension & df parsing here

  int? _parseAltitudeFromComment(String comment) {
    final match = RegExp(r'/A=([0-9]{6})').firstMatch(comment)?.group(1);
    return match == null ? null : int.parse(match);
  }

  AprsPosition _parsePosition(
      String dti, Tnc2Header header, ConsumableString buffer) {
    DateTime? timestamp;
    if (dti == "/" || dti == "@") {
      timestamp = _parseTimestamp(buffer);
    }
    // Ambiguity is determined by counting the number of spaces in the latitude (next 7 bytes)
    // APRS spec page 24 (position ambiguity) http://www.aprs.org/doc/APRS101.PDF
    buffer.mark();
    final ambiguity = buffer.take(7).where((c) => c == " ").length;
    buffer.rollback();
    final latitude = _parseLatitude(buffer);
    if (!buffer.moveNext()) throw MalformedException();
    final tableChr = buffer.current;
    final longitude = _parseLongitude(buffer, ambiguity);
    if (!buffer.moveNext()) throw MalformedException();
    final symbolChr = buffer.current;
    // TODO Parse extensions
    // TODO Parse DF
    // TODO if has extensions, comment max len = 36; if has DF, comment max len = 28
    const commentMaxLen = 43;
    var comment = buffer.take(commentMaxLen).join();
    if (buffer.isNotEmpty) throw MalformedException();
    final altitude = _parseAltitudeFromComment(comment);
    final isNullPosition = latitude == 0 && longitude == 0;
    return AprsPosition(
      header: header,
      latitude: latitude,
      longitude: longitude,
      ambiguity: ambiguity,
      tableChr: isNullPosition ? "\\" : tableChr,
      symbolChr: isNullPosition ? "." : symbolChr,
      comment: comment,
      altitude: altitude,
      timestamp: timestamp,
    );
  }

  void _onPacket(String packet) {
    final buffer = ConsumableString(packet);
    final header = _parseTnc2Header(buffer);
    final dti = _parseDti(buffer);
    switch (dti) {
      case "!":
      case "=":
      case "/":
      case "@":
        final aprsPacket = _parsePosition(dti, header, buffer);
        _addPosition(aprsPacket);
        break;
      default:
        print("Unsupported DTI (${buffer.current}), skipping");
        break;
    }
  }
}

@riverpod
AprsService aprsService(AprsServiceRef ref) {
  final service = AprsService(
    ref.watch(deviceServiceProvider),
    ref.watch(tncServiceProvider),
  );
  ref.onDispose(service.dispose);
  return service..start();
}
