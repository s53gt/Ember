#pragma once
#include <stdint.h>

void ember_ble_notify_custom(uint16_t chr_val_handle, void* buf, uint16_t len);
void ember_ble_notify(uint16_t chr_val_handle);
