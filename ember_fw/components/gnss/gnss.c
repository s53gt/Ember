#include "gnss.h"

#include "sdkconfig.h"

#if CONFIG_GNSS_ENABLE
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "driver/uart.h"
#include "esp_check.h"
#include "esp_err.h"
#include "esp_log.h"
#include "freertos/FreeRTOS.h"
#include "freertos/idf_additions.h"
#include "freertos/projdefs.h"
#include "gpgga.h"
#include "gprmc.h"
#include "nmea.h"
#include "portmacro.h"
#include "soc/gpio_num.h"

#define NMEA_DEBUG 0

#define GNSS_UART_NUM UART_NUM_2

static const char* TAG = "gnss";

static QueueHandle_t uart_queue = NULL;
static const int uart_buffer_size = (1024 * 2);
// TODO We may want to make reader_task access atomic
static volatile TaskHandle_t reader_task = NULL;
static volatile bool reader_running = false;
static volatile SemaphoreHandle_t fix_lock = NULL;
static volatile gnss_fix_t fix = {0};

esp_err_t gnss_init(void) {
  uart_config_t uart_config = {
      .baud_rate = CONFIG_GNSS_BAUD_RATE,
      .data_bits = UART_DATA_8_BITS,
      .parity = UART_PARITY_DISABLE,
      .stop_bits = UART_STOP_BITS_1,
      .flow_ctrl = UART_HW_FLOWCTRL_DISABLE,
      .rx_flow_ctrl_thresh = 122,
  };
  ESP_RETURN_ON_ERROR(uart_param_config(GNSS_UART_NUM, &uart_config), TAG, "Failed to configure UART");
  ESP_RETURN_ON_ERROR(uart_set_pin(GNSS_UART_NUM, CONFIG_GNSS_TX, CONFIG_GNSS_RX, GPIO_NUM_NC, GPIO_NUM_NC), TAG,
                      "Failed to set UART pins");
  ESP_RETURN_ON_ERROR(uart_driver_install(GNSS_UART_NUM, uart_buffer_size, uart_buffer_size, 10, &uart_queue, 0), TAG,
                      "Failed to install UART driver");
  fix_lock = xSemaphoreCreateMutex();
  ESP_RETURN_ON_ERROR(gnss_start(), TAG, "Failed to start GNSS");
  return ESP_OK;
}

static void gnss_parse_line(uint8_t* line, uint8_t len) {
  nmea_s* data = nmea_parse((char*)line, len, true);
  if (data == NULL) {
    ESP_LOGD(TAG, "Failed to parse NMEA sentence: %.*s", len, line);
    goto end;
  }
  if (data->errors != 0) {
    ESP_LOGW(TAG, "NMEA parser result contains %d errors", data->errors);
  }
  switch (data->type) {
    case NMEA_GPGGA: {
      nmea_gpgga_s* gga = (nmea_gpgga_s*)data;
      if (xSemaphoreTake(fix_lock, 10 / portTICK_PERIOD_MS) == pdTRUE) {
        fix.fix = gga->position_fix;
        fix.n_satellites = gga->n_satellites;
        fix.latitude.degrees = gga->latitude.degrees;
        fix.latitude.minutes = gga->latitude.minutes;
        fix.latitude.cardinal = gga->latitude.cardinal;
        fix.longitude.degrees = gga->longitude.degrees;
        fix.longitude.minutes = gga->longitude.minutes;
        fix.longitude.cardinal = gga->longitude.cardinal;
        fix.altitude = gga->altitude;
        fix.altitude_unit = gga->altitude_unit;
        xSemaphoreGive(fix_lock);
      }
      ESP_LOGD(TAG, "[GGA] Fix: %d\tLat: %lf° %c\tLon: %lf° %c\tAlt: %lf %c\tSats: %d", gga->position_fix,
               gga->latitude.degrees + (gga->latitude.minutes / 60), gga->latitude.cardinal,
               gga->longitude.degrees + (gga->longitude.minutes / 60), gga->longitude.cardinal, gga->altitude,
               gga->altitude_unit, gga->n_satellites);
      break;
    }
    case NMEA_GPRMC: {
      nmea_gprmc_s* rmc = (nmea_gprmc_s*)data;
      if (xSemaphoreTake(fix_lock, 10 / portTICK_PERIOD_MS) == pdTRUE) {  // TODO Sync GPRMC & GPGGA processing
        fix.time = rmc->date_time;
        xSemaphoreGive(fix_lock);
      }
      break;
    }
    default:
      // nop
      break;
  }
end:;
  nmea_free(data);
}

static void gnss_uart_event_task(void* arg) {
  uart_event_t event;
  uint8_t* buff = malloc(1024);
  uint8_t* line = malloc(NMEA_MAX_LENGTH);
  memset(line, 0, NMEA_MAX_LENGTH);
  uint8_t line_idx = 0;
  while (reader_running) {
    if (xQueueReceive(uart_queue, &event, portMAX_DELAY)) {
      switch (event.type) {
        case UART_DATA: {
          int ret = uart_read_bytes(GNSS_UART_NUM, buff, event.size, portMAX_DELAY);
          if (ret < 0) {
            ESP_LOGE(TAG, "Failed to read UART bytes");
            break;
          }
          for (int i = 0; i < event.size; i++) {
            if (line_idx == NMEA_MAX_LENGTH - 1) {
              ESP_LOGW(TAG, "GNSS line buffer overflow");
              line_idx = 0;
              memset(line, 0, NMEA_MAX_LENGTH);
            }
            line[line_idx++] = buff[i];
            if (buff[i] == '\n') {
              if (line_idx > 0) {
                gnss_parse_line(line, line_idx);
#if NMEA_DEBUG
                printf("Line: %s\n", line);
#endif
                line_idx = 0;
                memset(line, 0, NMEA_MAX_LENGTH);
              }
            }
          }
          break;
        }
        case UART_FIFO_OVF: {
          ESP_LOGI(TAG, "UART_FIFO_OVF");
          esp_err_t err = uart_flush_input(GNSS_UART_NUM);
          if (err != ESP_OK) ESP_LOGW(TAG, "Failed to flush UART input buffer");
          xQueueReset(uart_queue);
          break;
        }
        case UART_BUFFER_FULL:
          ESP_LOGI(TAG, "UART_BUFFER_FULL");
          break;
        case UART_BREAK:
          ESP_LOGI(TAG, "UART_BREAK");
          break;
        case UART_PARITY_ERR:
          ESP_LOGI(TAG, "UARTR_PARITY_ERR");
          break;
        case UART_FRAME_ERR:
          ESP_LOGI(TAG, "UART_FRAME_ERR");
          break;
        case UART_PATTERN_DET:
          ESP_LOGI(TAG, "PART_PATTERN_DET");
          break;
        default:
          ESP_LOGW(TAG, "Unknown UART event, type=%d", event.type);
          break;
      }
    }
  }
  free(line);
  free(buff);
  reader_task = NULL;
  vTaskDelete(NULL);
}

esp_err_t gnss_start(void) {
  if (reader_running) return ESP_OK;
  if (!reader_running && reader_task) gnss_stop();
  if (reader_task) {
    ESP_LOGW(TAG, "GNSS already started");
    return ESP_OK;
  }
  reader_running = true;
  BaseType_t err = xTaskCreate(gnss_uart_event_task, "gnss-read", 4096, NULL, 12, (TaskHandle_t*)&reader_task);
  if (err != pdPASS) {
    reader_running = false;
    ESP_LOGE(TAG, "Failed to start GNSS reder task, err=%d", err);
    return ESP_FAIL;
  }
  return ESP_OK;
}

void gnss_stop(void) {
  if (!reader_task) {
    ESP_LOGW(TAG, "GNSS not running");
    return;
  }
  reader_running = false;
  uart_event_t event = {
    .type = UART_BREAK,
  };
  xQueueSendToFront(uart_queue, &event, 0); // Wake up the reader thread
  eTaskState state = eRunning;
  while (reader_task && state != eDeleted && state != eInvalid && state != eReady) {
    state = eTaskGetState(reader_task);
    portYIELD();
  }
}

esp_err_t gnss_get_fix(gnss_fix_t* out, TickType_t block_time) {
  if (xSemaphoreTake(fix_lock, block_time) == pdTRUE) {
    memcpy(out, (void*)&fix, sizeof(gnss_fix_t));
    xSemaphoreGive(fix_lock);
    return ESP_OK;
  }
  return ESP_FAIL;
}
#else
esp_err_t gnss_init(void) { return ESP_OK; }
esp_err_t gnss_start(void) { return ESP_OK; }
void gnss_stop(void) {}
esp_err_t gnss_get_fix(gnss_fix_t* out, TickType_t block_time) { return ESP_OK; }
#endif
