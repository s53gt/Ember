import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'tracker_connect_button_controller.g.dart';

@riverpod
class TrackerConnectButtonController extends _$TrackerConnectButtonController {
  // Literally just a simple wrapper for any Future<void>, so we don't need to nest stuff inside of FutureBuilder()
  // FIXME There's probably a cleaner solution, but it's 1AM
  Future<void> resolveFuture(Future<void> future) async {
    state = const AsyncLoading();
    state = await AsyncValue.guard(() => future);
  }

  @override
  Future<void> build() async {}
}
