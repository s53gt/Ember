// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'aprs_position.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$AprsPosition {
  Tnc2Header get header => throw _privateConstructorUsedError;
  double get latitude => throw _privateConstructorUsedError;
  double get longitude => throw _privateConstructorUsedError;
  int get ambiguity => throw _privateConstructorUsedError;
  String get tableChr => throw _privateConstructorUsedError;
  String get symbolChr => throw _privateConstructorUsedError;
  String get comment => throw _privateConstructorUsedError;
  int? get altitude => throw _privateConstructorUsedError;
  DateTime? get timestamp => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $AprsPositionCopyWith<AprsPosition> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AprsPositionCopyWith<$Res> {
  factory $AprsPositionCopyWith(
          AprsPosition value, $Res Function(AprsPosition) then) =
      _$AprsPositionCopyWithImpl<$Res, AprsPosition>;
  @useResult
  $Res call(
      {Tnc2Header header,
      double latitude,
      double longitude,
      int ambiguity,
      String tableChr,
      String symbolChr,
      String comment,
      int? altitude,
      DateTime? timestamp});

  $Tnc2HeaderCopyWith<$Res> get header;
}

/// @nodoc
class _$AprsPositionCopyWithImpl<$Res, $Val extends AprsPosition>
    implements $AprsPositionCopyWith<$Res> {
  _$AprsPositionCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? header = null,
    Object? latitude = null,
    Object? longitude = null,
    Object? ambiguity = null,
    Object? tableChr = null,
    Object? symbolChr = null,
    Object? comment = null,
    Object? altitude = freezed,
    Object? timestamp = freezed,
  }) {
    return _then(_value.copyWith(
      header: null == header
          ? _value.header
          : header // ignore: cast_nullable_to_non_nullable
              as Tnc2Header,
      latitude: null == latitude
          ? _value.latitude
          : latitude // ignore: cast_nullable_to_non_nullable
              as double,
      longitude: null == longitude
          ? _value.longitude
          : longitude // ignore: cast_nullable_to_non_nullable
              as double,
      ambiguity: null == ambiguity
          ? _value.ambiguity
          : ambiguity // ignore: cast_nullable_to_non_nullable
              as int,
      tableChr: null == tableChr
          ? _value.tableChr
          : tableChr // ignore: cast_nullable_to_non_nullable
              as String,
      symbolChr: null == symbolChr
          ? _value.symbolChr
          : symbolChr // ignore: cast_nullable_to_non_nullable
              as String,
      comment: null == comment
          ? _value.comment
          : comment // ignore: cast_nullable_to_non_nullable
              as String,
      altitude: freezed == altitude
          ? _value.altitude
          : altitude // ignore: cast_nullable_to_non_nullable
              as int?,
      timestamp: freezed == timestamp
          ? _value.timestamp
          : timestamp // ignore: cast_nullable_to_non_nullable
              as DateTime?,
    ) as $Val);
  }

  @override
  @pragma('vm:prefer-inline')
  $Tnc2HeaderCopyWith<$Res> get header {
    return $Tnc2HeaderCopyWith<$Res>(_value.header, (value) {
      return _then(_value.copyWith(header: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$AprsPositionImplCopyWith<$Res>
    implements $AprsPositionCopyWith<$Res> {
  factory _$$AprsPositionImplCopyWith(
          _$AprsPositionImpl value, $Res Function(_$AprsPositionImpl) then) =
      __$$AprsPositionImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {Tnc2Header header,
      double latitude,
      double longitude,
      int ambiguity,
      String tableChr,
      String symbolChr,
      String comment,
      int? altitude,
      DateTime? timestamp});

  @override
  $Tnc2HeaderCopyWith<$Res> get header;
}

/// @nodoc
class __$$AprsPositionImplCopyWithImpl<$Res>
    extends _$AprsPositionCopyWithImpl<$Res, _$AprsPositionImpl>
    implements _$$AprsPositionImplCopyWith<$Res> {
  __$$AprsPositionImplCopyWithImpl(
      _$AprsPositionImpl _value, $Res Function(_$AprsPositionImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? header = null,
    Object? latitude = null,
    Object? longitude = null,
    Object? ambiguity = null,
    Object? tableChr = null,
    Object? symbolChr = null,
    Object? comment = null,
    Object? altitude = freezed,
    Object? timestamp = freezed,
  }) {
    return _then(_$AprsPositionImpl(
      header: null == header
          ? _value.header
          : header // ignore: cast_nullable_to_non_nullable
              as Tnc2Header,
      latitude: null == latitude
          ? _value.latitude
          : latitude // ignore: cast_nullable_to_non_nullable
              as double,
      longitude: null == longitude
          ? _value.longitude
          : longitude // ignore: cast_nullable_to_non_nullable
              as double,
      ambiguity: null == ambiguity
          ? _value.ambiguity
          : ambiguity // ignore: cast_nullable_to_non_nullable
              as int,
      tableChr: null == tableChr
          ? _value.tableChr
          : tableChr // ignore: cast_nullable_to_non_nullable
              as String,
      symbolChr: null == symbolChr
          ? _value.symbolChr
          : symbolChr // ignore: cast_nullable_to_non_nullable
              as String,
      comment: null == comment
          ? _value.comment
          : comment // ignore: cast_nullable_to_non_nullable
              as String,
      altitude: freezed == altitude
          ? _value.altitude
          : altitude // ignore: cast_nullable_to_non_nullable
              as int?,
      timestamp: freezed == timestamp
          ? _value.timestamp
          : timestamp // ignore: cast_nullable_to_non_nullable
              as DateTime?,
    ));
  }
}

/// @nodoc

class _$AprsPositionImpl implements _AprsPosition {
  const _$AprsPositionImpl(
      {required this.header,
      required this.latitude,
      required this.longitude,
      required this.ambiguity,
      required this.tableChr,
      required this.symbolChr,
      required this.comment,
      this.altitude,
      this.timestamp});

  @override
  final Tnc2Header header;
  @override
  final double latitude;
  @override
  final double longitude;
  @override
  final int ambiguity;
  @override
  final String tableChr;
  @override
  final String symbolChr;
  @override
  final String comment;
  @override
  final int? altitude;
  @override
  final DateTime? timestamp;

  @override
  String toString() {
    return 'AprsPosition(header: $header, latitude: $latitude, longitude: $longitude, ambiguity: $ambiguity, tableChr: $tableChr, symbolChr: $symbolChr, comment: $comment, altitude: $altitude, timestamp: $timestamp)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$AprsPositionImpl &&
            (identical(other.header, header) || other.header == header) &&
            (identical(other.latitude, latitude) ||
                other.latitude == latitude) &&
            (identical(other.longitude, longitude) ||
                other.longitude == longitude) &&
            (identical(other.ambiguity, ambiguity) ||
                other.ambiguity == ambiguity) &&
            (identical(other.tableChr, tableChr) ||
                other.tableChr == tableChr) &&
            (identical(other.symbolChr, symbolChr) ||
                other.symbolChr == symbolChr) &&
            (identical(other.comment, comment) || other.comment == comment) &&
            (identical(other.altitude, altitude) ||
                other.altitude == altitude) &&
            (identical(other.timestamp, timestamp) ||
                other.timestamp == timestamp));
  }

  @override
  int get hashCode => Object.hash(runtimeType, header, latitude, longitude,
      ambiguity, tableChr, symbolChr, comment, altitude, timestamp);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$AprsPositionImplCopyWith<_$AprsPositionImpl> get copyWith =>
      __$$AprsPositionImplCopyWithImpl<_$AprsPositionImpl>(this, _$identity);
}

abstract class _AprsPosition implements AprsPosition {
  const factory _AprsPosition(
      {required final Tnc2Header header,
      required final double latitude,
      required final double longitude,
      required final int ambiguity,
      required final String tableChr,
      required final String symbolChr,
      required final String comment,
      final int? altitude,
      final DateTime? timestamp}) = _$AprsPositionImpl;

  @override
  Tnc2Header get header;
  @override
  double get latitude;
  @override
  double get longitude;
  @override
  int get ambiguity;
  @override
  String get tableChr;
  @override
  String get symbolChr;
  @override
  String get comment;
  @override
  int? get altitude;
  @override
  DateTime? get timestamp;
  @override
  @JsonKey(ignore: true)
  _$$AprsPositionImplCopyWith<_$AprsPositionImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
