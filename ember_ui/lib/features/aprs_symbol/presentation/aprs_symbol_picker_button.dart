import 'package:ember_ui/features/aprs_symbol/application/current_aprs_symbol.dart';
import 'package:ember_ui/features/aprs_symbol/presentation/aprs_symbol.dart';
import 'package:ember_ui/features/aprs_symbol/presentation/aprs_symbol_dialog.dart';
import 'package:ember_ui/features/device_control/application/ham_sta_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class AprsSymbolPickerButton extends ConsumerStatefulWidget {
  const AprsSymbolPickerButton({super.key});

  @override
  ConsumerState<ConsumerStatefulWidget> createState() =>
      _AprsSymbolPickerButtonState();
}

class _AprsSymbolPickerButtonState
    extends ConsumerState<AprsSymbolPickerButton> {
  @override
  Widget build(BuildContext context) {
    final localizations = AppLocalizations.of(context)!;
    final symbol = ref.watch(currentAprsSymbolProvider);
    final symbolNotif = ref.read(currentAprsSymbolProvider.notifier);
    final hamStaService = ref.watch(hamStaServiceProvider);
    return IconButton.outlined(
      onPressed: symbol.isLoading || symbol.hasError
          ? null
          : () => showAprsSymbolDialog(context).then((symbol) async {
                if (symbol == null) return;
                await hamStaService.setSymbol(symbol);
                symbolNotif.refresh();
              }),
      style: ButtonStyle(
        fixedSize: const WidgetStatePropertyAll(Size.infinite),
        shape: WidgetStatePropertyAll(
          RoundedRectangleBorder(
            side: BorderSide(
              color: Theme.of(context).colorScheme.primary,
            ),
            borderRadius: BorderRadius.circular(5.0),
          ),
        ),
      ),
      icon: symbol.isLoading
          ? const CircularProgressIndicator()
          : AprsSymbol(
              tableChr: symbol.hasError ? "\\" : symbol.requireValue[0],
              symbolChr: symbol.hasError ? "." : symbol.requireValue[1]),
      tooltip: localizations.aprsSymbol,
      color: Theme.of(context).colorScheme.primary,
      padding: const EdgeInsets.all(8),
    );
  }
}
