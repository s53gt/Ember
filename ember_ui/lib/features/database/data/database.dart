import 'package:drift/drift.dart';
import 'package:ember_ui/features/database/data/connection/connection.dart'
    as impl;
import 'package:ember_ui/features/tracker/data/position_report.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
part 'database.g.dart';

@DriftDatabase(tables: [PositionReport])
class Database extends _$Database {
  Database() : super(impl.connect());

  @override
  int get schemaVersion => 1;
}

@riverpod
Database database(DatabaseRef ref) {
  return Database();
}
