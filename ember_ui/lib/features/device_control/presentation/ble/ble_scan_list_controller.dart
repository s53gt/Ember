import 'dart:async';

import 'package:ember_ui/features/device_control/application/device_service.dart';
import 'package:ember_ui/features/device_control/domain/device.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'ble_scan_list_controller.g.dart';

@riverpod
class BleScanListController extends _$BleScanListController {
  final Map<String, Device> _scanResults = {};
  Future<List<Device>>? _discoveryFuture;
  StreamSubscription<Device>? _subscription;

  BleScanListController();

  void refresh() {
    _discoveryFuture = null;
    ref.invalidateSelf();
  }

  void stopDiscovery() {
    final deviceService = ref.read(deviceServiceProvider);
    deviceService.stopDeviceDiscovery(LinkType.ble);
  }

  @override
  List<Device> build() {
    final deviceService = ref.watch(deviceServiceProvider);
    if (_discoveryFuture == null) {
      _scanResults.clear();
      _discoveryFuture = deviceService.discoverDevices(LinkType.ble);
      _subscription =
          deviceService.discoveredDevices(LinkType.ble).listen((device) {
        _scanResults[device.id] = device;
        ref.invalidateSelf();
      });
      ref.onDispose(() => _subscription?.cancel());
    }
    final list = _scanResults.values.toList();
    list.sort(
      (a, b) =>
          (a.linkQuality?.index ?? 0).compareTo(b.linkQuality?.index ?? 0),
    );
    return list;
  }
}
