#!/bin/bash
FILES=$(ls ../aprs-symbols/png/aprs-symbols-* | grep -E "aprs-symbols-[0-9]{2}-[0-9]")
while IFS= read -r line; do
  RATIO=$(echo $line | awk -F@ '{split($2, a, "x."); print a[1]}')
  DIR="assets/aprs-symbols"
  if [ ! -z "$RATIO" ]; then
    DIR="$DIR/$RATIO"
    if ! grep -q "\\." <<<"$RATIO"; then
      DIR="$DIR.0x"
    else
      DIR="${DIR}x"
    fi
    mkdir -p "$DIR"
  fi
  cp $line $DIR/$(basename "$line" | sed -E "s/@[0-9]+(\.[0-9]+)?x//g")
done <<<"$FILES"
