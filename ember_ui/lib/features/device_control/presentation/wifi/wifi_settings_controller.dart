import 'dart:async';

import 'package:ember_ui/features/device_control/application/device_service.dart';
import 'package:ember_ui/features/device_control/application/wifi_service.dart';
import 'package:ember_ui/features/device_control/domain/wifi_state.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'wifi_settings_controller.g.dart';

@riverpod
class WifiSettingsController extends _$WifiSettingsController {
  @override
  Stream<WifiState> build() {
    final deviceService = ref.watch(deviceServiceProvider);
    final sub =
        deviceService.connectedDeviceStream.listen((_) => ref.invalidateSelf());
    ref.onDispose(() => sub.cancel());
    final wifiService = ref.watch(wifiServiceProvider);
    try {
      return wifiService.subscribeWifiState();
    } catch (e) {
      state = const AsyncData(
        WifiState(
          ssid: "",
          gwAddress: "",
          netmask: "",
          ipAddress: "",
          macAddress: "",
          rssi: -100,
          status: WifiStatus.disconnected,
        ),
      );
      rethrow;
    }
  }
}
