#include "commands/wifi.h"

#include <stdio.h>
#include <string.h>

#include "argtable3/argtable3.h"
#include "esp_console.h"
#include "esp_err.h"
#include "esp_log.h"
#include "esp_wifi_types.h"
#include "repl_private.h"
#include "wifi.h"

static const char* TAG = "wifi";

static int repl_wifi_cmd(int argc, char** argv);

static struct {
  struct arg_lit* enable;
  struct arg_lit* disable;
  struct arg_lit* scan;
  struct arg_lit* connect;
  struct arg_str* ssid;
  struct arg_lit* disconnect;
  struct arg_end* end;
} wifi_args;

static esp_console_cmd_t wifi_cmd = {
    .command = "wifi",
    .help = "Manage the WiFi peripheral",
    .hint = NULL,
    .func = &repl_wifi_cmd,
    .argtable = &wifi_args,
};

static void repl_wifi_init_arg_tables(void) {
  wifi_args.enable = arg_lit0("e", "enable", "Enable WiFi radio");
  wifi_args.disable = arg_lit0("d", "disable", "Disable WiFi radio");
  wifi_args.scan = arg_lit0("s", "scan", "Scan for nearby APs");
  wifi_args.connect = arg_lit0("c", "connect", "Connect to a WiFi AP");
  wifi_args.ssid = arg_str0("i", "ssid", "[ssid]", "SSID to connect to");
  wifi_args.disconnect = arg_lit0("x", "disconnect", "Disconnect from a WiFi AP");
  wifi_args.end = arg_end(2);
}

esp_err_t repl_wifi_register(void) {
  repl_wifi_init_arg_tables();
  return esp_console_cmd_register(&wifi_cmd);
}

static int repl_wifi_cmd(int argc, char** argv) {
  REPL_ARGS_CHECK(wifi_args, argc, argv);
  esp_err_t err;
  if (wifi_args.enable->count > 0) {
    err = wifi_enable();
    if (err != ESP_OK) {
      ESP_LOGE(TAG, "Failed to enable WiFi, err=%d", err);
      return 1;
    }
    err = wifi_connect();
    if (err != ESP_OK) {
      ESP_LOGE(TAG, "Failed to connect to WiFi, err=%d", err);
      return 1;
    }
    return 0;
  }
  if (wifi_args.scan->count > 0) {
    uint16_t len = 32;
    wifi_ap_record_t* ap_info = malloc(sizeof(wifi_ap_record_t) * len);
    assert(ap_info);
    memset(ap_info, 0, sizeof(wifi_ap_record_t));
    err = wifi_scan(&len, ap_info);
    if (err != ESP_OK) {
      free(ap_info);
      ESP_LOGE(TAG, "Failed to scan WiFi, err=%d", err);
      return 1;
    }
    wifi_print_scan_results(len, ap_info);
    free(ap_info);
    return 0;
  }
  if (wifi_args.ssid->count > 0) {
    uint8_t* pass = (uint8_t*)repl_getpass("WiFi Password: ");
    // TODO Allow selecting auth mode threshold
    err = wifi_set_ap_creds((uint8_t*)wifi_args.ssid->sval[0], pass, WIFI_AUTH_WPA2_PSK);
    free(pass);
    if (err != ESP_OK) {
      ESP_LOGE(TAG, "Failed to save WiFi settings, err=%d", err);
      return 1;
    }
    return 0;
  }
  if (wifi_args.connect->count > 0) {
    err = wifi_connect();
    if (err != ESP_OK) {
      ESP_LOGE(TAG, "Failed to establish a WiFi conection, err=%d", err);
      return 1;
    }
    return 0;
  }
  if (wifi_args.disconnect->count > 0) {
    err = wifi_disconnect();
    if (err != ESP_OK) {
      ESP_LOGE(TAG, "Failed to end WiFi connection, err=%d", err);
      return 1;
    }
    return 0;
  }
  if (wifi_args.disable->count > 0) {
    err = wifi_disable();
    if (err != ESP_OK) {
      ESP_LOGE(TAG, "Failed to disable WiFi, err=%d", err);
      return 1;
    }
    return 0;
  }
  ESP_LOGE(TAG, "Invalid arguments");
  return 1;
}
