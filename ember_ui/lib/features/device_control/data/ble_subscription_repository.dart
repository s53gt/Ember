import 'dart:async';
import 'dart:typed_data';

import 'package:ember_ui/features/device_control/application/device_service.dart';
import 'package:ember_ui/features/device_control/domain/device.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:universal_ble/universal_ble.dart';

part 'ble_subscription_repository.g.dart';

typedef OnChrValueChanged = void Function(List<int> value);

class BleSubscriptionRepository {
  final DeviceService _deviceService;
  final Map<String, Set<OnChrValueChanged>> _subscriptions = {};

  BleSubscriptionRepository(this._deviceService) {
    UniversalBle.onValueChanged = _onValueChanged;
  }

  Stream<T> stream<T>(Device device, String serviceUuid, String chrUuid,
      FutureOr<T> Function(List<int> buffer) transform) {
    bool hasListeners = false;
    StreamController<T>? streamController;
    void Function()? cancel;
    final subFuture = listen(device, serviceUuid, chrUuid, (value) async {
      if (hasListeners) {
        streamController?.add(await transform(value));
      }
    });
    subFuture.then((c) => cancel = c).catchError((e) async {
      streamController?.addError(e);
      return () async {};
    });
    streamController = StreamController<T>(
        onListen: () => hasListeners = true,
        onPause: () => hasListeners = false,
        onResume: () => hasListeners = true,
        onCancel: () {
          hasListeners = false;
          if (cancel == null) {
            // In case onCancel() is called before the subFuture completes
            subFuture.then((cancel) => cancel());
          } else {
            cancel!();
          }
          streamController!.close();
          streamController = null;
        });
    return streamController!.stream;
  }

  Future<Future<void> Function()> listen(
    Device device,
    String serviceUuid,
    String chrUuid,
    OnChrValueChanged callback,
  ) async {
    String key = device.id + chrUuid;
    if (!_subscriptions.containsKey(key)) {
      (_subscriptions[key] ??= {}).add(callback);
      await UniversalBle.setNotifiable(
        device.id,
        serviceUuid,
        chrUuid,
        BleInputProperty.notification,
      );
    }
    return () => cancelSubscription(device.id, serviceUuid, chrUuid, callback);
  }

  Future<void> cancelSubscription(String id, String serviceUuid, String chrUuid,
      OnChrValueChanged callback) async {
    String key = id + chrUuid;
    _subscriptions[key]?.remove(callback);
    if ((_subscriptions[key] ?? {}).isEmpty) {
      _subscriptions.remove(key);
      if (_deviceService.connectedDevice?.id == id) {
        await UniversalBle.setNotifiable(
          id,
          serviceUuid,
          chrUuid,
          BleInputProperty.disabled,
        );
      }
    }
  }

  void dispose() {
    UniversalBle.onValueChanged = null;
    _subscriptions.clear();
  }

  void _onValueChanged(String id, String chrUuid, Uint8List value) {
    String key = id + chrUuid;
    _subscriptions[key]?.forEach((cb) => cb(value));
  }
}

@riverpod
BleSubscriptionRepository bleSubscriptionRepository(
    BleSubscriptionRepositoryRef ref) {
  final deviceService = ref.watch(deviceServiceProvider);
  final repo = BleSubscriptionRepository(deviceService);
  ref.onDispose(repo.dispose);
  return repo;
}
