// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'database.dart';

// ignore_for_file: type=lint
class $PositionReportTable extends PositionReport
    with TableInfo<$PositionReportTable, PositionReportData> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $PositionReportTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<int> id = GeneratedColumn<int>(
      'id', aliasedName, false,
      hasAutoIncrement: true,
      type: DriftSqlType.int,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('PRIMARY KEY AUTOINCREMENT'));
  static const VerificationMeta _fromCallMeta =
      const VerificationMeta('fromCall');
  @override
  late final GeneratedColumn<String> fromCall =
      GeneratedColumn<String>('from_call', aliasedName, false,
          additionalChecks: GeneratedColumn.checkTextLength(
            minTextLength: 1,
          ),
          type: DriftSqlType.string,
          requiredDuringInsert: true);
  static const VerificationMeta _toCallMeta = const VerificationMeta('toCall');
  @override
  late final GeneratedColumn<String> toCall =
      GeneratedColumn<String>('to_call', aliasedName, false,
          additionalChecks: GeneratedColumn.checkTextLength(
            minTextLength: 1,
          ),
          type: DriftSqlType.string,
          requiredDuringInsert: true);
  static const VerificationMeta _pathMeta = const VerificationMeta('path');
  @override
  late final GeneratedColumn<String> path = GeneratedColumn<String>(
      'path', aliasedName, false,
      type: DriftSqlType.string,
      requiredDuringInsert: false,
      defaultValue: const Constant(""));
  static const VerificationMeta _latitudeMeta =
      const VerificationMeta('latitude');
  @override
  late final GeneratedColumn<double> latitude = GeneratedColumn<double>(
      'latitude', aliasedName, false,
      type: DriftSqlType.double, requiredDuringInsert: true);
  static const VerificationMeta _longitudeMeta =
      const VerificationMeta('longitude');
  @override
  late final GeneratedColumn<double> longitude = GeneratedColumn<double>(
      'longitude', aliasedName, false,
      type: DriftSqlType.double, requiredDuringInsert: true);
  static const VerificationMeta _ambiguityMeta =
      const VerificationMeta('ambiguity');
  @override
  late final GeneratedColumn<int> ambiguity = GeneratedColumn<int>(
      'ambiguity', aliasedName, false,
      type: DriftSqlType.int, requiredDuringInsert: true);
  static const VerificationMeta _tableChrMeta =
      const VerificationMeta('tableChr');
  @override
  late final GeneratedColumn<String> tableChr = GeneratedColumn<String>(
      'table_chr', aliasedName, false,
      additionalChecks:
          GeneratedColumn.checkTextLength(minTextLength: 1, maxTextLength: 1),
      type: DriftSqlType.string,
      requiredDuringInsert: true);
  static const VerificationMeta _symbolChrMeta =
      const VerificationMeta('symbolChr');
  @override
  late final GeneratedColumn<String> symbolChr = GeneratedColumn<String>(
      'symbol_chr', aliasedName, false,
      additionalChecks:
          GeneratedColumn.checkTextLength(minTextLength: 1, maxTextLength: 1),
      type: DriftSqlType.string,
      requiredDuringInsert: true);
  static const VerificationMeta _commentMeta =
      const VerificationMeta('comment');
  @override
  late final GeneratedColumn<String> comment = GeneratedColumn<String>(
      'comment', aliasedName, false,
      type: DriftSqlType.string,
      requiredDuringInsert: false,
      defaultValue: const Constant(""));
  static const VerificationMeta _altitudeMeta =
      const VerificationMeta('altitude');
  @override
  late final GeneratedColumn<int> altitude = GeneratedColumn<int>(
      'altitude', aliasedName, true,
      type: DriftSqlType.int, requiredDuringInsert: false);
  static const VerificationMeta _timestampMeta =
      const VerificationMeta('timestamp');
  @override
  late final GeneratedColumn<DateTime> timestamp = GeneratedColumn<DateTime>(
      'timestamp', aliasedName, true,
      type: DriftSqlType.dateTime, requiredDuringInsert: false);
  static const VerificationMeta _createdAtMeta =
      const VerificationMeta('createdAt');
  @override
  late final GeneratedColumn<DateTime> createdAt = GeneratedColumn<DateTime>(
      'created_at', aliasedName, false,
      type: DriftSqlType.dateTime,
      requiredDuringInsert: false,
      defaultValue: currentDateAndTime);
  @override
  List<GeneratedColumn> get $columns => [
        id,
        fromCall,
        toCall,
        path,
        latitude,
        longitude,
        ambiguity,
        tableChr,
        symbolChr,
        comment,
        altitude,
        timestamp,
        createdAt
      ];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'position_report';
  @override
  VerificationContext validateIntegrity(Insertable<PositionReportData> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('from_call')) {
      context.handle(_fromCallMeta,
          fromCall.isAcceptableOrUnknown(data['from_call']!, _fromCallMeta));
    } else if (isInserting) {
      context.missing(_fromCallMeta);
    }
    if (data.containsKey('to_call')) {
      context.handle(_toCallMeta,
          toCall.isAcceptableOrUnknown(data['to_call']!, _toCallMeta));
    } else if (isInserting) {
      context.missing(_toCallMeta);
    }
    if (data.containsKey('path')) {
      context.handle(
          _pathMeta, path.isAcceptableOrUnknown(data['path']!, _pathMeta));
    }
    if (data.containsKey('latitude')) {
      context.handle(_latitudeMeta,
          latitude.isAcceptableOrUnknown(data['latitude']!, _latitudeMeta));
    } else if (isInserting) {
      context.missing(_latitudeMeta);
    }
    if (data.containsKey('longitude')) {
      context.handle(_longitudeMeta,
          longitude.isAcceptableOrUnknown(data['longitude']!, _longitudeMeta));
    } else if (isInserting) {
      context.missing(_longitudeMeta);
    }
    if (data.containsKey('ambiguity')) {
      context.handle(_ambiguityMeta,
          ambiguity.isAcceptableOrUnknown(data['ambiguity']!, _ambiguityMeta));
    } else if (isInserting) {
      context.missing(_ambiguityMeta);
    }
    if (data.containsKey('table_chr')) {
      context.handle(_tableChrMeta,
          tableChr.isAcceptableOrUnknown(data['table_chr']!, _tableChrMeta));
    } else if (isInserting) {
      context.missing(_tableChrMeta);
    }
    if (data.containsKey('symbol_chr')) {
      context.handle(_symbolChrMeta,
          symbolChr.isAcceptableOrUnknown(data['symbol_chr']!, _symbolChrMeta));
    } else if (isInserting) {
      context.missing(_symbolChrMeta);
    }
    if (data.containsKey('comment')) {
      context.handle(_commentMeta,
          comment.isAcceptableOrUnknown(data['comment']!, _commentMeta));
    }
    if (data.containsKey('altitude')) {
      context.handle(_altitudeMeta,
          altitude.isAcceptableOrUnknown(data['altitude']!, _altitudeMeta));
    }
    if (data.containsKey('timestamp')) {
      context.handle(_timestampMeta,
          timestamp.isAcceptableOrUnknown(data['timestamp']!, _timestampMeta));
    }
    if (data.containsKey('created_at')) {
      context.handle(_createdAtMeta,
          createdAt.isAcceptableOrUnknown(data['created_at']!, _createdAtMeta));
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  PositionReportData map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return PositionReportData(
      id: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}id'])!,
      fromCall: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}from_call'])!,
      toCall: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}to_call'])!,
      path: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}path'])!,
      latitude: attachedDatabase.typeMapping
          .read(DriftSqlType.double, data['${effectivePrefix}latitude'])!,
      longitude: attachedDatabase.typeMapping
          .read(DriftSqlType.double, data['${effectivePrefix}longitude'])!,
      ambiguity: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}ambiguity'])!,
      tableChr: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}table_chr'])!,
      symbolChr: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}symbol_chr'])!,
      comment: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}comment'])!,
      altitude: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}altitude']),
      timestamp: attachedDatabase.typeMapping
          .read(DriftSqlType.dateTime, data['${effectivePrefix}timestamp']),
      createdAt: attachedDatabase.typeMapping
          .read(DriftSqlType.dateTime, data['${effectivePrefix}created_at'])!,
    );
  }

  @override
  $PositionReportTable createAlias(String alias) {
    return $PositionReportTable(attachedDatabase, alias);
  }
}

class PositionReportData extends DataClass
    implements Insertable<PositionReportData> {
  final int id;
  final String fromCall;
  final String toCall;
  final String path;
  final double latitude;
  final double longitude;
  final int ambiguity;
  final String tableChr;
  final String symbolChr;
  final String comment;
  final int? altitude;
  final DateTime? timestamp;
  final DateTime createdAt;
  const PositionReportData(
      {required this.id,
      required this.fromCall,
      required this.toCall,
      required this.path,
      required this.latitude,
      required this.longitude,
      required this.ambiguity,
      required this.tableChr,
      required this.symbolChr,
      required this.comment,
      this.altitude,
      this.timestamp,
      required this.createdAt});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    map['from_call'] = Variable<String>(fromCall);
    map['to_call'] = Variable<String>(toCall);
    map['path'] = Variable<String>(path);
    map['latitude'] = Variable<double>(latitude);
    map['longitude'] = Variable<double>(longitude);
    map['ambiguity'] = Variable<int>(ambiguity);
    map['table_chr'] = Variable<String>(tableChr);
    map['symbol_chr'] = Variable<String>(symbolChr);
    map['comment'] = Variable<String>(comment);
    if (!nullToAbsent || altitude != null) {
      map['altitude'] = Variable<int>(altitude);
    }
    if (!nullToAbsent || timestamp != null) {
      map['timestamp'] = Variable<DateTime>(timestamp);
    }
    map['created_at'] = Variable<DateTime>(createdAt);
    return map;
  }

  PositionReportCompanion toCompanion(bool nullToAbsent) {
    return PositionReportCompanion(
      id: Value(id),
      fromCall: Value(fromCall),
      toCall: Value(toCall),
      path: Value(path),
      latitude: Value(latitude),
      longitude: Value(longitude),
      ambiguity: Value(ambiguity),
      tableChr: Value(tableChr),
      symbolChr: Value(symbolChr),
      comment: Value(comment),
      altitude: altitude == null && nullToAbsent
          ? const Value.absent()
          : Value(altitude),
      timestamp: timestamp == null && nullToAbsent
          ? const Value.absent()
          : Value(timestamp),
      createdAt: Value(createdAt),
    );
  }

  factory PositionReportData.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return PositionReportData(
      id: serializer.fromJson<int>(json['id']),
      fromCall: serializer.fromJson<String>(json['fromCall']),
      toCall: serializer.fromJson<String>(json['toCall']),
      path: serializer.fromJson<String>(json['path']),
      latitude: serializer.fromJson<double>(json['latitude']),
      longitude: serializer.fromJson<double>(json['longitude']),
      ambiguity: serializer.fromJson<int>(json['ambiguity']),
      tableChr: serializer.fromJson<String>(json['tableChr']),
      symbolChr: serializer.fromJson<String>(json['symbolChr']),
      comment: serializer.fromJson<String>(json['comment']),
      altitude: serializer.fromJson<int?>(json['altitude']),
      timestamp: serializer.fromJson<DateTime?>(json['timestamp']),
      createdAt: serializer.fromJson<DateTime>(json['createdAt']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'fromCall': serializer.toJson<String>(fromCall),
      'toCall': serializer.toJson<String>(toCall),
      'path': serializer.toJson<String>(path),
      'latitude': serializer.toJson<double>(latitude),
      'longitude': serializer.toJson<double>(longitude),
      'ambiguity': serializer.toJson<int>(ambiguity),
      'tableChr': serializer.toJson<String>(tableChr),
      'symbolChr': serializer.toJson<String>(symbolChr),
      'comment': serializer.toJson<String>(comment),
      'altitude': serializer.toJson<int?>(altitude),
      'timestamp': serializer.toJson<DateTime?>(timestamp),
      'createdAt': serializer.toJson<DateTime>(createdAt),
    };
  }

  PositionReportData copyWith(
          {int? id,
          String? fromCall,
          String? toCall,
          String? path,
          double? latitude,
          double? longitude,
          int? ambiguity,
          String? tableChr,
          String? symbolChr,
          String? comment,
          Value<int?> altitude = const Value.absent(),
          Value<DateTime?> timestamp = const Value.absent(),
          DateTime? createdAt}) =>
      PositionReportData(
        id: id ?? this.id,
        fromCall: fromCall ?? this.fromCall,
        toCall: toCall ?? this.toCall,
        path: path ?? this.path,
        latitude: latitude ?? this.latitude,
        longitude: longitude ?? this.longitude,
        ambiguity: ambiguity ?? this.ambiguity,
        tableChr: tableChr ?? this.tableChr,
        symbolChr: symbolChr ?? this.symbolChr,
        comment: comment ?? this.comment,
        altitude: altitude.present ? altitude.value : this.altitude,
        timestamp: timestamp.present ? timestamp.value : this.timestamp,
        createdAt: createdAt ?? this.createdAt,
      );
  @override
  String toString() {
    return (StringBuffer('PositionReportData(')
          ..write('id: $id, ')
          ..write('fromCall: $fromCall, ')
          ..write('toCall: $toCall, ')
          ..write('path: $path, ')
          ..write('latitude: $latitude, ')
          ..write('longitude: $longitude, ')
          ..write('ambiguity: $ambiguity, ')
          ..write('tableChr: $tableChr, ')
          ..write('symbolChr: $symbolChr, ')
          ..write('comment: $comment, ')
          ..write('altitude: $altitude, ')
          ..write('timestamp: $timestamp, ')
          ..write('createdAt: $createdAt')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(
      id,
      fromCall,
      toCall,
      path,
      latitude,
      longitude,
      ambiguity,
      tableChr,
      symbolChr,
      comment,
      altitude,
      timestamp,
      createdAt);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is PositionReportData &&
          other.id == this.id &&
          other.fromCall == this.fromCall &&
          other.toCall == this.toCall &&
          other.path == this.path &&
          other.latitude == this.latitude &&
          other.longitude == this.longitude &&
          other.ambiguity == this.ambiguity &&
          other.tableChr == this.tableChr &&
          other.symbolChr == this.symbolChr &&
          other.comment == this.comment &&
          other.altitude == this.altitude &&
          other.timestamp == this.timestamp &&
          other.createdAt == this.createdAt);
}

class PositionReportCompanion extends UpdateCompanion<PositionReportData> {
  final Value<int> id;
  final Value<String> fromCall;
  final Value<String> toCall;
  final Value<String> path;
  final Value<double> latitude;
  final Value<double> longitude;
  final Value<int> ambiguity;
  final Value<String> tableChr;
  final Value<String> symbolChr;
  final Value<String> comment;
  final Value<int?> altitude;
  final Value<DateTime?> timestamp;
  final Value<DateTime> createdAt;
  const PositionReportCompanion({
    this.id = const Value.absent(),
    this.fromCall = const Value.absent(),
    this.toCall = const Value.absent(),
    this.path = const Value.absent(),
    this.latitude = const Value.absent(),
    this.longitude = const Value.absent(),
    this.ambiguity = const Value.absent(),
    this.tableChr = const Value.absent(),
    this.symbolChr = const Value.absent(),
    this.comment = const Value.absent(),
    this.altitude = const Value.absent(),
    this.timestamp = const Value.absent(),
    this.createdAt = const Value.absent(),
  });
  PositionReportCompanion.insert({
    this.id = const Value.absent(),
    required String fromCall,
    required String toCall,
    this.path = const Value.absent(),
    required double latitude,
    required double longitude,
    required int ambiguity,
    required String tableChr,
    required String symbolChr,
    this.comment = const Value.absent(),
    this.altitude = const Value.absent(),
    this.timestamp = const Value.absent(),
    this.createdAt = const Value.absent(),
  })  : fromCall = Value(fromCall),
        toCall = Value(toCall),
        latitude = Value(latitude),
        longitude = Value(longitude),
        ambiguity = Value(ambiguity),
        tableChr = Value(tableChr),
        symbolChr = Value(symbolChr);
  static Insertable<PositionReportData> custom({
    Expression<int>? id,
    Expression<String>? fromCall,
    Expression<String>? toCall,
    Expression<String>? path,
    Expression<double>? latitude,
    Expression<double>? longitude,
    Expression<int>? ambiguity,
    Expression<String>? tableChr,
    Expression<String>? symbolChr,
    Expression<String>? comment,
    Expression<int>? altitude,
    Expression<DateTime>? timestamp,
    Expression<DateTime>? createdAt,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (fromCall != null) 'from_call': fromCall,
      if (toCall != null) 'to_call': toCall,
      if (path != null) 'path': path,
      if (latitude != null) 'latitude': latitude,
      if (longitude != null) 'longitude': longitude,
      if (ambiguity != null) 'ambiguity': ambiguity,
      if (tableChr != null) 'table_chr': tableChr,
      if (symbolChr != null) 'symbol_chr': symbolChr,
      if (comment != null) 'comment': comment,
      if (altitude != null) 'altitude': altitude,
      if (timestamp != null) 'timestamp': timestamp,
      if (createdAt != null) 'created_at': createdAt,
    });
  }

  PositionReportCompanion copyWith(
      {Value<int>? id,
      Value<String>? fromCall,
      Value<String>? toCall,
      Value<String>? path,
      Value<double>? latitude,
      Value<double>? longitude,
      Value<int>? ambiguity,
      Value<String>? tableChr,
      Value<String>? symbolChr,
      Value<String>? comment,
      Value<int?>? altitude,
      Value<DateTime?>? timestamp,
      Value<DateTime>? createdAt}) {
    return PositionReportCompanion(
      id: id ?? this.id,
      fromCall: fromCall ?? this.fromCall,
      toCall: toCall ?? this.toCall,
      path: path ?? this.path,
      latitude: latitude ?? this.latitude,
      longitude: longitude ?? this.longitude,
      ambiguity: ambiguity ?? this.ambiguity,
      tableChr: tableChr ?? this.tableChr,
      symbolChr: symbolChr ?? this.symbolChr,
      comment: comment ?? this.comment,
      altitude: altitude ?? this.altitude,
      timestamp: timestamp ?? this.timestamp,
      createdAt: createdAt ?? this.createdAt,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (fromCall.present) {
      map['from_call'] = Variable<String>(fromCall.value);
    }
    if (toCall.present) {
      map['to_call'] = Variable<String>(toCall.value);
    }
    if (path.present) {
      map['path'] = Variable<String>(path.value);
    }
    if (latitude.present) {
      map['latitude'] = Variable<double>(latitude.value);
    }
    if (longitude.present) {
      map['longitude'] = Variable<double>(longitude.value);
    }
    if (ambiguity.present) {
      map['ambiguity'] = Variable<int>(ambiguity.value);
    }
    if (tableChr.present) {
      map['table_chr'] = Variable<String>(tableChr.value);
    }
    if (symbolChr.present) {
      map['symbol_chr'] = Variable<String>(symbolChr.value);
    }
    if (comment.present) {
      map['comment'] = Variable<String>(comment.value);
    }
    if (altitude.present) {
      map['altitude'] = Variable<int>(altitude.value);
    }
    if (timestamp.present) {
      map['timestamp'] = Variable<DateTime>(timestamp.value);
    }
    if (createdAt.present) {
      map['created_at'] = Variable<DateTime>(createdAt.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('PositionReportCompanion(')
          ..write('id: $id, ')
          ..write('fromCall: $fromCall, ')
          ..write('toCall: $toCall, ')
          ..write('path: $path, ')
          ..write('latitude: $latitude, ')
          ..write('longitude: $longitude, ')
          ..write('ambiguity: $ambiguity, ')
          ..write('tableChr: $tableChr, ')
          ..write('symbolChr: $symbolChr, ')
          ..write('comment: $comment, ')
          ..write('altitude: $altitude, ')
          ..write('timestamp: $timestamp, ')
          ..write('createdAt: $createdAt')
          ..write(')'))
        .toString();
  }
}

abstract class _$Database extends GeneratedDatabase {
  _$Database(QueryExecutor e) : super(e);
  _$DatabaseManager get managers => _$DatabaseManager(this);
  late final $PositionReportTable positionReport = $PositionReportTable(this);
  late final Index positionReportFromCall = Index('position_report_from_call',
      'CREATE INDEX position_report_from_call ON position_report (from_call)');
  late final Index positionReportToCall = Index('position_report_to_call',
      'CREATE INDEX position_report_to_call ON position_report (to_call)');
  late final Index positionReportTimestamp = Index('position_report_timestamp',
      'CREATE INDEX position_report_timestamp ON position_report (timestamp)');
  late final Index positionReportCreatedAt = Index('position_report_created_at',
      'CREATE INDEX position_report_created_at ON position_report (created_at)');
  @override
  Iterable<TableInfo<Table, Object?>> get allTables =>
      allSchemaEntities.whereType<TableInfo<Table, Object?>>();
  @override
  List<DatabaseSchemaEntity> get allSchemaEntities => [
        positionReport,
        positionReportFromCall,
        positionReportToCall,
        positionReportTimestamp,
        positionReportCreatedAt
      ];
}

typedef $$PositionReportTableInsertCompanionBuilder = PositionReportCompanion
    Function({
  Value<int> id,
  required String fromCall,
  required String toCall,
  Value<String> path,
  required double latitude,
  required double longitude,
  required int ambiguity,
  required String tableChr,
  required String symbolChr,
  Value<String> comment,
  Value<int?> altitude,
  Value<DateTime?> timestamp,
  Value<DateTime> createdAt,
});
typedef $$PositionReportTableUpdateCompanionBuilder = PositionReportCompanion
    Function({
  Value<int> id,
  Value<String> fromCall,
  Value<String> toCall,
  Value<String> path,
  Value<double> latitude,
  Value<double> longitude,
  Value<int> ambiguity,
  Value<String> tableChr,
  Value<String> symbolChr,
  Value<String> comment,
  Value<int?> altitude,
  Value<DateTime?> timestamp,
  Value<DateTime> createdAt,
});

class $$PositionReportTableTableManager extends RootTableManager<
    _$Database,
    $PositionReportTable,
    PositionReportData,
    $$PositionReportTableFilterComposer,
    $$PositionReportTableOrderingComposer,
    $$PositionReportTableProcessedTableManager,
    $$PositionReportTableInsertCompanionBuilder,
    $$PositionReportTableUpdateCompanionBuilder> {
  $$PositionReportTableTableManager(_$Database db, $PositionReportTable table)
      : super(TableManagerState(
          db: db,
          table: table,
          filteringComposer:
              $$PositionReportTableFilterComposer(ComposerState(db, table)),
          orderingComposer:
              $$PositionReportTableOrderingComposer(ComposerState(db, table)),
          getChildManagerBuilder: (p) =>
              $$PositionReportTableProcessedTableManager(p),
          getUpdateCompanionBuilder: ({
            Value<int> id = const Value.absent(),
            Value<String> fromCall = const Value.absent(),
            Value<String> toCall = const Value.absent(),
            Value<String> path = const Value.absent(),
            Value<double> latitude = const Value.absent(),
            Value<double> longitude = const Value.absent(),
            Value<int> ambiguity = const Value.absent(),
            Value<String> tableChr = const Value.absent(),
            Value<String> symbolChr = const Value.absent(),
            Value<String> comment = const Value.absent(),
            Value<int?> altitude = const Value.absent(),
            Value<DateTime?> timestamp = const Value.absent(),
            Value<DateTime> createdAt = const Value.absent(),
          }) =>
              PositionReportCompanion(
            id: id,
            fromCall: fromCall,
            toCall: toCall,
            path: path,
            latitude: latitude,
            longitude: longitude,
            ambiguity: ambiguity,
            tableChr: tableChr,
            symbolChr: symbolChr,
            comment: comment,
            altitude: altitude,
            timestamp: timestamp,
            createdAt: createdAt,
          ),
          getInsertCompanionBuilder: ({
            Value<int> id = const Value.absent(),
            required String fromCall,
            required String toCall,
            Value<String> path = const Value.absent(),
            required double latitude,
            required double longitude,
            required int ambiguity,
            required String tableChr,
            required String symbolChr,
            Value<String> comment = const Value.absent(),
            Value<int?> altitude = const Value.absent(),
            Value<DateTime?> timestamp = const Value.absent(),
            Value<DateTime> createdAt = const Value.absent(),
          }) =>
              PositionReportCompanion.insert(
            id: id,
            fromCall: fromCall,
            toCall: toCall,
            path: path,
            latitude: latitude,
            longitude: longitude,
            ambiguity: ambiguity,
            tableChr: tableChr,
            symbolChr: symbolChr,
            comment: comment,
            altitude: altitude,
            timestamp: timestamp,
            createdAt: createdAt,
          ),
        ));
}

class $$PositionReportTableProcessedTableManager extends ProcessedTableManager<
    _$Database,
    $PositionReportTable,
    PositionReportData,
    $$PositionReportTableFilterComposer,
    $$PositionReportTableOrderingComposer,
    $$PositionReportTableProcessedTableManager,
    $$PositionReportTableInsertCompanionBuilder,
    $$PositionReportTableUpdateCompanionBuilder> {
  $$PositionReportTableProcessedTableManager(super.$state);
}

class $$PositionReportTableFilterComposer
    extends FilterComposer<_$Database, $PositionReportTable> {
  $$PositionReportTableFilterComposer(super.$state);
  ColumnFilters<int> get id => $state.composableBuilder(
      column: $state.table.id,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get fromCall => $state.composableBuilder(
      column: $state.table.fromCall,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get toCall => $state.composableBuilder(
      column: $state.table.toCall,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get path => $state.composableBuilder(
      column: $state.table.path,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<double> get latitude => $state.composableBuilder(
      column: $state.table.latitude,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<double> get longitude => $state.composableBuilder(
      column: $state.table.longitude,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<int> get ambiguity => $state.composableBuilder(
      column: $state.table.ambiguity,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get tableChr => $state.composableBuilder(
      column: $state.table.tableChr,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get symbolChr => $state.composableBuilder(
      column: $state.table.symbolChr,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<String> get comment => $state.composableBuilder(
      column: $state.table.comment,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<int> get altitude => $state.composableBuilder(
      column: $state.table.altitude,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<DateTime> get timestamp => $state.composableBuilder(
      column: $state.table.timestamp,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));

  ColumnFilters<DateTime> get createdAt => $state.composableBuilder(
      column: $state.table.createdAt,
      builder: (column, joinBuilders) =>
          ColumnFilters(column, joinBuilders: joinBuilders));
}

class $$PositionReportTableOrderingComposer
    extends OrderingComposer<_$Database, $PositionReportTable> {
  $$PositionReportTableOrderingComposer(super.$state);
  ColumnOrderings<int> get id => $state.composableBuilder(
      column: $state.table.id,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get fromCall => $state.composableBuilder(
      column: $state.table.fromCall,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get toCall => $state.composableBuilder(
      column: $state.table.toCall,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get path => $state.composableBuilder(
      column: $state.table.path,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<double> get latitude => $state.composableBuilder(
      column: $state.table.latitude,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<double> get longitude => $state.composableBuilder(
      column: $state.table.longitude,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<int> get ambiguity => $state.composableBuilder(
      column: $state.table.ambiguity,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get tableChr => $state.composableBuilder(
      column: $state.table.tableChr,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get symbolChr => $state.composableBuilder(
      column: $state.table.symbolChr,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<String> get comment => $state.composableBuilder(
      column: $state.table.comment,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<int> get altitude => $state.composableBuilder(
      column: $state.table.altitude,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<DateTime> get timestamp => $state.composableBuilder(
      column: $state.table.timestamp,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));

  ColumnOrderings<DateTime> get createdAt => $state.composableBuilder(
      column: $state.table.createdAt,
      builder: (column, joinBuilders) =>
          ColumnOrderings(column, joinBuilders: joinBuilders));
}

class _$DatabaseManager {
  final _$Database _db;
  _$DatabaseManager(this._db);
  $$PositionReportTableTableManager get positionReport =>
      $$PositionReportTableTableManager(_db, _db.positionReport);
}

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$databaseHash() => r'e4fbcd79c505e8d87cac73397a38be8a6f405b5f';

/// See also [database].
@ProviderFor(database)
final databaseProvider = AutoDisposeProvider<Database>.internal(
  database,
  name: r'databaseProvider',
  debugGetCreateSourceHash:
      const bool.fromEnvironment('dart.vm.product') ? null : _$databaseHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef DatabaseRef = AutoDisposeProviderRef<Database>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
