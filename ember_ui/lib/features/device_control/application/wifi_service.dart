import 'dart:async';

import 'package:ember_ui/exceptions/invalid_state_exception.dart';
import 'package:ember_ui/features/device_control/application/device_service.dart';
import 'package:ember_ui/features/device_control/data/ble_wifi_repository.dart';
import 'package:ember_ui/features/device_control/data/wifi_repository.dart';
import 'package:ember_ui/features/device_control/domain/device.dart';
import 'package:ember_ui/features/device_control/domain/wifi_network.dart';
import 'package:ember_ui/features/device_control/domain/wifi_state.dart';
import 'package:mutex/mutex.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'wifi_service.g.dart';

class WifiService {
  final DeviceService _deviceService;
  final Map<LinkType, WifiRepository> _repos;
  final Mutex _scanMutex = Mutex();

  WifiService(this._deviceService, this._repos);

  WifiRepository get _repo {
    if (_deviceService.connectedDevice == null) throw InvalidStateException();
    final repo = _repos[_deviceService.connectedDevice!.linkType];
    if (repo == null) throw UnimplementedError();
    return repo;
  }

  /// Synchronously scans for available WiFi APs
  /// @returns List of discovered networks
  Future<List<WifiNetwork>> scan() async {
    if (_deviceService.connectedDevice == null) throw InvalidStateException();
    return _scanMutex
        .protect(() async => _repo.scan(_deviceService.connectedDevice!));
  }

  Future<void> connect(WifiNetwork network, String? password) async {
    if (_deviceService.connectedDevice == null) throw InvalidStateException();
    await _repo.connect(_deviceService.connectedDevice!, network, password);
  }

  Future<WifiState> getState() async {
    if (_deviceService.connectedDevice == null) throw InvalidStateException();
    return _repo.getState(_deviceService.connectedDevice!);
  }

  Stream<WifiState> subscribeWifiState() {
    if (_deviceService.connectedDevice == null) throw InvalidStateException();
    return _repo.subscribeWifiState(_deviceService.connectedDevice!);
  }
}

@riverpod
WifiService wifiService(WifiServiceRef ref) {
  final deviceService = ref.watch(deviceServiceProvider);
  return WifiService(deviceService, {
    LinkType.ble: ref.watch(bleWifiRepositoryProvider),
  });
}
