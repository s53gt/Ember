import 'dart:async';

import 'package:ember_ui/features/tracker/application/aprs_service.dart';
import 'package:ember_ui/features/tracker/data/position_report_repository.dart';
import 'package:ember_ui/features/tracker/domain/aprs_position.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'tracker_service.g.dart';

class TrackerService {
  final AprsService _aprsService;
  final PositionReportRepository _positionReportRepository;
  StreamSubscription<AprsPosition>? _subscription;

  TrackerService(this._aprsService, this._positionReportRepository);

  void start() {
    _subscription = _aprsService.positionReports.listen(_onPositionReport);
  }

  void dispose() {
    _subscription?.cancel();
  }

  void _onPositionReport(AprsPosition position) {
    _positionReportRepository.append(position);
  }
}

@riverpod
TrackerService trackerService(TrackerServiceRef ref) {
  final trackerService = TrackerService(
    ref.watch(aprsServiceProvider),
    ref.watch(positionReportRepositoryProvider.notifier),
  );
  trackerService.start();
  ref.onDispose(trackerService.dispose);
  return trackerService;
}
