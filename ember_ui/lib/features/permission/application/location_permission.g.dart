// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'location_permission.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$locationPermissionHash() =>
    r'4677d8e5b55da2a4b01f8a9e8a6f1a1ff6fb0fe6';

/// See also [LocationPermission].
@ProviderFor(LocationPermission)
final locationPermissionProvider =
    AutoDisposeNotifierProvider<LocationPermission, PermissionResult>.internal(
  LocationPermission.new,
  name: r'locationPermissionProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$locationPermissionHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$LocationPermission = AutoDisposeNotifier<PermissionResult>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
