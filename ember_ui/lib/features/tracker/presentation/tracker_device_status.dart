import 'package:ember_ui/features/aprs_symbol/application/current_aprs_symbol.dart';
import 'package:ember_ui/features/aprs_symbol/presentation/aprs_symbol.dart';
import 'package:ember_ui/features/device_control/presentation/current_device_stream_builder.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:text_scroll/text_scroll.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class TrackerDeviceStatus extends ConsumerWidget {
  const TrackerDeviceStatus({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final localizations = AppLocalizations.of(context)!;
    final symbol = ref.watch(currentAprsSymbolProvider);
    return Container(
      decoration: BoxDecoration(
        color: Theme.of(context).colorScheme.primaryContainer,
        border: Border.all(color: Theme.of(context).colorScheme.primary),
        borderRadius: BorderRadius.circular(10.0),
      ),
      constraints: const BoxConstraints(minHeight: 55),
      padding: const EdgeInsets.all(10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          CurrentDeviceStreamBuilder(
            builder: (context, snapshot) => Expanded(
              child: TextScroll(
                snapshot.data?.name ?? localizations.noDevice,
                velocity: const Velocity(pixelsPerSecond: Offset(30, 0)),
              ),
            ),
          ),
          if (symbol.isLoading)
            const CircularProgressIndicator()
          else
            AprsSymbol(
              tableChr: symbol.hasError ? "\\" : symbol.requireValue[0],
              symbolChr: symbol.hasError ? "." : symbol.requireValue[1],
            ),
        ],
      ),
    );
  }
}
