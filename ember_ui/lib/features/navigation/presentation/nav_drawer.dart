import 'package:ember_ui/features/localization/presentation/language_change_dialog.dart';
import 'package:ember_ui/features/navigation/presentation/nav_drawer_header.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class NavDrawer extends StatelessWidget {
  const NavDrawer({super.key});

  Widget _buildListTile(BuildContext context,
      {Widget? leading,
      Widget? title,
      void Function()? onTap,
      String? routeName}) {
    var currentRouteName = GoRouter.of(context)
        .routerDelegate
        .currentConfiguration
        .last
        .route
        .name;
    return ListTile(
      leading: leading,
      title: title,
      selected: currentRouteName == routeName,
      onTap: onTap != null || routeName != null
          ? () {
              Navigator.pop(context); // Close the drawer
              if (onTap != null) onTap();
              if (routeName != null) context.pushReplacementNamed(routeName);
            }
          : null,
    );
  }

  @override
  Widget build(BuildContext context) {
    var localizations = AppLocalizations.of(context)!;
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: [
          const NavDrawerHeader(),
          _buildListTile(context,
              leading: const Icon(Icons.radar),
              title: Text(localizations.tracker),
              routeName: 'tracker'),
          _buildListTile(context,
              leading: const Icon(Icons.router),
              title: Text(localizations.igate),
              routeName: 'igate'),
          _buildListTile(context,
              leading: const Icon(Icons.device_hub),
              title: Text(localizations.digipeater),
              routeName: 'digipeater'),
          _buildListTile(context,
              leading: const Icon(Icons.settings_bluetooth),
              title: Text(localizations.deviceSettings),
              routeName: 'device-settings'),
          const Divider(),
          _buildListTile(
            context,
            leading: const Icon(Icons.language),
            title: Text(localizations.language),
            onTap: () => showLanguageChangeDialog(context),
          ),
        ],
      ),
    );
  }
}
