import 'package:ember_ui/features/device_control/application/device_service.dart';
import 'package:ember_ui/features/device_control/domain/device.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class CurrentDeviceStreamBuilder extends ConsumerWidget {
  final Widget Function(BuildContext context, AsyncSnapshot<Device?> snapshot)
      builder;

  const CurrentDeviceStreamBuilder({super.key, required this.builder});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final deviceService = ref.watch(deviceServiceProvider);
    return StreamBuilder(
      stream: deviceService.connectedDeviceStream,
      initialData: deviceService.connectedDevice,
      builder: builder,
    );
  }
}
