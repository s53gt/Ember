import 'dart:typed_data';

import 'package:ember_ui/features/aprs_symbol/data/aprs_symbol_repository.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'aprs_symbol_graphic.g.dart';

@riverpod
Future<Uint8List> aprsSymbolGraphic(
    AprsSymbolGraphicRef ref, String tableChr, String symbolChr) async {
  final aprsSymbolRepository = ref.watch(aprsSymbolRepositoryProvider);
  return aprsSymbolRepository.getSymbol(tableChr, symbolChr);
}
