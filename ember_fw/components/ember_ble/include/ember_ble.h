#pragma once

#include "esp_err.h"

#define EMBER_BLE_MAX_NOTIFY 5

esp_err_t ember_ble_init(void);
