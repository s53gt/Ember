#pragma once

#include "esp_err.h"
#include "gnss.h"

esp_err_t ui_init(void);
void ui_update(gnss_fix_t *fix);
