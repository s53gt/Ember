import 'dart:async';
import 'dart:convert';

import 'package:ember_ui/constants/ble_constants.dart';
import 'package:ember_ui/features/device_control/data/ble_ham_sta_repository.dart';
import 'package:ember_ui/features/device_control/data/device_repository.dart';
import 'package:ember_ui/features/device_control/domain/device.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:mutex/mutex.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:universal_ble/universal_ble.dart';
import 'package:universal_io/io.dart';

part 'ble_device_repository.g.dart';

class BleDeviceRepository extends DeviceRepository {
  final Ref _ref;
  final Map<String, Device> _scanResults = {};
  final Set<String> _connectedDevices = {};
  final Mutex _scanSemaphore = Mutex();
  final Mutex _pairingSemaphore = Mutex();
  late final StreamController<Device> _scanController;
  late final StreamController<bool> _isScanningController;
  late final StreamController<MapEntry<String, DeviceConnectionState>>
      _connectionStateController;
  Timer? _scanTimer;
  bool _isScanning = false;
  bool _scanHasListeners = false;
  bool _isScanningHasListeners = false;
  bool _connectionStateHasListeners = false;
  bool _pairResult = false;

  @override
  Stream<Device> get discoveredDevices => _scanController.stream;
  @override
  Stream<bool> get isScanning => _isScanningController.stream;
  @override
  Stream<MapEntry<String, DeviceConnectionState>> get connectionState =>
      _connectionStateController.stream;

  BleDeviceRepository(this._ref) {
    UniversalBle.onConnectionChanged = _onConnectionChanged;
    UniversalBle.onScanResult = _onScanResult;
    UniversalBle.onPairingStateChange = _onPairingStateChanged;
    _scanController = StreamController.broadcast(
      onListen: _onScanListen,
      onCancel: () => _scanHasListeners = false,
    );
    _isScanningController = StreamController.broadcast(
      onListen: _onIsScanningListen,
      onCancel: () => _isScanningHasListeners = false,
    );
    _connectionStateController = StreamController.broadcast(
      onListen: _onConnectionStateListen,
      onCancel: () => _connectionStateHasListeners = false,
    );
  }

  @override
  Future<List<Device>> discoverDevices() async {
    _scanResults.clear();
    await _getConnectedDevices();
    await _startScan();
    await _scanSemaphore.acquire();
    _scanSemaphore.release();
    return _scanResults.values.toList();
  }

  @override
  Future<void> stopDeviceDiscovery() => _stopScan();

  @override
  Future<void> connect(Device device) async {
    await _stopScan();
    if (device.connected) {
      _onConnectionChanged(device.id, BleConnectionState.connected);
      return;
    }
    await UniversalBle.connect(device.id);
  }

  @override
  Future<void> disconnect(Device device) async {
    if (!device.connected) {
      _onConnectionChanged(device.id, BleConnectionState.disconnected);
      return;
    }
    await UniversalBle.disconnect(device.id);
  }

  @override
  bool isConnected(Device device) {
    return _connectedDevices.contains(device.id);
  }

  @override
  Future<String> getName(Device device) async {
    if (!kIsWeb && !Platform.isLinux) {
      final buff = await UniversalBle.readValue(
        device.id,
        bleGapServiceUid,
        bleGapDeviceNameChrUid,
      );
      return utf8.decode(buff.toList());
    } else {
      // On some platforms, we can't read GAP service for some reason
      // Try to guess the new name instead
      final hamStaRepository = _ref.read(bleHamStaRepositoryProvider);
      return "ember_${await hamStaRepository.getCallsign(device)}";
    }
  }

  void dispose() async {
    UniversalBle.onConnectionChanged = null;
    UniversalBle.onScanResult = null;
    for (var d in _connectedDevices) {
      await AsyncValue.guard(() => UniversalBle.disconnect(d));
    }
    await _stopScan();
    await _scanController.close();
    await _isScanningController.close();
    await _connectionStateController.close();
  }

  void _onScanListen() {
    _scanHasListeners = true;
  }

  void _onIsScanningListen() {
    _isScanningHasListeners = true;
    _isScanningController.add(_isScanning);
  }

  void _onConnectionStateListen() {
    _connectionStateHasListeners = true;
    for (var d in _connectedDevices) {
      _connectionStateController.add(
        MapEntry(d, DeviceConnectionState.connected),
      );
    }
  }

  void _setIsScanning(bool isScanning) {
    _isScanning = isScanning;
    if (_isScanningHasListeners) _isScanningController.add(isScanning);
  }

  LinkQuality? _getConnectionQuality(int? rssi) {
    if (rssi == null) return null;
    if (rssi > -60) return LinkQuality.excellent;
    if (rssi >= -85) return LinkQuality.good;
    return LinkQuality.poor;
  }

  Future<bool> _pairSync(String deviceId) async {
    if (await UniversalBle.isPaired(deviceId)) return true;
    await _pairingSemaphore.acquire();
    try {
      await UniversalBle.pair(deviceId);
      await _pairingSemaphore.acquire();
    } catch (_) {
      _pairingSemaphore.release();
      rethrow;
    }
    return _pairResult;
  }

  void _onPairingStateChanged(String id, bool paired, String? error) async {
    if (error != null && _connectionStateHasListeners) {
      _connectionStateController.addError(error);
    }
    _pairResult = error != null ? false : paired;
    if (_pairingSemaphore.isLocked) _pairingSemaphore.release();
  }

  void _onConnectionChanged(String id, BleConnectionState state) async {
    var domainState = DeviceConnectionState.disconnected;
    if (state == BleConnectionState.connected) {
      domainState = DeviceConnectionState.connected;
      // TODO Verify all required services exist
      await UniversalBle.discoverServices(id);
      if (kIsWeb || Platform.isIOS || Platform.isMacOS) {
        // Reading a protected characteristic will force the OS to pair with the device
        await UniversalBle.readValue(
            id, bleHamStaServiceUid, bleHamStaCallsignChrUid);
      } else {
        await _pairSync(id);
      }
      _connectedDevices.add(id);
    } else {
      _connectedDevices.remove(id);
    }
    if (_connectionStateHasListeners) {
      _connectionStateController.add(MapEntry(id, domainState));
    }
  }

  Device _makeDevice(BleScanResult result, bool connected) {
    return Device(
      id: result.deviceId,
      name: result.name!,
      connected: connected,
      linkQuality: _getConnectionQuality(result.rssi),
      linkType: LinkType.ble,
    );
  }

  void _onScanResult(BleScanResult result, {bool? connected}) {
    // Ignore devices without a name; Ember devices always advertize their name
    if (result.name == null) return;
    _scanResults[result.deviceId] = _makeDevice(result, connected ?? false);
    if (_scanHasListeners) _scanController.add(_scanResults[result.deviceId]!);
  }

  Future<void> _startScan() async {
    if (_isScanning) return;
    await _scanSemaphore.acquire();
    try {
      _setIsScanning(true);
      await UniversalBle.startScan(
          scanFilter: ScanFilter(
        withNamePrefix: ["ember_"],
        withServices: kIsWeb ? bleAllServices : [],
      ));
      _scanTimer = Timer(const Duration(seconds: 5), () => _stopScan());
    } catch (_) {
      _scanSemaphore.release();
      rethrow;
    }
  }

  Future<void> _stopScan() async {
    if (!_isScanning) return;
    _scanTimer?.cancel();
    _scanTimer = null;
    await UniversalBle.stopScan();
    _setIsScanning(false);
    if (_scanSemaphore.isLocked) _scanSemaphore.release();
  }

  Future<void> _getConnectedDevices() async {
    if (kIsWeb) return;
    final results = (await UniversalBle.getConnectedDevices())
        .where((d) => d.name?.startsWith("ember_") == true);
    for (var result in results) {
      _onScanResult(result, connected: true);
    }
  }
}

@riverpod
BleDeviceRepository bleDeviceRepository(BleDeviceRepositoryRef ref) {
  final repo = BleDeviceRepository(ref);
  ref.onDispose(() => repo.dispose());
  return repo;
}
