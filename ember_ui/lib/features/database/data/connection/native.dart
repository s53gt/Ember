import 'package:drift/drift.dart';
import 'package:drift/native.dart';
import 'package:ember_ui/constants/database_constants.dart';
import 'package:sqlite3_flutter_libs/sqlite3_flutter_libs.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:universal_io/io.dart';
import 'package:sqlite3/sqlite3.dart';

LazyDatabase connect() {
  return LazyDatabase(() async {
    final dbDir = await getApplicationDocumentsDirectory();
    final path = join(dbDir.path, "$dbName.db");
    if (Platform.isAndroid) {
      await applyWorkaroundToOpenSqlite3OnOldAndroidVersions();
    }
    final cacheBase = (await getTemporaryDirectory()).path;
    sqlite3.tempDirectory = cacheBase;
    return NativeDatabase.createInBackground(File(path));
  });
}
