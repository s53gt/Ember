#include "commands/settings.h"

#include "argtable3/argtable3.h"
#include "esp_console.h"
#include "esp_err.h"
#include "esp_log.h"
#include "esp_system.h"
#include "repl_private.h"
#include "settings.h"

static const char* TAG = "settings_repl";

static int repl_settings_cmd(int argc, char** argv);

static struct {
  struct arg_lit* erase;
  struct arg_end* end;
} settings_args;

static esp_console_cmd_t settings_cmd = {
    .command = "settings",
    .help = "Manage NVS key-value store",
    .hint = NULL,
    .func = &repl_settings_cmd,
    .argtable = &settings_args,
};

static void repl_settings_init_arg_tables(void) {
  settings_args.erase = arg_lit0("e", "erase", "Erase NVS partition. Ignores all other options");
  settings_args.end = arg_end(2);
}

esp_err_t repl_settings_register(void) {
  repl_settings_init_arg_tables();
  return esp_console_cmd_register(&settings_cmd);
}

static int repl_settings_cmd(int argc, char** argv) {
  REPL_ARGS_CHECK(settings_args, argc, argv);
  if (settings_args.erase->count > 0) {
    ESP_ERROR_CHECK(settings_erase());
    esp_restart();
    return 0;
  }
  ESP_LOGE(TAG, "Invalid arguments");
  return 1;
}
