import 'package:flutter/material.dart';

enum NoticeLevel { info, warn, error, success }

Color _getLevelColor(NoticeLevel level) {
  switch (level) {
    case NoticeLevel.info:
      return Colors.blue;
    case NoticeLevel.warn:
      return Colors.yellow;
    case NoticeLevel.error:
      return Colors.red;
    case NoticeLevel.success:
      return Colors.green;
  }
}

class Notice extends StatelessWidget {
  final String text;
  final NoticeLevel level;

  const Notice({super.key, required this.text, this.level = NoticeLevel.info});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(10),
      decoration: BoxDecoration(
          borderRadius: const BorderRadius.all(Radius.circular(5)),
          border:
              Border.all(color: _getLevelColor(level).withAlpha(200), width: 1),
          color: _getLevelColor(level).withAlpha(128)),
      child: Text(text, textAlign: TextAlign.center),
    );
  }
}
