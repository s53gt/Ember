// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'tnc2_header.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$Tnc2Header {
  String get from => throw _privateConstructorUsedError;
  String get to => throw _privateConstructorUsedError;
  String get path => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $Tnc2HeaderCopyWith<Tnc2Header> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $Tnc2HeaderCopyWith<$Res> {
  factory $Tnc2HeaderCopyWith(
          Tnc2Header value, $Res Function(Tnc2Header) then) =
      _$Tnc2HeaderCopyWithImpl<$Res, Tnc2Header>;
  @useResult
  $Res call({String from, String to, String path});
}

/// @nodoc
class _$Tnc2HeaderCopyWithImpl<$Res, $Val extends Tnc2Header>
    implements $Tnc2HeaderCopyWith<$Res> {
  _$Tnc2HeaderCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? from = null,
    Object? to = null,
    Object? path = null,
  }) {
    return _then(_value.copyWith(
      from: null == from
          ? _value.from
          : from // ignore: cast_nullable_to_non_nullable
              as String,
      to: null == to
          ? _value.to
          : to // ignore: cast_nullable_to_non_nullable
              as String,
      path: null == path
          ? _value.path
          : path // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$Tnc2HeaderImplCopyWith<$Res>
    implements $Tnc2HeaderCopyWith<$Res> {
  factory _$$Tnc2HeaderImplCopyWith(
          _$Tnc2HeaderImpl value, $Res Function(_$Tnc2HeaderImpl) then) =
      __$$Tnc2HeaderImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String from, String to, String path});
}

/// @nodoc
class __$$Tnc2HeaderImplCopyWithImpl<$Res>
    extends _$Tnc2HeaderCopyWithImpl<$Res, _$Tnc2HeaderImpl>
    implements _$$Tnc2HeaderImplCopyWith<$Res> {
  __$$Tnc2HeaderImplCopyWithImpl(
      _$Tnc2HeaderImpl _value, $Res Function(_$Tnc2HeaderImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? from = null,
    Object? to = null,
    Object? path = null,
  }) {
    return _then(_$Tnc2HeaderImpl(
      from: null == from
          ? _value.from
          : from // ignore: cast_nullable_to_non_nullable
              as String,
      to: null == to
          ? _value.to
          : to // ignore: cast_nullable_to_non_nullable
              as String,
      path: null == path
          ? _value.path
          : path // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$Tnc2HeaderImpl implements _Tnc2Header {
  const _$Tnc2HeaderImpl(
      {required this.from, required this.to, required this.path});

  @override
  final String from;
  @override
  final String to;
  @override
  final String path;

  @override
  String toString() {
    return 'Tnc2Header(from: $from, to: $to, path: $path)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$Tnc2HeaderImpl &&
            (identical(other.from, from) || other.from == from) &&
            (identical(other.to, to) || other.to == to) &&
            (identical(other.path, path) || other.path == path));
  }

  @override
  int get hashCode => Object.hash(runtimeType, from, to, path);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$Tnc2HeaderImplCopyWith<_$Tnc2HeaderImpl> get copyWith =>
      __$$Tnc2HeaderImplCopyWithImpl<_$Tnc2HeaderImpl>(this, _$identity);
}

abstract class _Tnc2Header implements Tnc2Header {
  const factory _Tnc2Header(
      {required final String from,
      required final String to,
      required final String path}) = _$Tnc2HeaderImpl;

  @override
  String get from;
  @override
  String get to;
  @override
  String get path;
  @override
  @JsonKey(ignore: true)
  _$$Tnc2HeaderImplCopyWith<_$Tnc2HeaderImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
