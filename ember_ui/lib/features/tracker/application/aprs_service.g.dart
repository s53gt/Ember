// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'aprs_service.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$aprsServiceHash() => r'12ee85e6aa2e8b9714f040d00c6201d95c9890aa';

/// See also [aprsService].
@ProviderFor(aprsService)
final aprsServiceProvider = AutoDisposeProvider<AprsService>.internal(
  aprsService,
  name: r'aprsServiceProvider',
  debugGetCreateSourceHash:
      const bool.fromEnvironment('dart.vm.product') ? null : _$aprsServiceHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef AprsServiceRef = AutoDisposeProviderRef<AprsService>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
