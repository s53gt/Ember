// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ble_subscription_repository.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$bleSubscriptionRepositoryHash() =>
    r'cdf84cdeda12940d03d16a439177f49aa739f240';

/// See also [bleSubscriptionRepository].
@ProviderFor(bleSubscriptionRepository)
final bleSubscriptionRepositoryProvider =
    AutoDisposeProvider<BleSubscriptionRepository>.internal(
  bleSubscriptionRepository,
  name: r'bleSubscriptionRepositoryProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$bleSubscriptionRepositoryHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef BleSubscriptionRepositoryRef
    = AutoDisposeProviderRef<BleSubscriptionRepository>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
