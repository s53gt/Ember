#include "digi.h"

#include "esp_check.h"
#include "tnc2.h"

static const char* TAG = "digi";

static void digi_handle_packet(const tnc2_packet_t* packet) { ESP_LOGI(TAG, "Got packet from %s", packet->fromcall); }

esp_err_t digi_init(void) {
  tnc2_proto_t proto = {
      .name = "DIGI",
      .process_packet = digi_handle_packet,
  };
  esp_err_t err = tnc2_register_proto(&proto);
  ESP_RETURN_ON_ERROR(err, TAG, "Failed to register TNC2 packet handler");
  return ESP_OK;
}
