#include "power.h"

#include "esp_adc/adc_cali.h"
#include "esp_adc/adc_cali_scheme.h"
#include "esp_adc/adc_oneshot.h"
#include "esp_check.h"
#include "esp_err.h"
#include "freertos/FreeRTOS.h"
#include "freertos/idf_additions.h"
#include "freertos/projdefs.h"
#include "hal/adc_types.h"
#include "portmacro.h"
#include "sdkconfig.h"

#define CLAMP(N, MIN, MAX) ((N) > (MAX) ? (MAX) : ((N) < (MIN) ? (MIN) : (N)))

static const char *TAG = "power";

static adc_oneshot_unit_handle_t adc_handle = NULL;
static adc_cali_handle_t adc_cali_handle = NULL;
static SemaphoreHandle_t batt_mutex = NULL;
static adc_unit_t adc_unit;
static adc_channel_t adc_channel;

esp_err_t power_init(void) {
#if CONFIG_POWER_BATT_ENABLE
  esp_err_t err;
  err = adc_oneshot_io_to_channel(CONFIG_POWER_BATT_PIN, &adc_unit, &adc_channel);
  ESP_RETURN_ON_ERROR(err, TAG, "Failed to determine ADC channel for  GPIO%d", CONFIG_POWER_BATT_PIN);
  adc_oneshot_unit_init_cfg_t adc_cfg = {
      .ulp_mode = ADC_ULP_MODE_DISABLE,
      .unit_id = adc_unit,
  };
  adc_cali_line_fitting_config_t cali_cfg = {
      .bitwidth = ADC_BITWIDTH_DEFAULT,
      .atten = ADC_ATTEN_DB_12,
      .unit_id = adc_unit,
  };
  adc_oneshot_chan_cfg_t chan_cfg = {
      .atten = ADC_ATTEN_DB_12,
      .bitwidth = ADC_BITWIDTH_DEFAULT,
  };
  err = adc_oneshot_new_unit(&adc_cfg, &adc_handle);
  ESP_RETURN_ON_ERROR(err, TAG, "Failed to initialize battery ADC");
  err = adc_cali_create_scheme_line_fitting(&cali_cfg, &adc_cali_handle);
  ESP_RETURN_ON_ERROR(err, TAG, "Failed to create battery ADC line fitting scheme");
  err = adc_oneshot_config_channel(adc_handle, adc_channel, &chan_cfg);
  ESP_RETURN_ON_ERROR(err, TAG, "Failed to configure battery ADC channel");
  batt_mutex = xSemaphoreCreateMutex();
  ESP_RETURN_ON_FALSE(batt_mutex != NULL, ESP_ERR_NO_MEM, TAG, "Failed to allocate a battery mutex");
#endif
  return ESP_OK;
}

esp_err_t power_get_batt_volt(int *voltage, TickType_t block_time) {
#if CONFIG_POWER_BATT_ENABLE
  if (xSemaphoreTake(batt_mutex, block_time) != pdPASS) return ESP_ERR_TIMEOUT;
  esp_err_t err;
  int raw;
  err = adc_oneshot_read(adc_handle, adc_channel, &raw);
  ESP_RETURN_ON_ERROR(err, TAG, "Failed to read battery voltage");
  err = adc_cali_raw_to_voltage(adc_cali_handle, raw, voltage);
  ESP_RETURN_ON_ERROR(err, TAG, "Failed to convert battery voltage");
  *voltage *= CONFIG_POWER_BATT_DIV_FACTOR;
  xSemaphoreGive(batt_mutex);
#endif
  return ESP_OK;
}

esp_err_t power_get_batt_percent(uint8_t *percent, TickType_t block_time) {
  int voltage;
  esp_err_t err = power_get_batt_volt(&voltage, block_time);
  ESP_RETURN_ON_ERROR(err, TAG, "Failed to get battery voltage");
  voltage = CLAMP(voltage, CONFIG_POWER_BATT_MIN_MV, CONFIG_POWER_BATT_MAX_MV);
  *percent = (float)voltage / CONFIG_POWER_BATT_MAX_MV * 100;
  // Clamp the percentage, because floats are weird (at 100% we'll sometimes get just a little over 101%)
  *percent = CLAMP(*percent, 0, 100);
  return ESP_OK;
}
