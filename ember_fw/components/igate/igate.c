#include "igate.h"

#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include "aprs_is.h"
#include "esp_check.h"
#include "esp_err.h"
#include "esp_log.h"
#include "freertos/idf_additions.h"
#include "portmacro.h"
#include "settings.h"
#include "tnc2.h"

#define IGATE_3RD_PARTY_DTI '}'
#define IGATE_GENERIC_QUERY_DTI '?'

#define IS_DIGIT(C) ((C) >= '0' && (C) <= '9')
#define IS_ALPHANUM(C) (IS_DIGIT(C) || ((C) >= 'A' && (C) <= 'Z') || ((C) >= 'a' && (C) <= 'z'))

static const char *TAG = "igate";
static const char *nogate_constructs[] = {
    "TCPIP",
    "TCPXX",
    "NOGATE",
    "RFONLY",
};

static char callsign[HAM_STA_CALL_SSID_MAX_LEN + 1] = {0};
static TaskHandle_t beacon_task = NULL;
static SemaphoreHandle_t beacon_semaphore = NULL;

static bool igate_should_gate_from_rf(const tnc2_packet_t *packet);

static void igate_handle_packet(const tnc2_packet_t *packet) {
  ESP_LOGI(TAG, "Got packet from %s, proto=%d", packet->fromcall, packet->proto);
  switch (packet->proto) {
    case TNC2_SRC_PROTO_LORA: {  // These must be gated to APRS-IS
      if (!aprs_is_connected()) break;
      if (!igate_should_gate_from_rf(packet)) {
        ESP_LOGD(TAG, "Received RF packet from %s-%s with path %s. It shouldn't be gated to APRS-IS. Skipping...",
                 packet->fromcall, packet->fromcall_ssid, packet->path);
        break;
      }
      tnc2_packet_t third_party = {0};
      if (packet->data[0] == IGATE_3RD_PARTY_DTI) {
        if (tnc2_parse_packet(packet->proto, (const uint8_t *)packet->data + 1, packet->data_len - 1, &third_party) !=
            ESP_OK) {
          ESP_LOGW(TAG, "Can't unwrap 3rd party packet, from=%s, from_ssid=%s, to=%s, to_ssid=%s, path=%s, proto=%d",
                   packet->fromcall, packet->fromcall_ssid, packet->tocall, packet->tocall_ssid, packet->path,
                   packet->proto);
          ESP_LOG_BUFFER_HEXDUMP(TAG, packet->data, packet->data_len, ESP_LOG_DEBUG);
          break;
        }
        packet = &third_party;
      }
      char from_ssid[HAM_STA_SSID_MAX_LEN + 2] = {0};
      if (IS_ALPHANUM(packet->fromcall_ssid[0])) {
        from_ssid[0] = '-';
        strcpy(from_ssid + 1, packet->fromcall_ssid);
      }
      char to_ssid[HAM_STA_SSID_MAX_LEN + 2] = {0};
      if (IS_ALPHANUM(packet->tocall_ssid[0])) {
        to_ssid[0] = '-';
        strcpy(to_ssid + 1, packet->tocall_ssid);
      }
      bool has_path = IS_ALPHANUM(packet->path[0]);
      int path_suffix_len = has_path + 4 /* qAR, */ + strlen(callsign) + 1;
      char *path_suffix = malloc(path_suffix_len);
      memset(path_suffix, 0, path_suffix_len);
      sprintf(path_suffix, "%sqAR,%s", has_path ? "," : "", callsign);
      esp_err_t err = aprs_is_sendf(100 / portTICK_PERIOD_MS, "%s%s>%s%s,%s%s:%.*s", packet->fromcall, from_ssid,
                                    packet->tocall, to_ssid, packet->path, path_suffix, packet->data_len, packet->data);
      free(path_suffix);
      if (err != ESP_OK) {
        ESP_LOGW(TAG, "Failed to gate packet to APRS-IS, err=%d, err_str=%s", err, esp_err_to_name(err));
        break;
      }
      break;
    }
    case TNC2_SRC_PROTO_APRS_IS: {  // These must be gated to RF
      // TODO
      break;
    }
    default:
      ESP_LOGW(TAG, "Received packet from unknown source, proto=%d", packet->proto);
      break;
  }
}

// Rules taken from https://www.aprs-is.net/IGateDetails.aspx
static bool igate_should_gate_from_rf(const tnc2_packet_t *packet) {
  if (packet->data[0] == IGATE_GENERIC_QUERY_DTI) return false;  // Generic queries shouldn't be gated
  // Packets with TCPIP, TCPXX, NOGATE, or RFONLY in the path or 3rd party header, must NOT be gated
  for (uint8_t i = 0; i < sizeof(nogate_constructs) / sizeof(char *); i++) {
    if (strstr(packet->path, nogate_constructs[i])) return false;
    if (packet->data[0] == IGATE_3RD_PARTY_DTI) {  // 3rd party data
      char *third_party_header_end = strstr(packet->data + 1, ":");
      if (!third_party_header_end) {
        ESP_LOGW(
            TAG, "Incorrectly formatted 3rd party data, from=%s, from_ssid=%s, to=%s, to_ssid=%s, path=%s, proto=%d",
            packet->fromcall, packet->fromcall_ssid, packet->tocall, packet->tocall_ssid, packet->path, packet->proto);
        ESP_LOG_BUFFER_HEXDUMP(TAG, packet->data, packet->data_len, ESP_LOG_DEBUG);
        return false;
      }
      if (strnstr(packet->data + 1, nogate_constructs[i], third_party_header_end - (packet->data + 1))) return false;
    }
  }
  return true;
}

void igate_beacon_task(void *arg) {
  esp_err_t err;
  while (1) {
    err = aprs_is_sendf(100 / portTICK_PERIOD_MS, "%s>%s,TCPIP");
    if (err != ESP_OK) {
      ESP_LOGW(TAG, "Failed to send beacon to APRS-IS");
    }
    xSemaphoreTake(beacon_semaphore, 20 * 60 * 1000 * portTICK_PERIOD_MS);  // TODO Dynamic beacon period
  }
  beacon_task = NULL;
  vTaskDelete(NULL);
}

esp_err_t igate_init(void) {
  size_t len = HAM_STA_CALL_SSID_MAX_LEN + 1;
  esp_err_t ret = settings_get_str_default(SETTINGS_KEY_CALLSIGN, callsign, &len, HAM_STA_CALLSIGN_DEFAULT);
  ESP_RETURN_ON_ERROR(ret, TAG, "Failed to get callsign from settings");
  tnc2_proto_t tnc2_proto = {
      .name = "iGate",
      .process_packet = igate_handle_packet,
  };
  esp_err_t err = tnc2_register_proto(&tnc2_proto);
  ESP_RETURN_ON_ERROR(err, TAG, "Failed to register TNC2 packet handler");
  beacon_semaphore = xSemaphoreCreateBinary();
  BaseType_t res = xTaskCreate(igate_beacon_task, "gate-beacon", 4096, NULL, 12, &beacon_task);
  ESP_RETURN_ON_FALSE(res == pdPASS, ESP_FAIL, TAG, "Failed to start iGate beacon task");
  return ESP_OK;
}
