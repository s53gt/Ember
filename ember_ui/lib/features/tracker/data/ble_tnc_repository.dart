import 'dart:convert';

import 'package:ember_ui/constants/ble_constants.dart';
import 'package:ember_ui/features/device_control/data/ble_subscription_repository.dart';
import 'package:ember_ui/features/device_control/domain/device.dart';
import 'package:ember_ui/features/tracker/data/tnc_repository.dart';
import 'package:ember_ui/utils/newline_delimited_stream_transformer.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'ble_tnc_repository.g.dart';

class BleTncRepository extends TncRepository {
  final BleSubscriptionRepository _bleSubscriptionRepository;

  BleTncRepository(this._bleSubscriptionRepository);

  @override
  Stream<String> getPacketStream(Device device) => _bleSubscriptionRepository
      .stream(
        device,
        bleTncServiceUid,
        bleTncSerialChrUid,
        utf8.decode,
      )
      .transform(NewlineDelimitedStreamTransformer());
}

@riverpod
BleTncRepository bleTncRepository(BleTncRepositoryRef ref) {
  final bleSubscriptionRepository =
      ref.watch(bleSubscriptionRepositoryProvider);
  return BleTncRepository(bleSubscriptionRepository);
}
