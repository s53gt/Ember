import 'package:ember_ui/common_widgets/root_layout.dart';
import 'package:ember_ui/features/device_control/presentation/settings/device_settings_page.dart';
import 'package:ember_ui/features/device_control/presentation/wifi/wifi_scan_page.dart';
import 'package:ember_ui/features/tracker/presentation/tracker_page.dart';
import 'package:ember_ui/features/tracker/presentation/tracker_layout.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

GoRoute _wrapRoute({
  required String path,
  required GoRouterWidgetBuilder builder,
  Widget Function(BuildContext, AppLocalizations)? title,
  String? name,
  bool customLayout = false,
}) {
  return GoRoute(
    path: path,
    name: name,
    pageBuilder: (context, state) => CustomTransitionPage(
      key: state.pageKey,
      transitionDuration: Duration.zero,
      reverseTransitionDuration: Duration.zero,
      transitionsBuilder: (_, __, ___, child) => child,
      child: customLayout
          ? builder(context, state)
          : RootLayout(
              title: title == null
                  ? Text(path)
                  : title(context, AppLocalizations.of(context)!),
              body: builder(context, state),
            ),
    ),
  );
}

final routerConfig = GoRouter(routes: [
  _wrapRoute(
    path: '/tracker',
    name: 'tracker',
    customLayout: true,
    builder: (_, __) => const TrackerLayout(
      body: TrackerPage(),
    ),
  ),
  _wrapRoute(
      path: '/igate',
      name: 'igate',
      title: (_, localizations) => Text(localizations.igate),
      builder: (_, __) => const Text("iGate")),
  _wrapRoute(
      path: '/digipeater',
      name: 'digipeater',
      title: (_, localizations) => Text(localizations.digipeater),
      builder: (_, __) => const Text("Digipeater")),
  _wrapRoute(
      path: '/device-settings',
      name: 'device-settings',
      title: (_, localizations) => Text(localizations.deviceSettings),
      builder: (_, __) => const DeviceSettingsPage()),
  _wrapRoute(
      path: '/device-settings/wifi-scan',
      name: 'wifi-scan',
      title: (_, localizations) => Text(localizations.deviceSettings),
      builder: (_, __) => const WifiScanPage()),
], initialLocation: '/tracker');
