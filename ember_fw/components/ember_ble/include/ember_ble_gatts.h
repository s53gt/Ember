#pragma once

#include "esp_err.h"
#include "host/ble_gatt.h"
esp_err_t ember_ble_gatts_init(void);
void ember_ble_on_gatts_register(struct ble_gatt_register_ctxt *ctx, void *arg);
esp_err_t ember_ble_gatts_set_batt_level(uint8_t level);
