#include "aprs_is.h"

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "cc.h"
#include "esp_app_desc.h"
#include "esp_check.h"
#include "esp_err.h"
#include "esp_event.h"
#include "esp_event_base.h"
#include "esp_log.h"
#include "esp_netif_types.h"
#include "freertos/idf_additions.h"
#include "freertos/projdefs.h"
#include "lwip/dns.h"
#include "lwip/err.h"
#include "lwip/ip_addr.h"
#include "lwip/sockets.h"
#include "nvs.h"
#include "portmacro.h"
#include "settings.h"
#include "settings_default.h"

#define APRS_IS_PORT 14580
#define APRS_IS_HOST "euro.aprs2.net"
#define APRS_IS_MAX_LINE_LEN 512

static const char *TAG = "aprs_is";

static int sockfd = -1;
static SemaphoreHandle_t dns_semaphore = NULL;
static SemaphoreHandle_t socket_tx_mutex = NULL;
static TaskHandle_t aprs_is_task_handle = NULL;
static esp_event_handler_instance_t ip_got_event_handler = NULL;
static esp_event_handler_instance_t ip_lost_event_handler = NULL;
static esp_event_handler_instance_t settings_updated_event_handler = NULL;
static ip_addr_t server_addr = {0};
static volatile bool task_running = false;
static volatile bool received_hello = false;
static volatile bool should_retry = false;
static volatile bool authenticated = false;
static volatile bool verified = false;
static char callsign[HAM_STA_CALL_SSID_MAX_LEN + 1] = {0};
static char passcode[APRS_IS_PASSCODE_MAX_LEN + 1] = {0};
static aprs_is_proto_t protos[CONFIG_APRS_IS_PROTOS_MAX];
static uint8_t protos_idx = 0;
static bool wifi_has_ip = false;

static void aprs_is_hostname_resolved(const char *name, const ip_addr_t *addr, void *arg) {
  if (addr)
    memcpy(&server_addr, addr, sizeof(server_addr));
  else
    memset(&server_addr, 0, sizeof(server_addr));
  xSemaphoreGive(dns_semaphore);
}

static esp_err_t aprs_is_dns_resolve(const char *hostname, uint32_t *addr) {
  xSemaphoreTake(dns_semaphore, 0);  // Ensure the semaphore is in the right state
  memset(&server_addr, 0, sizeof(server_addr));
  err_t err =
      dns_gethostbyname_addrtype(hostname, &server_addr, aprs_is_hostname_resolved, NULL, LWIP_DNS_ADDRTYPE_IPV4);
  if (err == ERR_INPROGRESS)
    xSemaphoreTake(dns_semaphore, 5000 / portTICK_PERIOD_MS);
  else if (err != ERR_OK)
    return ESP_FAIL;
  if (server_addr.u_addr.ip4.addr == 0) return ESP_FAIL;
  *addr = server_addr.u_addr.ip4.addr;
  return ESP_OK;
}

static esp_err_t aprs_is_connect(const char *host, int16_t port) {
  if (sockfd >= 0) {
    ESP_LOGW(TAG, "Socket already open");
    return ESP_OK;
  }
  int opt = 1;
  struct sockaddr_in sockaddr = {
      .sin_family = AF_INET,
      .sin_port = htons(port),
  };
  if (aprs_is_dns_resolve(host, &sockaddr.sin_addr.s_addr) != ESP_OK) {
    ESP_LOGE(TAG, "Failed to resolve APRS-IS hostname %s", host);
    goto err;
  }
  sockfd = socket(AF_INET, SOCK_STREAM, 0);
  if (sockfd < 0) {
    ESP_LOGE(TAG, "Failed to create a new socket, fd=%d", sockfd);
    goto err;
  }
  if (setsockopt(sockfd, IPPROTO_TCP, TCP_NODELAY, &opt, sizeof(opt))) {
    ESP_LOGE(TAG, "Failed to setsockopt TCP_NODELAY");
    goto err;
  }
  if (connect(sockfd, (const struct sockaddr *)&sockaddr, sizeof(sockaddr))) {
    ESP_LOGE(TAG, "Failed to connect to %s:%d", host, port);
    goto err;
  }
  ESP_LOGI(TAG, "Connected to APRS-IS server %s:%d", host, port);
  return ESP_OK;
err:;
  if (sockfd >= 0 && closesocket(sockfd)) ESP_LOGW(TAG, "Failed to close socket after error");
  return ESP_FAIL;
}

static void aprs_is_disconnect(void) {
  if (shutdown(sockfd, SHUT_RDWR)) ESP_LOGW(TAG, "Failed to shutdown socket");
  if (close(sockfd)) ESP_LOGW(TAG, "Failed to close socket");
  sockfd = -1;
  received_hello = false;
  authenticated = false;
  verified = false;
}

static esp_err_t aprs_is_internal_vsendf(const char *fmt, va_list args) {
  esp_err_t err = ESP_OK;
  char *msg = malloc(APRS_IS_MAX_LINE_LEN + 1);
  memset(msg, 0, APRS_IS_MAX_LINE_LEN + 1);
  int len = vsprintf(msg, fmt, args);
  if (len < 0) {
    ESP_LOGE(TAG, "Failed to format outgoing APRS-IS packet, ret=%d", len);
    err = ESP_FAIL;
    goto end;
  }
  ESP_LOGD(TAG, "Sending APRS-IS message %s", msg);
  // Make sure the line is properly terminated with CRLF
  if (msg[len - 1] != '\n' || msg[len - 2] != '\r') {
    if (len > APRS_IS_MAX_LINE_LEN - 2) {
      ESP_LOGE(TAG, "Line too long. Can't terminate with CRLF, len=%d", len);
      ESP_LOG_BUFFER_HEXDUMP(TAG, msg, len, ESP_LOG_DEBUG);
      err = ESP_FAIL;
      goto end;
    }
    msg[len++] = '\r';
    msg[len++] = '\n';
  }
  ssize_t ret;
  if ((ret = send(sockfd, msg, len, 0)) < 0) {
    ESP_LOGE(TAG, "Failed to send message to APRS-IS, ret=%d", ret);
    ESP_LOG_BUFFER_HEXDUMP(TAG, msg, len, ESP_LOG_DEBUG);
    err = ESP_FAIL;
    goto end;
  }
end:;
  free(msg);
  return err;
}

static esp_err_t aprs_is_internal_sendf(const char *fmt, ...) {
  va_list args;
  va_start(args, fmt);
  esp_err_t err = aprs_is_internal_vsendf(fmt, args);
  va_end(args);
  return err;
}

esp_err_t aprs_is_vsendf(TickType_t block_time, const char *fmt, va_list args) {
  if (!socket_tx_mutex || !verified) return ESP_ERR_INVALID_STATE;
  if (xSemaphoreTake(socket_tx_mutex, block_time) != pdPASS) return ESP_ERR_TIMEOUT;
  esp_err_t err = aprs_is_internal_vsendf(fmt, args);
  xSemaphoreGive(socket_tx_mutex);
  return err;
}

esp_err_t aprs_is_sendf(TickType_t block_time, const char *fmt, ...) {
  va_list args;
  va_start(args, fmt);
  esp_err_t err = aprs_is_vsendf(block_time, fmt, args);
  va_end(args);
  return err;
}

static esp_err_t aprs_is_authenticate(void) {
  const esp_app_desc_t *app_desc = esp_app_get_description();
  //  filter r/33.25/-96.5/50
  esp_err_t err = aprs_is_internal_sendf("user %s pass %s vers %s %s filter r/33.25/-96.5/50", callsign, passcode,
                                         app_desc->project_name, app_desc->version);
  ESP_RETURN_ON_ERROR(err, TAG, "Failed to send authentication info to APRS-IS server");
  return ESP_OK;
}

static bool aprs_is_parse_logresp(const uint8_t *buf, ssize_t len) {
  // Expected format: # logresp <callsign> [un]verified, server <servercall>
  bool callsign_checked = false;
  int idx = 0;
  char verified_status[11] = {0};
  // Skip the first 10 characters (prefix '# logresp ')
  for (int i = 10; i < len; i++) {
    if (!callsign_checked) {
      if (buf[i] == ' ') {
        callsign_checked = true;
        idx = 0;
        continue;
      }
      if (buf[i] != callsign[idx++]) {
        ESP_LOGE(TAG, "Callsign mismatch. Expected '%s', but got '%.*s'", callsign, i + 1, buf);
        return false;
      }
    } else {
      if (buf[i] == ',') break;
      if (idx == 10) {
        ESP_LOGE(TAG, "Overflow, parsing verified status");
        return false;
      }
      verified_status[idx++] = buf[i];
    }
  }
  return strcmp(verified_status, "verified") == 0;
}

static void aprs_is_interpret_line(const uint8_t *buf, ssize_t len) {
  ESP_LOGD(TAG, "Interpreting line: %.*s", (int)len - 2, buf);
  // The very first received message is assumed to be the server hello. It signals that we can attempt to authenticate
  if (!received_hello) {
    ESP_LOGI(TAG, "Got server hello. Authenticating...");
    esp_err_t err = aprs_is_authenticate();
    if (err != ESP_OK) {
      ESP_LOGE(TAG, "Failed to authenticate to APRS-IS");
      if (err == ESP_FAIL)
        should_retry = true;
      else
        task_running = false;
      aprs_is_disconnect();
      return;
    }
    received_hello = true;
    return;
  }
  // The second received message should be a 'logresp', confirming we're authenticated
  if (!authenticated) {
    if (memcmp(buf, "# logresp", 9) != 0) {
      ESP_LOGE(TAG, "Received unexpected response. Expected '# logresp', got '%.*s'", len, buf);
      // Something's really wrong. Kill the task
      task_running = false;
      aprs_is_disconnect();
      return;
    }
    authenticated = true;
    verified = aprs_is_parse_logresp(buf, len);
    ESP_LOGI(TAG, "Authenticated, verified=%s", verified ? "YES" : "NO");
    return;
  }
  if (buf[0] == '#') return;
  aprs_is_packet_t packet = {
      .data = buf,
      .len = len,
  };
  for (uint8_t i = 0; i < protos_idx; i++) {
    ESP_LOGD(TAG, "Passing packet to proto %s", protos[i].name);
    protos[i].process_packet(&packet);
  }
}

static int aprs_is_parse_chunk(const uint8_t *buf, ssize_t len) {
  const uint8_t *line_start = buf;
  for (int i = 0; i < len; i++) {
    if (buf[i] == '\n') {
      int line_len = buf + i + 1 - line_start;
      aprs_is_interpret_line(line_start, line_len);
      line_start += line_len;
    }
  }
  return line_start - buf;
}

static void aprs_is_task(void *arg) {
  uint8_t *buf = malloc(APRS_IS_MAX_LINE_LEN);
  memset(buf, 0, APRS_IS_MAX_LINE_LEN);
  ssize_t len = 0;
  uint32_t timeout = 1000;
  while (task_running) {
    len = 0;
    ESP_LOGI(TAG, "Connecting to APRS-IS server...");
    esp_err_t err = aprs_is_connect(APRS_IS_HOST, APRS_IS_PORT);
    if (err != ESP_OK) {
      ESP_LOGE(TAG, "Failed to connect to APRS-IS");
      if (wifi_has_ip) {
        goto retry;
      } else {
        task_running = false;
        continue;
      }
    }
    while ((len = recv(sockfd, buf + len, APRS_IS_MAX_LINE_LEN - len, 0) + len) > 0) {
      ESP_LOGD(TAG, "Received from APRS-IS:");
      ESP_LOG_BUFFER_HEXDUMP(TAG, buf, len, ESP_LOG_DEBUG);
      uint8_t *b = buf;
      int consumed = 0;
      while (b < (buf + len) && (consumed = aprs_is_parse_chunk(b, len)) > 0) {
        len -= consumed;
        b += consumed;
      }
      if (b == buf && len >= APRS_IS_MAX_LINE_LEN) {
        ESP_LOGW(TAG, "APRS-IS Rx buffer overflow");
        ESP_LOG_BUFFER_HEXDUMP(TAG, buf, len, ESP_LOG_DEBUG);
        len = 0;
        continue;
      }
      if (b < (buf + len)) memmove(buf, b, len);
    }
    if (authenticated) timeout = 1000;
    ESP_LOGI(TAG, "Socket has been closed");
    aprs_is_disconnect();
    if (!should_retry) continue;
  retry:;
    ESP_LOGI(TAG, "Reconnecting...");
    vTaskDelay(timeout / portTICK_PERIOD_MS);
    should_retry = false;
    timeout *= 2;
    if (timeout >= 32000) timeout = 32000;
  }
  ESP_LOGI(TAG, "APRS-IS task exiting...");
  free(buf);
  aprs_is_task_handle = NULL;
  vTaskDelete(NULL);
}

static void aprs_is_start(void) {
  if (aprs_is_task_handle || !wifi_has_ip) return;
  BaseType_t ret = pdPASS;
  task_running = true;
  ret = xTaskCreate(aprs_is_task, "aprs-is", 4096, NULL, 12, &aprs_is_task_handle);
  if (ret != pdPASS) {
    ESP_LOGW(TAG, "Failed to start APRS-IS task");
  }
}

static void aprs_is_on_ip_event(void *handler_arg, esp_event_base_t base, int32_t id, void *event_data) {
  switch (id) {
    case IP_EVENT_STA_GOT_IP:
      wifi_has_ip = true;
      if (task_running) break;
      aprs_is_start();
      break;
    case IP_EVENT_STA_LOST_IP:
      wifi_has_ip = false;
      if (!task_running) break;
      ESP_LOGI(TAG, "Disconnecting from APRS-IS server...");
      task_running = false;
      aprs_is_disconnect();
      break;
  }
}

static void aprs_is_on_setting_updated(void *arg, esp_event_base_t base, int32_t id, void *event_data) {
  settings_updated_event_t *event = (settings_updated_event_t *)event_data;
  size_t len;
  esp_err_t err;
  bool reconnect = false;
  if (strcmp(event->key, SETTINGS_KEY_CALLSIGN) == 0) {
    len = HAM_STA_CALL_SSID_MAX_LEN + 1;
    char new_callsign[HAM_STA_CALL_SSID_MAX_LEN + 1];
    err = settings_get_str_default(event->key, new_callsign, &len, HAM_STA_CALLSIGN_DEFAULT);
    if (err != ESP_OK) {
      ESP_LOGE(TAG, "Failed to get the updated callsign, err=%d", err);
      return;
    }
    if (strcmp(new_callsign, callsign) == 0) return;
    strcpy(callsign, new_callsign);
    reconnect = true;
  } else if (strcmp(event->key, SETTINGS_KEY_PASSCODE) == 0) {
    len = APRS_IS_PASSCODE_MAX_LEN + 1;
    char new_passcode[APRS_IS_PASSCODE_MAX_LEN + 1];
    err = settings_get_str_default(event->key, new_passcode, &len, APRS_IS_PASSCODE_DEFAULT);
    if (err != ESP_OK) {
      ESP_LOGE(TAG, "Failed to get the updated passcode, err=%d", err);
      return;
    }
    if (strcmp(new_passcode, passcode) == 0) return;
    strcpy(passcode, new_passcode);
    reconnect = true;
  }
  if (reconnect) {
    if (aprs_is_task_handle)
      aprs_is_disconnect();
    else
      aprs_is_start();
  }
}

bool aprs_is_connected() { return task_running && received_hello && authenticated; }

esp_err_t aprs_is_init(void) {
  size_t len;
  esp_err_t ret = ESP_OK;
  dns_semaphore = xSemaphoreCreateBinary();
  socket_tx_mutex = xSemaphoreCreateMutex();
  ret = esp_event_handler_instance_register(IP_EVENT, IP_EVENT_STA_GOT_IP, aprs_is_on_ip_event, NULL,
                                            &ip_got_event_handler);
  ESP_GOTO_ON_ERROR(ret, error, TAG, "Failed to register STA_GOT_IP event handler");
  ret = esp_event_handler_instance_register(IP_EVENT, IP_EVENT_STA_LOST_IP, aprs_is_on_ip_event, NULL,
                                            &ip_lost_event_handler);
  ESP_GOTO_ON_ERROR(ret, error, TAG, "Failed to register STA_LOST_IP event handler");
  ret = esp_event_handler_instance_register(SETTINGS_EVENT, SETTINGS_EVENT_UPDATED, aprs_is_on_setting_updated, NULL,
                                            &settings_updated_event_handler);
  ESP_GOTO_ON_ERROR(ret, error, TAG, "Failed to register SETTINGS_EVENT_UPDATED event handler");
  len = HAM_STA_CALL_SSID_MAX_LEN + 1;
  ret = settings_get_str_default(SETTINGS_KEY_CALLSIGN, callsign, &len, HAM_STA_CALLSIGN_DEFAULT);
  ESP_GOTO_ON_FALSE(ret != ESP_ERR_NVS_INVALID_LENGTH, ESP_ERR_INVALID_ARG, error, TAG,
                    "Configured callsign cannot be longer than %d characters", HAM_STA_CALL_SSID_MAX_LEN);
  ESP_GOTO_ON_ERROR(ret, error, TAG, "Failed to get callsign from settings");
  len = APRS_IS_PASSCODE_MAX_LEN + 1;
  ret = settings_get_str_default(SETTINGS_KEY_PASSCODE, passcode, &len, APRS_IS_PASSCODE_DEFAULT);
  ESP_GOTO_ON_FALSE(ret != ESP_ERR_NVS_INVALID_LENGTH, ESP_ERR_INVALID_ARG, error, TAG,
                    "Configured passcode cannot be longer than %d digits", APRS_IS_PASSCODE_MAX_LEN);
  ESP_GOTO_ON_ERROR(ret, error, TAG, "Failed to get passcode from settings");
  return ret;
error:;
  if (ip_got_event_handler) {
    esp_event_handler_instance_unregister(IP_EVENT, IP_EVENT_STA_GOT_IP, ip_got_event_handler);
    ip_got_event_handler = NULL;
  }
  if (ip_lost_event_handler) {
    esp_event_handler_instance_unregister(IP_EVENT, IP_EVENT_STA_LOST_IP, ip_lost_event_handler);
    ip_lost_event_handler = NULL;
  }
  if (settings_updated_event_handler) {
    esp_event_handler_instance_unregister(SETTINGS_EVENT, SETTINGS_EVENT_UPDATED, settings_updated_event_handler);
    settings_updated_event_handler = NULL;
  }
  if (dns_semaphore) {
    vSemaphoreDelete(dns_semaphore);
    dns_semaphore = NULL;
  }
  if (socket_tx_mutex) {
    vSemaphoreDelete(socket_tx_mutex);
    socket_tx_mutex = NULL;
  }
  return ret;
}

esp_err_t aprs_is_register_proto(const aprs_is_proto_t *proto) {
  if (protos_idx >= CONFIG_APRS_IS_PROTOS_MAX) {
    ESP_LOGE(
        TAG,
        "Not enough space to register APRS-IS proto %s. Increase CONFIG_APRS_IS_PROTOS_MAX in sdkconfig to allocate "
        "more space",
        proto->name);
    return ESP_FAIL;
  }
  memcpy(&protos[protos_idx++], proto, sizeof(aprs_is_proto_t));
  return ESP_OK;
}
