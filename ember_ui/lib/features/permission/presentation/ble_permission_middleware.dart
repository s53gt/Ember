import 'package:ember_ui/features/permission/application/ble_permission.dart';
import 'package:ember_ui/features/permission/domain/permission_result.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class BlePermissionMiddleware extends ConsumerStatefulWidget {
  final Widget child;

  const BlePermissionMiddleware({super.key, required this.child});

  @override
  ConsumerState<ConsumerStatefulWidget> createState() =>
      _BlePermissionMiddleware();
}

class _BlePermissionMiddleware extends ConsumerState<BlePermissionMiddleware> {
  late final AppLifecycleListener _listener;
  Future<void>? _requestBlePermissionFuture;
  ScaffoldFeatureController<MaterialBanner, MaterialBannerClosedReason>?
      _banner;

  @override
  void initState() {
    super.initState();
    _listener = AppLifecycleListener(
      onStateChange: _onLifecycleStateChanged,
    );
  }

  @override
  void dispose() {
    _listener.dispose();
    super.dispose();
  }

  void _onLifecycleStateChanged(AppLifecycleState state) {
    final blePermRes = ref.read(blePermissionProvider);
    if (state == AppLifecycleState.resumed &&
        blePermRes == PermissionResult.permanentlyDenied) {
      final locPermNotif = ref.read(blePermissionProvider.notifier);
      _requestBlePermissionFuture = locPermNotif.request(false);
    }
  }

  @override
  Widget build(BuildContext context) {
    final blePermNotif = ref.read(blePermissionProvider.notifier);
    _requestBlePermissionFuture ??= blePermNotif.request(false);
    final blePermRes = ref.watch(blePermissionProvider);
    if (isLocPermDenied(blePermRes) && _banner == null) {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        setState(() {
          _banner = ScaffoldMessenger.of(context).showMaterialBanner(
            MaterialBanner(
              backgroundColor: Theme.of(context).colorScheme.errorContainer,
              content: Builder(builder: (context) {
                final localizations = AppLocalizations.of(context)!;
                return Text(localizations.missingBlePerm);
              }),
              actions: [
                TextButton(
                  style: ButtonStyle(
                    foregroundColor: WidgetStatePropertyAll(
                        Theme.of(context).colorScheme.error),
                  ),
                  onPressed: () => blePermNotif.request(true),
                  child: Builder(builder: (context) {
                    final localizations = AppLocalizations.of(context)!;
                    return Text(localizations.requestPerm);
                  }),
                ),
              ],
            ),
          );
        });
      });
    } else if (!isLocPermDenied(blePermRes) && _banner != null) {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        setState(() {
          _banner!.close();
          _banner = null;
        });
      });
    }
    return widget.child;
  }
}
