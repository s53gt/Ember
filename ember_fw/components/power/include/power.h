#pragma once

#include "freertos/FreeRTOS.h"
#include "esp_err.h"

esp_err_t power_init(void);
esp_err_t power_get_batt_volt(int *voltage, TickType_t block_time);
esp_err_t power_get_batt_percent(uint8_t *percent, TickType_t block_time);
