import 'dart:async';

import 'package:ember_ui/exceptions/invalid_state_exception.dart';
import 'package:ember_ui/features/device_control/data/ble_device_repository.dart';
import 'package:ember_ui/features/device_control/data/device_repository.dart';
import 'package:ember_ui/features/device_control/domain/device.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'device_service.g.dart';

class DeviceService {
  final Map<LinkType, DeviceRepository> _repos;
  final Map<LinkType,
          StreamSubscription<MapEntry<String, DeviceConnectionState>>>
      _connectionStateSubscriptions = {};
  late final StreamController<Device?> _connectedDeviceController;
  Device? _connectedDevice;
  bool _connectedDeviceHasListeners = false;

  /// Currently connected device, otherwise null
  Device? get connectedDevice => _connectedDevice;

  /// Same as connectedDevice, but as a broadcast stream instead
  Stream<Device?> get connectedDeviceStream =>
      _connectedDeviceController.stream;

  DeviceService(this._repos) {
    _connectedDeviceController = StreamController.broadcast(
      onListen: () => _connectedDeviceHasListeners = true,
      onCancel: () => _connectedDeviceHasListeners = false,
    );
    for (var repo in _repos.entries) {
      final sub = repo.value.connectionState.listen((entry) {
        if (entry.key == _connectedDevice?.id &&
            entry.value == DeviceConnectionState.disconnected) {
          _setConnectedDevice(null);
        }
      });
      _connectionStateSubscriptions[repo.key] = sub;
    }
  }

  void dispose() async {
    for (var sub in _connectionStateSubscriptions.values) {
      sub.cancel();
    }
    _connectedDeviceController.close();
  }

  /// Synchronously scans for available devices using the selected linkType. NOTE: The amount of time this call blocks
  /// for is determined by the underlying repository handling the discovery. E.g. BLE blocks for 5 seconds.
  /// @param linkType Communication method to use when looking for devices
  /// @returns List of discovered devices
  Future<List<Device>> discoverDevices(LinkType linkType) async {
    final repo = _repos[linkType];
    if (repo == null) throw UnimplementedError();
    return repo.discoverDevices();
  }

  /// Stops discovery previously started by `discoverDevices()`
  /// @param linkType LinkType used when starting discovery
  /// @see discoverDevices()
  Future<void> stopDeviceDiscovery(LinkType linkType) async {
    final repo = _repos[linkType];
    if (repo == null) throw UnimplementedError();
    await repo.stopDeviceDiscovery();
  }

  /// Returns a stream of discovered devices, updated as the underlying repository discovers new devices. This can be
  /// used during a running scan to give user real-time feedback, instead of waiting for the `discoverDevices()` future
  /// to complete. NOTE: Call to `discoverDevices()` is still required to initiate the scan.
  /// @param linkType Same linkType used when starting discovery
  /// @returns Real-time stream of discovered devices
  Stream<Device> discoveredDevices(LinkType linkType) {
    final repo = _repos[linkType];
    if (repo == null) throw UnimplementedError();
    return repo.discoveredDevices;
  }

  /// @param linkType Same linkType used when starting discovery
  /// @returns Stream of booleans, updated as device discovery is started/finished
  Stream<bool> isScanningStream(LinkType linkType) {
    final repo = _repos[linkType];
    if (repo == null) throw UnimplementedError();
    return repo.isScanning;
  }

  /// Connects to a device previously discovered by `discoverDevices()`. If a device is already connected, it is first
  /// disconnected. Once a connection is established, `connectedDevice` is updated. This operation may block for up to
  /// 15 seconds, before throwing a TimeoutException
  /// @param device Device to establish a connection with
  Future<void> connect(Device device) async {
    if (_connectedDevice != null) {
      await AsyncValue.guard(() => disconnect());
    }
    final repo = _repos[device.linkType];
    if (repo == null) throw UnimplementedError();
    final stateFuture = repo.connectionState
        .timeout(const Duration(seconds: 15))
        .firstWhere((v) => v.key == device.id);
    await repo.connect(device);
    final state = await stateFuture;
    if (state.value != DeviceConnectionState.connected) {
      throw TimeoutException("BLE connect timed out");
    }
    _setConnectedDevice(device.copyWith(connected: true));
    // The OS likes to cache old names for a little while, so if it was just changed and you try to reconnect, the scan
    // result will return the old name
    await refreshDeviceName();
  }

  /// Disconnects `connectedDevice`, if any. May block for up to 5 seconds
  Future<void> disconnect() async {
    if (_connectedDevice == null) return;
    final repo = _repos[_connectedDevice!.linkType];
    if (repo == null) throw UnimplementedError();
    final id = _connectedDevice!.id;
    final stateFuture = repo.connectionState
        .timeout(const Duration(seconds: 5))
        .firstWhere((v) => v.key == id);
    await repo.disconnect(_connectedDevice!);
    final state = await stateFuture;
    if (state.value != DeviceConnectionState.disconnected) {
      throw TimeoutException("BLE disconnect timed out");
    }
  }

  /// Checks, if a device is connected
  bool isConnected(Device device) {
    final repo = _repos[device.linkType];
    if (repo == null) throw UnimplementedError();
    return repo.isConnected(device);
  }

  /// Refreshes the device name
  Future<void> refreshDeviceName() async {
    if (_connectedDevice == null) throw InvalidStateException();
    late final String name;
    final repo = _repos[_connectedDevice!.linkType];
    if (repo == null) throw UnimplementedError();
    name = await repo.getName(_connectedDevice!);
    _setConnectedDevice(_connectedDevice!.copyWith(name: name));
  }

  void _setConnectedDevice(Device? device) {
    _connectedDevice = device;
    if (_connectedDeviceHasListeners) _connectedDeviceController.add(device);
  }
}

@riverpod
DeviceService deviceService(DeviceServiceRef ref) {
  final service = DeviceService(
    {
      LinkType.ble: ref.watch(bleDeviceRepositoryProvider),
    },
  );
  ref.onDispose(() => service.dispose());
  return service;
}
