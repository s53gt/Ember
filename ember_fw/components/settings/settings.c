#include "settings.h"

#include <string.h>

#include "esp_check.h"
#include "esp_err.h"
#include "esp_event.h"
#include "nvs.h"
#include "nvs_flash.h"
#include "portmacro.h"

#define SETTINGS_SET_NUM(BITS)                                                                                     \
  esp_err_t settings_set_i##BITS(const char* key, int##BITS##_t value) {                                           \
    esp_err_t err = nvs_set_i##BITS(handle, key, value);                                                           \
    ESP_RETURN_ON_ERROR(err, TAG, "Failed to set i%d %s", BITS, key);                                              \
    err = nvs_commit(handle);                                                                                      \
    ESP_RETURN_ON_ERROR(err, TAG, "Failed to commit i%d %s", BITS, key);                                           \
    settings_updated_event_t event = {.type = INT##BITS, .key = {0}};                                              \
    memcpy(event.key, key, strlen(key));                                                                           \
    event.i##BITS = value;                                                                                         \
    err = esp_event_post(SETTINGS_EVENT, SETTINGS_EVENT_UPDATED, &event, sizeof(event), 100 / portTICK_PERIOD_MS); \
    if (err != ESP_OK) {                                                                                           \
      ESP_LOGW(TAG, "Failed to post settings updated event, err=%d, key=%s", err, key);                            \
    }                                                                                                              \
    return ESP_OK;                                                                                                 \
  }                                                                                                                \
  esp_err_t settings_set_u##BITS(const char* key, uint##BITS##_t value) {                                          \
    esp_err_t err = nvs_set_u##BITS(handle, key, value);                                                           \
    ESP_RETURN_ON_ERROR(err, TAG, "Failed to set u%d %s", BITS, key);                                              \
    err = nvs_commit(handle);                                                                                      \
    ESP_RETURN_ON_ERROR(err, TAG, "Failed to commit u%d %s", BITS, key);                                           \
    settings_updated_event_t event = {.type = UINT##BITS, .key = {0}};                                             \
    memcpy(event.key, key, strlen(key));                                                                           \
    event.u##BITS = value;                                                                                         \
    err = esp_event_post(SETTINGS_EVENT, SETTINGS_EVENT_UPDATED, &event, sizeof(event), 100 / portTICK_PERIOD_MS); \
    if (err != ESP_OK) {                                                                                           \
      ESP_LOGW(TAG, "Failed to post settings updated event, err=%d, key=%s", err, key);                            \
    }                                                                                                              \
    return ESP_OK;                                                                                                 \
  }

#define SETTINGS_GET_NUM(BITS)                                                                           \
  esp_err_t settings_get_i##BITS##_default(const char* key, int##BITS##_t* value, int##BITS##_t def) {   \
    esp_err_t err = nvs_get_i##BITS(handle, key, value);                                                 \
    if (err == ESP_ERR_NVS_NOT_FOUND) *value = def;                                                      \
    return err;                                                                                          \
  }                                                                                                      \
  esp_err_t settings_get_u##BITS##_default(const char* key, uint##BITS##_t* value, uint##BITS##_t def) { \
    esp_err_t err = nvs_get_u##BITS(handle, key, value);                                                 \
    if (err == ESP_ERR_NVS_NOT_FOUND) *value = def;                                                      \
    return err;                                                                                          \
  }                                                                                                      \
  esp_err_t settings_get_i##BITS(const char* key, int##BITS##_t* value) {                                \
    return nvs_get_i##BITS(handle, key, value);                                                          \
  }                                                                                                      \
  esp_err_t settings_get_u##BITS(const char* key, uint##BITS##_t* value) { return nvs_get_u##BITS(handle, key, value); }

ESP_EVENT_DEFINE_BASE(SETTINGS_EVENT);

static const char* TAG = "settings";
static const char* SETTINGS_NVS_NS = "settings";

static nvs_handle_t handle = 0;

esp_err_t settings_init() {
  if (handle) return ESP_ERR_INVALID_STATE;
  esp_err_t err = nvs_flash_init();
  if (err == ESP_ERR_NVS_NO_FREE_PAGES || err == ESP_ERR_NVS_NEW_VERSION_FOUND) {
    // NVS partition was truncated and needs to be erased
    // Retry nvs_flash_init
    ESP_RETURN_ON_ERROR(nvs_flash_erase(), TAG, "Can't erase NVS flash");
    err = nvs_flash_init();
  }
  ESP_RETURN_ON_ERROR(err, TAG, "Failed to init NVS :(");
  err = nvs_open(SETTINGS_NVS_NS, NVS_READWRITE, &handle);
  ESP_RETURN_ON_ERROR(err, TAG, "Failed to open NVS settings namespace :(");
  return ESP_OK;
}

SETTINGS_GET_NUM(64);
SETTINGS_GET_NUM(32);
SETTINGS_GET_NUM(16);
SETTINGS_GET_NUM(8);

SETTINGS_SET_NUM(64);
SETTINGS_SET_NUM(32);
SETTINGS_SET_NUM(16);
SETTINGS_SET_NUM(8);

esp_err_t settings_set_blob(const char* key, const void* value, size_t len) {
  esp_err_t err = nvs_set_blob(handle, key, value, len);
  ESP_RETURN_ON_ERROR(err, TAG, "Failed to set NVS blob %s", key);
  err = nvs_commit(handle);
  ESP_RETURN_ON_ERROR(err, TAG, "Failed to commit NVS blob %s", key);
  settings_updated_event_t event = {.type = BLOB, .key = {0}};
  memcpy(event.key, key, strlen(key));
  err = esp_event_post(SETTINGS_EVENT, SETTINGS_EVENT_UPDATED, &event, sizeof(event), 100 / portTICK_PERIOD_MS);
  if (err != ESP_OK) {
    ESP_LOGW(TAG, "Failed to post settings updated event, err=%d, key=%s", err, key);
  }
  return ESP_OK;
}

esp_err_t settings_set_str(const char* key, const char* value) {
  esp_err_t err = nvs_set_str(handle, key, value);
  ESP_RETURN_ON_ERROR(err, TAG, "Failed to set NVS str %s", key);
  err = nvs_commit(handle);
  ESP_RETURN_ON_ERROR(err, TAG, "Failed to commit NVS str %s", key);
  settings_updated_event_t event = {.type = STRING, .key = {0}};
  memcpy(event.key, key, strlen(key));
  err = esp_event_post(SETTINGS_EVENT, SETTINGS_EVENT_UPDATED, &event, sizeof(event), 100 / portTICK_PERIOD_MS);
  if (err != ESP_OK) {
    ESP_LOGW(TAG, "Failed to post settings updated event, err=%d, key=%s", err, key);
  }
  return ESP_OK;
}

esp_err_t settings_get_blob(const char* key, void* value, size_t* len) { return nvs_get_blob(handle, key, value, len); }

esp_err_t settings_get_str(const char* key, char* value, size_t* len) { return nvs_get_str(handle, key, value, len); }

esp_err_t settings_get_blob_default(const char* key, void* value, size_t* len, const void* def, const size_t def_len) {
  esp_err_t err = nvs_get_blob(handle, key, value, len);
  if (err == ESP_ERR_NVS_NOT_FOUND) {
    if (!value) {
      *len = def_len;
      return ESP_OK;
    }
    if (def_len > *len) return ESP_ERR_NVS_INVALID_LENGTH;
    *len = def_len;
    memcpy(value, def, def_len);
    return ESP_OK;
  }
  return err;
}

esp_err_t settings_get_str_default(const char* key, char* value, size_t* len, const char* def) {
  esp_err_t err = nvs_get_str(handle, key, value, len);
  if (err == ESP_ERR_NVS_NOT_FOUND) {
    size_t def_len = strlen(def) + 1;
    if (!value) {
      *len = def_len;
      return ESP_OK;
    }
    if (def_len > *len) return ESP_ERR_NVS_INVALID_LENGTH;
    *len = def_len;
    memcpy(value, def, def_len);
    return ESP_OK;
  }
  return err;
}

esp_err_t settings_erase() {
  if (handle != 0) nvs_close(handle);
  handle = 0;
  ESP_RETURN_ON_ERROR(nvs_flash_erase(), TAG, "Can't erase NVS flash");
  return settings_init();
}
