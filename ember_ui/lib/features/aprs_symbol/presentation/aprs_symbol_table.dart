import 'dart:convert';

import 'package:ember_ui/features/aprs_symbol/presentation/aprs_symbol_button.dart';
import 'package:flutter/material.dart';

final firstSymbolCode = utf8.encode("!")[0];
final lastSymbolCode = utf8.encode("~")[0];

final ovlayFirstSymbolCode = utf8.encode("A")[0];
final ovlayLastSymbolCode = utf8.encode("Z")[0];
final ovlayNumFirstSymbolCode = utf8.encode("0")[0];
final ovlayNumLastSymbolCode = utf8.encode("9")[0];

class AprsSymbolTable extends StatelessWidget {
  final String table;
  final String selectedSymbol;
  final void Function(String) onPressed;

  const AprsSymbolTable({
    super.key,
    required this.table,
    required this.selectedSymbol,
    required this.onPressed,
  });

  @override
  Widget build(BuildContext context) {
    // Remove empty overlay icons
    List<String> symbols;
    if (table == "\x20") {
      final ovlayLetters = List.generate(
          ovlayLastSymbolCode - ovlayFirstSymbolCode,
          (i) => utf8.decode([ovlayFirstSymbolCode + i]));
      final ovlayNumbers = List.generate(
          ovlayNumLastSymbolCode - ovlayNumFirstSymbolCode,
          (i) => utf8.decode([ovlayNumFirstSymbolCode + i]));
      symbols = [...ovlayNumbers, ...ovlayLetters];
    } else {
      symbols = List.generate(lastSymbolCode - firstSymbolCode,
          (i) => utf8.decode([firstSymbolCode + i]));
    }
    return Wrap(
      children: symbols
          .map(
            (s) => Padding(
              padding: const EdgeInsets.all(5),
              child: AprsSymbolButton(
                tableChr: table,
                symbolChr: s,
                onPressed: () => onPressed(s),
                selected: table == "\x20"
                    ? selectedSymbol[0] == s
                    : table != "/" &&
                            selectedSymbol[0] != "\\" &&
                            selectedSymbol[0] != "/"
                        ? selectedSymbol[1] == s
                        : "$table$s" == selectedSymbol,
              ),
            ),
          )
          .toList(),
    );
  }
}
