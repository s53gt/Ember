import 'package:ember_ui/features/device_control/application/device_service.dart';
import 'package:ember_ui/features/device_control/application/ham_sta_service.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'current_aprs_symbol.g.dart';

@riverpod
class CurrentAprsSymbol extends _$CurrentAprsSymbol {
  void refresh() => ref.invalidateSelf();

  @override
  Future<String> build() async {
    final deviceService = ref.watch(deviceServiceProvider);
    final subscription =
        deviceService.connectedDeviceStream.listen((_) => refresh());
    ref.onDispose(subscription.cancel);
    final hamStaService = ref.watch(hamStaServiceProvider);
    return hamStaService.getSymbol();
  }
}
