import 'package:drift/drift.dart';
import 'package:ember_ui/features/database/data/database.dart';
import 'package:ember_ui/features/tracker/data/mapper/position_report_mapper.dart';
import 'package:ember_ui/features/tracker/domain/aprs_position.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'position_report_repository.g.dart';

@riverpod
class PositionReportRepository extends _$PositionReportRepository {
  @override
  Stream<List<AprsPosition>> build() {
    final database = ref.watch(databaseProvider);
    final posIdQuery = database.positionReport.selectOnly()
      ..addColumns([database.positionReport.id.max()])
      ..groupBy([database.positionReport.fromCall]);
    final query = database.positionReport.select()
      ..where((row) => row.id.isInQuery(posIdQuery));
    return query.watch().map(PositionReportMapper.transformToModelList);
  }

  Future<void> append(AprsPosition position) async {
    final database = ref.read(databaseProvider);
    database.positionReport.insertOne(PositionReportCompanion.insert(
      fromCall: position.header.from,
      toCall: position.header.to,
      path: Value(position.header.path),
      latitude: position.latitude,
      longitude: position.longitude,
      ambiguity: position.ambiguity,
      tableChr: position.tableChr,
      symbolChr: position.symbolChr,
      altitude: Value(position.altitude),
      comment: Value(position.comment),
      timestamp: Value(position.timestamp),
    ));
  }
}
