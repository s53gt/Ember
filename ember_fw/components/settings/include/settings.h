#pragma once

#include <stdint.h>

#include "esp_err.h"
#include "esp_event_base.h"
#include "settings_default.h"

#define SETTINGS_KEY_CALLSIGN "callsign"
#define SETTINGS_KEY_PASSCODE "passcode"
#define SETTINGS_KEY_SYMBOL "symbol"

ESP_EVENT_DECLARE_BASE(SETTINGS_EVENT);

enum {
  SETTINGS_EVENT_UPDATED,
};

typedef enum {
  STRING = 0,
  BLOB,
  UINT8,
  UINT16,
  UINT32,
  UINT64,
  INT8,
  INT16,
  INT32,
  INT64,
} settings_datatype_t;

typedef struct {
  settings_datatype_t type;
  char key[16];
  union {
    uint8_t u8;
    uint16_t u16;
    uint32_t u32;
    uint64_t u64;
    int8_t i8;
    int16_t i16;
    int32_t i32;
    int64_t i64;
  };
} settings_updated_event_t;

#define SETTINGS_GET_NUM_H(BITS)                                                                        \
  esp_err_t settings_get_i##BITS##_default(const char* key, int##BITS##_t* value, int##BITS##_t def);   \
  esp_err_t settings_get_u##BITS##_default(const char* key, uint##BITS##_t* value, uint##BITS##_t def); \
  esp_err_t settings_get_i##BITS(const char* key, int##BITS##_t* value);                                \
  esp_err_t settings_get_u##BITS(const char* key, uint##BITS##_t* value);

#define SETTINGS_SET_NUM_H(BITS)                                        \
  esp_err_t settings_set_i##BITS(const char* key, int##BITS##_t value); \
  esp_err_t settings_set_u##BITS(const char* key, uint##BITS##_t value);

esp_err_t settings_init();
esp_err_t settings_erase();
SETTINGS_GET_NUM_H(64);
SETTINGS_GET_NUM_H(32);
SETTINGS_GET_NUM_H(16);
SETTINGS_GET_NUM_H(8);
SETTINGS_SET_NUM_H(64);
SETTINGS_SET_NUM_H(32);
SETTINGS_SET_NUM_H(16);
SETTINGS_SET_NUM_H(8);
esp_err_t settings_set_blob(const char* key, const void* value, size_t len);
esp_err_t settings_set_str(const char* key, const char* value);
esp_err_t settings_get_blob(const char* key, void* value, size_t* len);
esp_err_t settings_get_str(const char* key, char* value, size_t* len);
esp_err_t settings_get_blob_default(const char* key, void* value, size_t* len, const void* def, const size_t def_len);
esp_err_t settings_get_str_default(const char* key, char* value, size_t* len, const char* def);
