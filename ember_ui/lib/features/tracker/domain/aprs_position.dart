import 'package:ember_ui/features/tracker/domain/tnc2_header.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'aprs_position.freezed.dart';

@freezed
class AprsPosition with _$AprsPosition {
  const factory AprsPosition({
    required Tnc2Header header,
    required double latitude,
    required double longitude,
    required int ambiguity,
    required String tableChr,
    required String symbolChr,
    required String comment,
    int? altitude,
    DateTime? timestamp,
  }) = _AprsPosition;
}
