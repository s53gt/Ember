// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ble_scan_list_controller.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$bleScanListControllerHash() =>
    r'eca6703ef3d3dc3610b9c729636cf08e00f50595';

/// See also [BleScanListController].
@ProviderFor(BleScanListController)
final bleScanListControllerProvider =
    AutoDisposeNotifierProvider<BleScanListController, List<Device>>.internal(
  BleScanListController.new,
  name: r'bleScanListControllerProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$bleScanListControllerHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$BleScanListController = AutoDisposeNotifier<List<Device>>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
