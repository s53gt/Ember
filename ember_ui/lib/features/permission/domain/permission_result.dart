enum PermissionResult { pending, granted, denied, permanentlyDenied }

bool isLocPermDenied(PermissionResult result) =>
    result == PermissionResult.denied ||
    result == PermissionResult.permanentlyDenied;
