// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'wifi_service.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$wifiServiceHash() => r'a630a2fce079d4a528276a1b04b731e46ed7254b';

/// See also [wifiService].
@ProviderFor(wifiService)
final wifiServiceProvider = AutoDisposeProvider<WifiService>.internal(
  wifiService,
  name: r'wifiServiceProvider',
  debugGetCreateSourceHash:
      const bool.fromEnvironment('dart.vm.product') ? null : _$wifiServiceHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef WifiServiceRef = AutoDisposeProviderRef<WifiService>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
