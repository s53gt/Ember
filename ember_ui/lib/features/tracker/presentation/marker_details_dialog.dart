import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

Future<void> showMarkerDetailsDialog(BuildContext context) {
  final localizations = AppLocalizations.of(context)!;
  return showDialog(
    context: context,
    builder: (context) => AlertDialog.adaptive(
      title: const Text("TODO"),
      content: const Text("CONTENT"),
      actions: [
        TextButton(
          onPressed: () {},
          child: Text(localizations.ok),
        ),
        TextButton(
          onPressed: () {},
          child: Text(localizations.cancel),
        ),
      ],
    ),
  );
}
