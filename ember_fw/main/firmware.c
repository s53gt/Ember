#include "aprs_is.h"
#include "digi.h"
#include "ember_ble_gatts.h"
#include "esp_check.h"
#include "settings.h"
#if CONFIG_DISPLAY_ENABLE
#include "display.h"
#include "ui.h"
#endif
#include "esp_err.h"
#include "esp_event.h"
#include "esp_log.h"
#include "esp_netif.h"
#if CONFIG_GNSS_ENABLE
#include "gnss.h"
#endif
#include "ember_ble.h"
#include "igate.h"
#include "lora.h"
#include "power.h"
#include "repl.h"
#include "tnc2.h"
#include "wifi.h"

#define LOG_INIT(X, MOD)                      \
  {                                           \
    ESP_LOGI(TAG, "Initializing " MOD "..."); \
    ESP_ERROR_CHECK(X);                       \
    ESP_LOGI(TAG, MOD " initialized");        \
  }

static const char* TAG = "main";

void app_main(void) {
  LOG_INIT(power_init(), "power");
  LOG_INIT(settings_init(), "settings");
  LOG_INIT(esp_netif_init(), "NETIF");
  LOG_INIT(esp_event_loop_create_default(), "Default Event Loop");
  LOG_INIT(lora_init(), "LoRa");
  LOG_INIT(wifi_enable(), "WiFi");
  LOG_INIT(ember_ble_init(), "BLE");
  LOG_INIT(repl_init(), "REPL");
#if CONFIG_GNSS_ENABLE
  LOG_INIT(gnss_init(), "gnss");
#endif
#if CONFIG_DISPLAY_ENABLE
  LOG_INIT(display_init(), "display");
  LOG_INIT(ui_init(), "display");
#endif
  LOG_INIT(tnc2_init(), "TNC-2");
  LOG_INIT(aprs_is_init(), "APRS-IS");
  LOG_INIT(digi_init(), "Digipeater");
  LOG_INIT(igate_init(), "iGate");
  wifi_connect();
#if 0
  int secs = 60;
  while (1) {
    if (secs++ >= 60) {
      secs = 0;
      ESP_LOGI(TAG, "Sending packet");
      esp_err_t err =
          lora_transmit((uint8_t*)"<\xff\x01S53GT-7>GPS,WIDE1-1:@201419z4603.99N/01428.34E>Hello LoRa!", 61, portMAX_DELAY);
      if (err != ESP_OK) ESP_LOGW(TAG, "Failed to transmit packet");
    }
    vTaskDelay(1000 / portTICK_PERIOD_MS);
  }
#endif
  uint8_t percent;
  while (1) {
    gnss_fix_t fix = {0};
    gnss_get_fix(&fix, 10000 / portTICK_PERIOD_MS);
    ui_update(&fix);
    if (power_get_batt_percent(&percent, 0) != ESP_OK || ember_ble_gatts_set_batt_level(percent) != ESP_OK) {
      ESP_LOGE(TAG, "Failed to get or set battery percentage");
    }
    vTaskDelay(1000 / portTICK_PERIOD_MS);
  }
  // int secs = 0;
  // while (1) {
  //   gnss_fix_t fix;
  //   esp_err_t err = gnss_get_fix(&fix, 10000 / portTICK_PERIOD_MS);
  //   if (err != ESP_OK || !fix.fix) ESP_LOGE(TAG, "GNSS has no fix");
  //   if (err == ESP_OK) ui_update(&fix);
  //   if (secs++ >= 3 * 60) {
  //     secs = 0;
  //     char lat[9] = {0};
  //     char lon[10] = {0};
  //     char* msg = "Hello LoRa!";
  //     char time[8] = {0};
  //     sprintf(lat, "%02d%05.2lf%c", fix.latitude.degrees, fix.latitude.minutes, fix.latitude.cardinal);
  //     sprintf(lon, "%03d%05.2lf%c", fix.longitude.degrees, fix.longitude.minutes, fix.longitude.cardinal);
  //     sprintf(time, "%02d%02d%02dz", fix.time.tm_mday, fix.time.tm_hour, fix.time.tm_min);
  //     lora_send_aprs(lat, lon, msg, time);
  //   }
  //   vTaskDelay(1000 / portTICK_PERIOD_MS);
  // }
}
