// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'tracker_connect_button_controller.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$trackerConnectButtonControllerHash() =>
    r'c4695a5512d7a6f057c4aa198f7319a0b77df857';

/// See also [TrackerConnectButtonController].
@ProviderFor(TrackerConnectButtonController)
final trackerConnectButtonControllerProvider = AutoDisposeAsyncNotifierProvider<
    TrackerConnectButtonController, void>.internal(
  TrackerConnectButtonController.new,
  name: r'trackerConnectButtonControllerProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$trackerConnectButtonControllerHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$TrackerConnectButtonController = AutoDisposeAsyncNotifier<void>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
