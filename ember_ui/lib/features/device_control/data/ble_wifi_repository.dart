import 'dart:async';
import 'dart:convert';
import 'dart:typed_data';

import 'package:ember_ui/constants/ble_constants.dart';
import 'package:ember_ui/exceptions/out_of_bounds_exception.dart';
import 'package:ember_ui/features/device_control/data/ble_subscription_repository.dart';
import 'package:ember_ui/features/device_control/data/versioned_struct_array.dart';
import 'package:ember_ui/features/device_control/data/wifi_repository.dart';
import 'package:ember_ui/features/device_control/domain/device.dart';
import 'package:ember_ui/features/device_control/domain/wifi_network.dart';
import 'package:ember_ui/features/device_control/domain/wifi_state.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:universal_ble/universal_ble.dart';

part 'ble_wifi_repository.g.dart';

class BleWifiRepository implements WifiRepository {
  final BleSubscriptionRepository _bleSubscriptionRepository;

  BleWifiRepository(this._bleSubscriptionRepository);

  @override
  Future<void> connect(
      Device device, WifiNetwork network, String? password) async {
    final request = _wifiConnectRequest(network, password);
    await UniversalBle.writeValue(device.id, bleWifiServiceUid,
        bleWifiConnectChrUid, request, BleOutputProperty.withResponse);
  }

  @override
  Future<List<WifiNetwork>> scan(Device device) async {
    final buffer = await UniversalBle.readValue(
      device.id,
      bleWifiServiceUid,
      bleWifiScanChrUid,
    );
    final results = VersionedStructArray.fromBytes(buffer, wifiNetworkSize);
    return results.map((chunk) => _parseWifiNetwork(chunk)).toList();
  }

  Uint8List _wifiConnectRequest(WifiNetwork network, String? password) {
    const version = 0;
    return Uint8List.fromList([
      version,
      0, // Reserved
      0, // Reserved
      0, // Reserved
      ...utf8.encode(network.ssid.padRight(bleWifiApMaxLen, '\x00')),
      ...utf8.encode((password ?? "").padRight(bleWifiPassMaxLen, '\x00')),
    ]);
  }

  WifiNetwork _parseWifiNetwork(List<int> bytes) {
    var idx = 0;
    final ssid = utf8.decode(
        bytes.take(bleWifiApMaxLen).takeWhile((c) => c != 0x00).toList());
    idx += bleWifiApMaxLen;
    final rssi = Int8List.fromList([bytes.elementAt(idx++)]).first;
    var authModeByte = bytes.elementAt(idx++);
    if (authModeByte >= WifiAuthMode.values.length) {
      throw OutOfBoundsException();
    }
    final authMode = WifiAuthMode.values[authModeByte];
    return WifiNetwork(ssid: ssid, rssi: rssi, authMode: authMode);
  }

  WifiState _parseWifiState(List<int> buffer) {
    // Skip packet header (1b version + 3b reserved)
    buffer = buffer.sublist(4);
    // SSID
    final ssidBytes =
        buffer.take(bleWifiApMaxLen).takeWhile((c) => c != 0x00).toList();
    buffer = buffer.sublist(bleWifiApMaxLen);
    // IP Address
    final ipAddress = buffer.take(4).join(".");
    buffer = buffer.sublist(4);
    // Gateway
    final gwAddress = buffer.take(4).join(".");
    buffer = buffer.sublist(4);
    // MAC Address
    final macAddress = buffer
        .take(6)
        .map((b) => b.toRadixString(16).padLeft(2, '0'))
        .join(":")
        .toUpperCase();
    buffer = buffer.sublist(6);
    // Netmask/CIDR
    final cidr = buffer.take(1).first;
    buffer = buffer.sublist(1);
    // Expand the CIDR into a full netmask
    final netmask = (Uint8List(4)
          ..buffer.asUint32List()[0] = (1 << 32) - (1 << (32 - cidr)))
        .reversed
        .join(".");
    // RSSI
    final rssi = buffer.take(1).first.toSigned(8);
    buffer = buffer.sublist(1);
    // Status
    final status = buffer.take(1).first;
    buffer = buffer.sublist(1);
    return WifiState(
      ssid: utf8.decode(ssidBytes),
      ipAddress: ipAddress,
      gwAddress: gwAddress,
      netmask: netmask,
      macAddress: macAddress,
      rssi: rssi,
      status: WifiStatus.values[status],
    );
  }

  @override
  Future<WifiState> getState(Device device) async {
    var buffer = await UniversalBle.readValue(
      device.id,
      bleWifiServiceUid,
      bleWifiConnectChrUid,
    );
    return _parseWifiState(buffer);
  }

  @override
  Stream<WifiState> subscribeWifiState(Device device) {
    bool hasListeners = false;
    StreamController<WifiState>? streamController;
    void Function()? cancel;
    Future<void Function()?> subFuture = _bleSubscriptionRepository
        .listen(
          device,
          bleWifiServiceUid,
          bleWifiConnectChrUid,
          (_) async {
            if (hasListeners) {
              final state = await getState(device);
              streamController?.add(state);
            }
          },
        )
        .then((c) => cancel = c)
        .catchError((e) async {
          streamController?.addError(e);
          return () async {};
        });
    streamController = StreamController<WifiState>(
      onListen: () async {
        hasListeners = true;
        final state = await getState(device);
        streamController!.add(state);
      },
      onPause: () => hasListeners = false,
      onResume: () => hasListeners = true,
      onCancel: () {
        hasListeners = false;
        if (cancel == null) {
          // In case onCancel() is called before the subFuture completes
          subFuture.then((cancel) => cancel!());
        } else {
          cancel!();
        }
        streamController!.close();
        streamController = null;
      },
    );
    return streamController!.stream;
  }
}

@riverpod
BleWifiRepository bleWifiRepository(BleWifiRepositoryRef ref) {
  final bleSubscriptionRepository =
      ref.watch(bleSubscriptionRepositoryProvider);
  return BleWifiRepository(bleSubscriptionRepository);
}
