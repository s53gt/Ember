// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'aprs_symbol_graphic.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$aprsSymbolGraphicHash() => r'6b3c8aea75742071efcce695d3b9f2f7e1d92381';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

/// See also [aprsSymbolGraphic].
@ProviderFor(aprsSymbolGraphic)
const aprsSymbolGraphicProvider = AprsSymbolGraphicFamily();

/// See also [aprsSymbolGraphic].
class AprsSymbolGraphicFamily extends Family<AsyncValue<Uint8List>> {
  /// See also [aprsSymbolGraphic].
  const AprsSymbolGraphicFamily();

  /// See also [aprsSymbolGraphic].
  AprsSymbolGraphicProvider call(
    String tableChr,
    String symbolChr,
  ) {
    return AprsSymbolGraphicProvider(
      tableChr,
      symbolChr,
    );
  }

  @override
  AprsSymbolGraphicProvider getProviderOverride(
    covariant AprsSymbolGraphicProvider provider,
  ) {
    return call(
      provider.tableChr,
      provider.symbolChr,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'aprsSymbolGraphicProvider';
}

/// See also [aprsSymbolGraphic].
class AprsSymbolGraphicProvider extends AutoDisposeFutureProvider<Uint8List> {
  /// See also [aprsSymbolGraphic].
  AprsSymbolGraphicProvider(
    String tableChr,
    String symbolChr,
  ) : this._internal(
          (ref) => aprsSymbolGraphic(
            ref as AprsSymbolGraphicRef,
            tableChr,
            symbolChr,
          ),
          from: aprsSymbolGraphicProvider,
          name: r'aprsSymbolGraphicProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$aprsSymbolGraphicHash,
          dependencies: AprsSymbolGraphicFamily._dependencies,
          allTransitiveDependencies:
              AprsSymbolGraphicFamily._allTransitiveDependencies,
          tableChr: tableChr,
          symbolChr: symbolChr,
        );

  AprsSymbolGraphicProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.tableChr,
    required this.symbolChr,
  }) : super.internal();

  final String tableChr;
  final String symbolChr;

  @override
  Override overrideWith(
    FutureOr<Uint8List> Function(AprsSymbolGraphicRef provider) create,
  ) {
    return ProviderOverride(
      origin: this,
      override: AprsSymbolGraphicProvider._internal(
        (ref) => create(ref as AprsSymbolGraphicRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        tableChr: tableChr,
        symbolChr: symbolChr,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<Uint8List> createElement() {
    return _AprsSymbolGraphicProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is AprsSymbolGraphicProvider &&
        other.tableChr == tableChr &&
        other.symbolChr == symbolChr;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, tableChr.hashCode);
    hash = _SystemHash.combine(hash, symbolChr.hashCode);

    return _SystemHash.finish(hash);
  }
}

mixin AprsSymbolGraphicRef on AutoDisposeFutureProviderRef<Uint8List> {
  /// The parameter `tableChr` of this provider.
  String get tableChr;

  /// The parameter `symbolChr` of this provider.
  String get symbolChr;
}

class _AprsSymbolGraphicProviderElement
    extends AutoDisposeFutureProviderElement<Uint8List>
    with AprsSymbolGraphicRef {
  _AprsSymbolGraphicProviderElement(super.provider);

  @override
  String get tableChr => (origin as AprsSymbolGraphicProvider).tableChr;
  @override
  String get symbolChr => (origin as AprsSymbolGraphicProvider).symbolChr;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
