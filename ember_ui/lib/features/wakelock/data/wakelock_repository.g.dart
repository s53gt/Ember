// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'wakelock_repository.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$wakelockRepositoryHash() =>
    r'72b6ed60a585c704879d3644b14c35c96c47fd55';

/// See also [wakelockRepository].
@ProviderFor(wakelockRepository)
final wakelockRepositoryProvider =
    AutoDisposeProvider<WakelockRepository>.internal(
  wakelockRepository,
  name: r'wakelockRepositoryProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$wakelockRepositoryHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef WakelockRepositoryRef = AutoDisposeProviderRef<WakelockRepository>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
