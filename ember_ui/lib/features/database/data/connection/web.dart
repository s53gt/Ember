import 'package:drift/drift.dart';
import 'package:drift/wasm.dart';
import 'package:ember_ui/constants/database_constants.dart';

DatabaseConnection connect() {
  return DatabaseConnection.delayed(Future.sync(() async {
    final result = await WasmDatabase.open(
        databaseName: dbName,
        sqlite3Uri: Uri.parse("sqlite3.wasm"),
        driftWorkerUri: Uri.parse("drift_worker.js"));
    if (result.missingFeatures.isNotEmpty) {
      print('Using ${result.chosenImplementation} due to missing browser '
          'features: ${result.missingFeatures}');
    }
    return result.resolvedExecutor;
  }));
}
