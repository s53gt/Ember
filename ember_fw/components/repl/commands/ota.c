#include "commands/ota.h"

#include "argtable3/argtable3.h"
#include "esp_console.h"
#include "esp_err.h"
#include "esp_log.h"
#include "ota.h"
#include "repl_private.h"

static const char* TAG = "ota";

static int repl_ota_cmd(int argc, char** argv);

static struct {
  struct arg_str* url;
  struct arg_end* end;
} ota_args;

static esp_console_cmd_t ota_cmd = {
    .command = "ota",
    .help = "Update the device OTA",
    .hint = NULL,
    .func = &repl_ota_cmd,
    .argtable = &ota_args,
};

static void repl_ota_init_arg_tables(void) {
  ota_args.url = arg_str1("u", "url", "<https-url>", "HTTPS URL to download the update from");
  ota_args.end = arg_end(2);
}

esp_err_t repl_ota_register(void) {
  repl_ota_init_arg_tables();
  return esp_console_cmd_register(&ota_cmd);
}

static int repl_ota_cmd(int argc, char** argv) {
  REPL_ARGS_CHECK(ota_args, argc, argv);
  if (ota_args.url->count > 0) {
    esp_err_t err = ota_update_https(ota_args.url->sval[0]);
    if (err != ESP_OK) {
      ESP_LOGE(TAG, "OTA upgrade failed, err=%d, err_str=%s", err, esp_err_to_name(err));
      return 1;
    }
    return 0;
  }
  ESP_LOGE(TAG, "Invalid arguments");
  return 1;
}
