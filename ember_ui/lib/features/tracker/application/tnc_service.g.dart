// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'tnc_service.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$tncServiceHash() => r'abf72bcf444e1842ca2a5a8d8ba09b810800462f';

/// See also [tncService].
@ProviderFor(tncService)
final tncServiceProvider = AutoDisposeProvider<TncService>.internal(
  tncService,
  name: r'tncServiceProvider',
  debugGetCreateSourceHash:
      const bool.fromEnvironment('dart.vm.product') ? null : _$tncServiceHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef TncServiceRef = AutoDisposeProviderRef<TncService>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
