#pragma once

#include "esp_err.h"
#include "freertos/FreeRTOS.h"

typedef struct {
  const uint8_t *data;
  size_t len;
} aprs_is_packet_t;

typedef void (*aprs_is_process_packet_cb)(const aprs_is_packet_t *packet);
typedef struct {
  char name[10];
  aprs_is_process_packet_cb process_packet;
} aprs_is_proto_t;

esp_err_t aprs_is_init(void);
esp_err_t aprs_is_vsendf(TickType_t block_time, const char *fmt, va_list args);
esp_err_t aprs_is_sendf(TickType_t block_time, const char *fmt, ...);
esp_err_t aprs_is_register_proto(const aprs_is_proto_t *proto);
bool aprs_is_connected();
