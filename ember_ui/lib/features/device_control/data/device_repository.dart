import 'package:ember_ui/features/device_control/domain/device.dart';

abstract class DeviceRepository {
  DeviceRepository();
  Stream<Device> get discoveredDevices;
  Stream<bool> get isScanning;
  Stream<MapEntry<String, DeviceConnectionState>> get connectionState;

  Future<List<Device>> discoverDevices();
  Future<void> stopDeviceDiscovery();
  Future<void> connect(Device device);
  Future<void> disconnect(Device device);
  bool isConnected(Device device);
  Future<String> getName(Device device);
}
