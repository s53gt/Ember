import 'package:ember_ui/features/permission/application/permission_mutex.dart';
import 'package:ember_ui/features/permission/domain/permission_result.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:universal_ble/universal_ble.dart';

part 'ble_permission.g.dart';

final permissions = [
  Permission.bluetoothScan,
  Permission.bluetoothConnect,
];

@riverpod
class BlePermission extends _$BlePermission {
  PermissionResult _result = PermissionResult.pending;

  Future<void> request(bool launchSettings) async {
    await permissionMutex.acquire();
    try {
      _result = PermissionResult.pending;
      ref.invalidateSelf();
      final state = await UniversalBle.getBluetoothAvailabilityState();
      if (state == AvailabilityState.unsupported) {
        _result = PermissionResult.permanentlyDenied;
        ref.invalidateSelf();
        return;
      }
      final status = await Future.wait(permissions.map((perm) => perm.status),
          eagerError: true);
      if (status
          .any((status) => status == PermissionStatus.permanentlyDenied)) {
        if (launchSettings) await openAppSettings();
        _result = PermissionResult.permanentlyDenied;
        ref.invalidateSelf();
        return;
      }
      final reqStatus = await permissions.request();
      bool granted = true;
      bool permanentlyDenied = false;
      for (var status in reqStatus.values) {
        if ([
          PermissionStatus.denied,
          PermissionStatus.restricted,
          PermissionStatus.permanentlyDenied,
        ].contains(status)) {
          granted = false;
          if (status == PermissionStatus.permanentlyDenied) {
            permanentlyDenied = true;
          }
          // Don't break, loop till the end, so permanentlyDenied is always set correctly
        }
      }
      if (!granted) {
        _result = permanentlyDenied
            ? PermissionResult.permanentlyDenied
            : PermissionResult.denied;
        ref.invalidateSelf();
        return;
      }
      await UniversalBle.enableBluetooth();
      _result = PermissionResult.granted;
      ref.invalidateSelf();
    } finally {
      permissionMutex.release();
    }
  }

  @override
  PermissionResult build() {
    return _result;
  }
}
