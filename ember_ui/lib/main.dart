import 'package:ember_ui/features/database/data/database.dart';
import 'package:ember_ui/features/device_control/application/device_service.dart';
import 'package:ember_ui/features/localization/domain/language_model.dart';
import 'package:ember_ui/features/tracker/application/tracker_service.dart';
import 'package:ember_ui/routing/root.dart';
import 'package:ember_ui/utils/environment.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Environment.init();
  runApp(const ProviderScope(child: MyApp()));
}

class MyApp extends ConsumerWidget {
  const MyApp({super.key});

  void _eagerInit(WidgetRef ref) {
    ref.watch(databaseProvider);
    ref.watch(deviceServiceProvider);
    ref.watch(trackerServiceProvider);
  }

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    _eagerInit(ref);
    var lang = ref.watch(languageProvider);
    return MaterialApp.router(
      title: 'Ember UI',
      locale: Locale(lang),
      localizationsDelegates: AppLocalizations.localizationsDelegates,
      supportedLocales: AppLocalizations.supportedLocales,
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(
          dynamicSchemeVariant: DynamicSchemeVariant.content,
          seedColor: const Color.fromARGB(255, 26, 22, 21),
          primary: const Color.fromARGB(255, 183, 82, 49),
          brightness: Brightness.dark,
        ),
        useMaterial3: true,
      ),
      themeMode: ThemeMode.dark,
      routerConfig: routerConfig,
    );
  }
}
