// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ble_permission.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$blePermissionHash() => r'85192ab555b12183bfd341b074c0743504e32faf';

/// See also [BlePermission].
@ProviderFor(BlePermission)
final blePermissionProvider =
    AutoDisposeNotifierProvider<BlePermission, PermissionResult>.internal(
  BlePermission.new,
  name: r'blePermissionProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$blePermissionHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$BlePermission = AutoDisposeNotifier<PermissionResult>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
