import 'dart:async';

import 'package:ember_ui/features/tracker/application/aprs_service.dart';
import 'package:ember_ui/features/tracker/application/tracker_service.dart';
import 'package:ember_ui/features/tracker/data/position_report_repository.dart';
import 'package:ember_ui/features/tracker/domain/aprs_position.dart';
import 'package:ember_ui/features/aprs_symbol/presentation/aprs_symbol.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_map_supercluster/flutter_map_supercluster.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:latlong2/latlong.dart';
import 'package:quiver/collection.dart';

class ClusterMarkerLayer extends ConsumerStatefulWidget {
  const ClusterMarkerLayer({super.key});

  @override
  ConsumerState<ConsumerStatefulWidget> createState() =>
      _ClusterMarkerLayerState();
}

class _ClusterMarkerLayerState extends ConsumerState<ClusterMarkerLayer> {
  final SuperclusterMutableController _controller =
      SuperclusterMutableController();
  final BiMap<String, Marker> _activeMarkers = BiMap();

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  Marker _makeMarker(AprsPosition position) => Marker(
        // TODO set to false when course data is available and rotate the icon based on that instead
        rotate: true,
        point: LatLng(position.latitude, position.longitude),
        child: Tooltip(
          message: position.header.from,
          showDuration: const Duration(seconds: 5),
          child: AprsSymbol(
            tableChr: position.tableChr,
            symbolChr: position.symbolChr,
          ),
        ),
        width: 32,
        height: 32,
      );

  void _onPositionUpdated(AprsPosition position) {
    if (_activeMarkers.containsKey(position.header.from)) {
      _controller.remove(_activeMarkers.remove(position.header.from)!);
    }
    final marker = _makeMarker(position);
    _controller.add(marker);
    _activeMarkers[position.header.from] = marker;
  }

  @override
  Widget build(BuildContext context) {
    final localizations = AppLocalizations.of(context)!;
    ref.listen(positionReportRepositoryProvider, (_, positionReports) {
      for (var position in positionReports.valueOrNull ?? []) {
        _onPositionUpdated(position);
      }
    });
    return SuperclusterLayer.mutable(
      controller: _controller,
      onMarkerTap: (marker) {
        if (!_activeMarkers.containsValue(marker)) {
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(content: Text(localizations.somethingWentWrong)),
          );
          return;
        }
        final fromCall = _activeMarkers.inverse[marker];
        print("FROM: $fromCall");
      },
      builder: (context, position, markerCount, extraClusterData) => Container(
        width: 32,
        height: 32,
        alignment: Alignment.center,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: Theme.of(context).colorScheme.primary,
        ),
        child: Text(markerCount.toString()),
      ),
    );
  }
}
