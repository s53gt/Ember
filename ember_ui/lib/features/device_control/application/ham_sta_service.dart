import 'package:ember_ui/exceptions/invalid_state_exception.dart';
import 'package:ember_ui/features/device_control/application/device_service.dart';
import 'package:ember_ui/features/device_control/data/ble_ham_sta_repository.dart';
import 'package:ember_ui/features/device_control/data/ham_sta_repository.dart';
import 'package:ember_ui/features/device_control/domain/device.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'ham_sta_service.g.dart';

class HamStaService {
  final DeviceService _deviceService;
  final Map<LinkType, HamStaRepository> _repos;

  HamStaService(this._deviceService, this._repos);

  HamStaRepository get _repo {
    if (_deviceService.connectedDevice == null) throw InvalidStateException();
    final repo = _repos[_deviceService.connectedDevice!.linkType];
    if (repo == null) throw UnimplementedError();
    return repo;
  }

  Future<String> getCallsign() async {
    return _repo.getCallsign(_deviceService.connectedDevice!);
  }

  Future<void> setCallsign(String callsign) async {
    await _repo.setCallsign(_deviceService.connectedDevice!, callsign);
    // Updating callsign will also update the device name
    await _deviceService.refreshDeviceName();
  }

  Future<String> getPasscode() async {
    return _repo.getPasscode(_deviceService.connectedDevice!);
  }

  Future<void> setPasscode(String passcode) async {
    await _repo.setPasscode(_deviceService.connectedDevice!, passcode);
  }

  Future<String> getSymbol() async {
    return _repo.getSymbol(_deviceService.connectedDevice!);
  }

  Future<void> setSymbol(String symbol) async {
    await _repo.setSymbol(_deviceService.connectedDevice!, symbol);
  }
}

@riverpod
HamStaService hamStaService(HamStaServiceRef ref) {
  final deviceService = ref.watch(deviceServiceProvider);
  return HamStaService(
    deviceService,
    {
      LinkType.ble: ref.watch(bleHamStaRepositoryProvider),
    },
  );
}
