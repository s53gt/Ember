#pragma once

// Radio defaults
#define LORA_FREQ_MHZ (433.755)
#define LORA_BANDWIDTH_KHZ (125)
#define LORA_SPREAD_FACTOR (12)
#define LORA_CODING_RATE (5)

// LoRa network stack defaults
#define LORA_RX_QUEUE_SIZE (10)
#define LORA_TX_QUEUE_SIZE (10)

// HAM Station defaults
#define HAM_STA_CALLSIGN_DEFAULT ("NOCALL")
#define HAM_STA_CALLSIGN_MAX_LEN (7)
#define HAM_STA_SSID_MAX_LEN (2)
#define HAM_STA_CALL_SSID_MAX_LEN (HAM_STA_CALLSIGN_MAX_LEN + HAM_STA_SSID_MAX_LEN + 1)
#define HAM_STA_SYMBOL_LEN (2)
#define HAM_STA_SYMBOL_DEFAULT ("/>")

// APRS-IS defaults
#define APRS_IS_PASSCODE_MAX_LEN (5)
#define APRS_IS_PASSCODE_DEFAULT ("-1")
