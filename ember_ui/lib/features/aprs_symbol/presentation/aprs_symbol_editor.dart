import 'package:ember_ui/features/aprs_symbol/presentation/aprs_symbol.dart';
import 'package:ember_ui/features/aprs_symbol/presentation/aprs_symbol_table.dart';
import 'package:ember_ui/features/device_control/application/ham_sta_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class AprsSymbolEditor extends ConsumerStatefulWidget {
  final TextEditingController controller;

  const AprsSymbolEditor({super.key, required this.controller});

  @override
  ConsumerState<ConsumerStatefulWidget> createState() =>
      _AprsSymbolEditorState();
}

class _AprsSymbolEditorState extends ConsumerState<AprsSymbolEditor> {
  final List<String> _selectableTables = ["/", "\\", "\x20"];
  List<bool> _selectedTable = [true, false, false];
  String _selectedTableChr = "/";
  String _tableChr = "";
  String _symbolChr = "";

  @override
  void initState() {
    super.initState();
    final hamStaService = ref.read(hamStaServiceProvider);
    widget.controller.addListener(_onSymbolChanged);
    hamStaService.getSymbol().then((symbol) => widget.controller.text = symbol);
  }

  void _onSymbolChanged() {
    final symbol = widget.controller.text;
    if (symbol.length != 2) return;
    setState(() {
      _tableChr = symbol[0];
      _symbolChr = symbol[1];
    });
  }

  @override
  Widget build(BuildContext context) {
    final localizations = AppLocalizations.of(context)!;
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(bottom: 10),
          child: Row(
            children: [
              AprsSymbol(tableChr: _tableChr, symbolChr: _symbolChr),
              const SizedBox(width: 10),
              Expanded(
                child: TextField(
                  controller: widget.controller,
                  inputFormatters: [
                    FilteringTextInputFormatter.allow(RegExp(r'[!-~]')),
                    LengthLimitingTextInputFormatter(2),
                  ],
                  decoration: InputDecoration(
                    hintText: localizations.aprsSymbol,
                    isDense: true,
                  ),
                ),
              ),
            ],
          ),
        ),
        Expanded(
          child: SingleChildScrollView(
            child: Column(children: [
              Padding(
                padding: const EdgeInsets.only(bottom: 10),
                child: ToggleButtons(
                  onPressed: (i) => setState(
                    () {
                      if (_tableChr == "/" && i == 2) {
                        ScaffoldMessenger.of(context).showSnackBar(
                          SnackBar(
                            content:
                                Text(localizations.pickSecondarySymbolFirst),
                          ),
                        );
                        return;
                      }
                      _selectedTable =
                          List.generate(_selectedTable.length, (j) => i == j);
                      _selectedTableChr = _selectableTables[i];
                    },
                  ),
                  isSelected: _selectedTable,
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10),
                      child: Text(localizations.primary),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10),
                      child: Text(localizations.secondary),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10),
                      child: Text(localizations.overlay),
                    ),
                  ],
                ),
              ),
              AprsSymbolTable(
                table: _selectedTableChr,
                selectedSymbol: "$_tableChr$_symbolChr",
                onPressed: (symbol) {
                  String newSymbol = "$_selectedTableChr$symbol";
                  if (_selectedTableChr == "\x20") {
                    newSymbol = "$symbol$_symbolChr";
                  } else if (_selectedTableChr == "\\" &&
                      _tableChr != "\\" &&
                      _tableChr != "/") {
                    newSymbol = "$_tableChr$symbol";
                  }
                  widget.controller.text = newSymbol;
                },
              ),
            ]),
          ),
        ),
      ],
    );
  }
}
