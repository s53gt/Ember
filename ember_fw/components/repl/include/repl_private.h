#pragma once

#define REPL_ARGS_CHECK(ARGS, ARGC, ARGV)               \
  {                                                     \
    int nerrors = arg_parse(ARGC, ARGV, (void**)&ARGS); \
    if (nerrors != 0) {                                 \
      arg_print_errors(stderr, ARGS.end, ARGV[0]);      \
      return 1;                                         \
    }                                                   \
  }

char* repl_getpass(const char* prompt);
