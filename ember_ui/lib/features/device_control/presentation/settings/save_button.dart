import 'package:flutter/material.dart';

class SaveButton extends StatelessWidget {
  final String? tooltip;
  final VoidCallback onPressed;
  final bool disabled;
  final bool loading;

  const SaveButton({
    super.key,
    this.tooltip,
    this.disabled = false,
    this.loading = false,
    required this.onPressed,
  });

  @override
  Widget build(BuildContext context) {
    return IconButton(
      padding: const EdgeInsets.all(8),
      style: ButtonStyle(
        shape: WidgetStatePropertyAll(
          RoundedRectangleBorder(
            side: BorderSide(
              color: Theme.of(context).colorScheme.primary,
            ),
            borderRadius: BorderRadius.circular(5.0),
          ),
        ),
      ),
      tooltip: tooltip,
      onPressed: disabled ? null : onPressed,
      icon: loading
          ? SizedBox(
              width: IconTheme.of(context).size ?? 24,
              height: IconTheme.of(context).size ?? 24,
              child: const CircularProgressIndicator(),
            )
          : const Icon(Icons.save),
    );
  }
}
