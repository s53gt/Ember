import 'package:ember_ui/features/device_control/application/device_service.dart';
import 'package:ember_ui/features/device_control/domain/device.dart';
import 'package:ember_ui/features/device_control/presentation/ble/ble_scan_list_controller.dart';
import 'package:ember_ui/features/device_control/presentation/ble/ble_scan_result.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class BleScanList extends ConsumerStatefulWidget {
  final void Function(Device device) onConnected;

  const BleScanList({super.key, required this.onConnected});

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _BleScanListState();
}

class _BleScanListState extends ConsumerState<BleScanList> {
  @override
  void deactivate() {
    final bleScanListController =
        ref.watch(bleScanListControllerProvider.notifier);
    bleScanListController.stopDiscovery();
    super.deactivate();
  }

  @override
  Widget build(BuildContext context) {
    final deviceService = ref.watch(deviceServiceProvider);
    final bleScanResults = ref.watch(bleScanListControllerProvider);
    final bleScanListController =
        ref.watch(bleScanListControllerProvider.notifier);
    var localizations = AppLocalizations.of(context)!;
    return StreamBuilder(
        stream: deviceService.isScanningStream(LinkType.ble),
        builder: (context, snapshot) => Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      localizations.nearbyDevices,
                      style: const TextStyle(fontSize: 23),
                    ),
                    IconButton(
                        onPressed: snapshot.data == true
                            ? null
                            : bleScanListController.refresh,
                        icon: const Icon(Icons.refresh)),
                  ],
                ),
                const Divider(),
                if (bleScanResults.isEmpty)
                  Padding(
                    padding: const EdgeInsets.all(10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        if (snapshot.data == true) ...[
                          const SizedBox(
                            width: 16,
                            height: 16,
                            child: CircularProgressIndicator(
                              strokeWidth: 2,
                            ),
                          ),
                          const SizedBox(
                            width: 10,
                          ),
                        ],
                        Text(snapshot.data == true
                            ? localizations.lookingForDevices
                            : localizations.noResults),
                      ],
                    ),
                  ),
                Expanded(
                    child: ListView(
                  shrinkWrap: true,
                  children: bleScanResults
                      .map((r) => BleScanResult(
                            device: r,
                            onConnected: widget.onConnected,
                          ))
                      .toList(),
                )),
              ],
            ));
  }
}
