import 'package:ember_ui/features/permission/application/permission_mutex.dart';
import 'package:ember_ui/features/permission/domain/permission_result.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'location_permission.g.dart';

@riverpod
class LocationPermission extends _$LocationPermission {
  PermissionResult _result = PermissionResult.pending;

  Future<void> request(bool launchSettings) async {
    await permissionMutex.acquire();
    try {
      _result = PermissionResult.pending;
      ref.invalidateSelf();
      if (await Permission.location.status ==
          PermissionStatus.permanentlyDenied) {
        if (launchSettings) await openAppSettings();
        _result = PermissionResult.permanentlyDenied;
        ref.invalidateSelf();
        return;
      }
      final status = await Permission.location.request();
      if (![
        PermissionStatus.denied,
        PermissionStatus.restricted,
        PermissionStatus.permanentlyDenied,
      ].contains(status)) {
        _result = PermissionResult.granted;
        ref.invalidateSelf();
        return;
      }
      _result = status == PermissionStatus.permanentlyDenied
          ? PermissionResult.permanentlyDenied
          : PermissionResult.denied;
      ref.invalidateSelf();
    } finally {
      permissionMutex.release();
    }
  }

  @override
  PermissionResult build() {
    return _result;
  }
}
