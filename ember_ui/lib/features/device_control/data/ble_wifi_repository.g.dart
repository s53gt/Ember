// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ble_wifi_repository.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$bleWifiRepositoryHash() => r'1616d451a39dbf6f628cea47d0054d8c7bb7300d';

/// See also [bleWifiRepository].
@ProviderFor(bleWifiRepository)
final bleWifiRepositoryProvider =
    AutoDisposeProvider<BleWifiRepository>.internal(
  bleWifiRepository,
  name: r'bleWifiRepositoryProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$bleWifiRepositoryHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef BleWifiRepositoryRef = AutoDisposeProviderRef<BleWifiRepository>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
